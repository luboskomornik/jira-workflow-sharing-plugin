(function() {

    var localWFShareBundles = function() {

        // Handle a click on a plugin row
        this.loadPlugins = function(page) {
            AJS.$.ajax({
                url: "$contextPath/rest/wfshare/1.0/workflowbundles/summary/" + page,
                type: 'get',
                cache: false,
                success:function (pluginsJson) {

                    jQuery('.upm-bundles-loading-container').css('display','none');

                    var plugins = pluginsJson.plugins;

                    for (var i = 0, len = plugins.length; i < len; i++)
                    {
                        var plugin = plugins[i];
                        plugin.nextFormUrl = jQuery("#wfshareNextFormUrl").val();
                        jQuery("#workflowBundleListContainer").append(JIRA.Templates.WFShare.pluginEntry({plugin:plugin}));
                    }
                    if(pluginsJson.links.next != null && pluginsJson.links.next != undefined)
                    {
                        jQuery('.upm-plugins-see-more').css('display','block');
                        jQuery('.upm-plugins-see-more').unbind('click').click(function(){
                            jQuery(this).css('display','none');
                            jQuery('.upm-bundles-loading-container').css('display','inline');
                            wfshareBundles.loadPlugins(page + 1);
                        });
                    }
                    else
                    {
                        jQuery('.upm-plugins-see-more').css('display','none');
                    }
                    
                }
            });
            
        };

        this.pluginRowClick = function(e)
        {
            var target = jQuery(e.target);
            if (!target.closest('.upm-plugin-actions').length) {
                wfshareBundles.togglePluginDetails(target.closest('div.upm-plugin'));
            }
        }

        this.togglePluginDetails = function(container) {
            if (container.hasClass('expanded')) {
                container.removeClass('expanded');
            } else {
                var details = container.find('div.upm-details');
                if (!details.hasClass('loaded') && !details.hasClass('loading')) {
                    container.addClass('loading');

                    AJS.$.ajax({
                        url: "$contextPath/rest/wfshare/1.0/workflowbundles/details/" + container.attr('plugin-key'),
                        type: 'get',
                        cache: false,
                        success:function (detailsJson) {
                            detailsJson.nextFormUrl = jQuery("#wfshareNextFormUrl").val();
                            
                            details.append(JIRA.Templates.WFShare.pluginDetails({details:detailsJson}));
                            container.removeClass('loading').addClass('expanded');
                            details.addClass('loaded');
                            
                            var screenshotsContainer = jQuery('div.upm-plugin-screenshots', details),
                                numScreenshots = detailsJson.media.screenshots.length || 0;
                            if (numScreenshots) {
                                jQuery('a', screenshotsContainer).click(function (e) {
                                    e.preventDefault();
                                    jQuery.fancybox(
                                        wfshareBundles.getScreenshotArray(detailsJson.media.screenshots),
                                        {
                                            changeSpeed: 0,
                                            cyclic: true,
                                            overlayColor: '#000',
                                            overlayOpacity: 0.8,
                                            speedIn: 0,
                                            speedOut: 0,
                                            transitionIn: 'none',
                                            transitionOut: 'none'
                                        });
                                });
                            }
                            
                        }
                    });
                } else {
                    container.addClass('expanded').trigger('pluginLoaded');
                }
            }
        };
        
        this.getScreenshotArray = function(screenshots)
        {
            var ss = [],
                s;
            for (var i = 0; i < screenshots.length; i++) {
                s = { 'href' : screenshots[i].image.links.binary };
                screenshots[i].name && (s.title = screenshots[i].name);
                ss.push(s);
            }

            return ss;
        }

    };

    window.wfshareBundles = new localWFShareBundles();

    jQuery(document).ready(function ()
    {
        var plugins = jQuery('div.upm-plugin-list.expandable div.upm-plugin');
        plugins.find('div.upm-plugin-row').live('click', wfshareBundles.pluginRowClick);
        
        jQuery('.upm-plugins-see-bundles').click(function(){
            jQuery(this).css('display','none');
            jQuery('.upm-bundles-loading-container').css('display','inline');
            wfshareBundles.loadPlugins(1);
        });
        
        var tabs = jQuery('ul.tabs-menu li.aui-tabs-group ul.aui-tabs-group-menu li.menu-item a');
        if(tabs.length > 0)
        {
            tabs.click(function(e){
                e.stopImmediatePropagation();
                e.preventDefault();
            });
        }

    });

})();