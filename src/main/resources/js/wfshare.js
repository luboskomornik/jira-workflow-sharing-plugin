AJS.namespace("wfshare");

(function() {

var localWFShare = function() {

    // Handle a click on a plugin row
    this.pluginRowClick = function(e) {
        wfshare.togglePluginDetails(AJS.$(e.target).closest('div.wfshare-plugin'));
    };

    /**
     * Toggles an html element representing a plugin between an expanded and collapsed state
     * @method togglePluginDetails
     * @param {HTMLElement} container The plugin details container
     */
    this.togglePluginDetails = function(container) {
        var details;
        if (container.hasClass('expanded')) {
            container.removeClass('expanded');

        } else {
            container.addClass('expanded');
        }
    };


};

    window.wfshare = new localWFShare();

    jQuery(document).ready(function ()
    {
        var tabs = jQuery('ul.tabs-menu li.aui-tabs-group ul.aui-tabs-group-menu li.menu-item a');
        if(tabs.length > 0)
        {
            tabs.click(function(e){
                e.stopImmediatePropagation();
                e.preventDefault();
            });
        }
        
    });
    
})();
