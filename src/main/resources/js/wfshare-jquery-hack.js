jQuery(document).ready(function ()
{
    if(window.location.pathname.indexOf("/admin/workflows/ListWorkflows.jspa") > 0)
    {
        var major = "$jiraMajorVersion";
        var minor = "$jiraMinorVersion";
        var isBeforeFive = (major < 5);
        /*
         * EXPORT LINKS FOR EACH WORKFLOW
         */
        var copyLinks = jQuery('.operations-list a[id^="copy_"]');
        if(copyLinks.length > 0)
        {
            copyLinks.each(function(index, element){
                var copyLink = jQuery(this);
                var wfName = copyLink.url().param('workflowName');
                var wfMode = copyLink.url().param('workflowMode');
                
                if(wfName !== null && wfName !== "" && wfName !== "jira" && wfMode !== null && wfMode !== "")
                {
                    copyLink.parent().after('<li><a href="$contextPath/plugins/servlet/wfshare-export?workflowMode=' + wfMode + '&workflowName=' + wfName +'">' + AJS.I18n.getText("wfshare.screen.workflows.label.export") + '</a></li>');
                }
                
            });
        }
        
        /*
         * IMPORT LINK
         */
        var importXmlLink = jQuery('a[href^="ImportWorkflowFromXml"]');
        var importBundleLink = AJS.I18n.getText("wfshare.screen.workflows.comma.or") + ' <b><a id="import-bundle" href="$contextPath/plugins/servlet/wfshare-import">' + AJS.I18n.getText("wfshare.screen.workflows.label.import") + '</a></b>';
        var importBundleToolItem = '<li class="toolbar-item"><a id="import-bundle" class="toolbar-trigger trigger-dialog" href="$contextPath/plugins/servlet/wfshare-import">' + AJS.I18n.getText("wfshare.screen.workflows.toolitem.import") + '</a></li>';
        var importHtml;
        
        if(isBeforeFive)
        {
            importHtml = importBundleLink;
        }
        else
        {
            importHtml = importBundleToolItem
        }
        
        if(importXmlLink.length > 0)
        {
            importXmlLink.parent().after(importHtml);
        }
        else
        {
            if(isBeforeFive)
            {
                var descWrap = jQuery('.desc-wrap');
                descWrap.append('You can create a new workflow below' + importHtml);
            }
            else
            {
                var addWorkflowLink = jQuery('a[href^="AddNewWorkflow.jspa"]');
                addWorkflowLink.parent().after(importHtml)
            }
        }
    }
   
});