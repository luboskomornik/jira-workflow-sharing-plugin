package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.atlassian.jira.plugin.workflow.sharing.importer.PluginInstallInfo;

/**
 * @since version
 */
public interface PluginInstallInfoFactory
{
    List<PluginInstallInfo> getPluginInstallationInfo(File pluginsJson) throws IOException;
}
