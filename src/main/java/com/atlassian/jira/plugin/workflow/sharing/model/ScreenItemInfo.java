package com.atlassian.jira.plugin.workflow.sharing.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since version
 */
public class ScreenItemInfo implements Serializable
{
    private static final long serialVersionUID = -2834016805917751125L;
    private Long originalId;
    private String fieldId;
    private Integer position;

    @JsonCreator
    public ScreenItemInfo(@JsonProperty("originalId") Long originalId
            ,@JsonProperty("fieldId") String fieldId
            ,@JsonProperty("position") Integer position)
    {
        this.originalId = originalId;
        this.fieldId = fieldId;
        this.position = position;
    }

    public Long getOriginalId()
    {
        return originalId;
    }

    public String getFieldId()
    {
        return fieldId;
    }

    public Integer getPosition()
    {
        return position;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(originalId).append(fieldId).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this) { return true; }

        if(!(obj instanceof ScreenItemInfo)) { return false; }

        ScreenItemInfo info = (ScreenItemInfo) obj;

        return new EqualsBuilder().append(this.getOriginalId(),info.getOriginalId()).append(this.getFieldId(), info.getFieldId()).isEquals();
    }

    @Override
    public String toString()
    {
        return "ScreenItemInfo fieldId:" + fieldId;//NON-NLS
    }
}
