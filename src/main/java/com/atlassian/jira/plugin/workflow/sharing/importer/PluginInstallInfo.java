package com.atlassian.jira.plugin.workflow.sharing.importer;

import java.io.Serializable;
import java.net.URL;

import com.atlassian.jira.plugin.workflow.sharing.model.WorkflowExtensionsPluginInfo;
import com.atlassian.velocity.htmlsafe.HtmlSafe;

import org.apache.commons.lang.StringUtils;

/**
 * @since version
 */
public class PluginInstallInfo implements Serializable
{
    private static final long serialVersionUID = -406389627357923525L;

    public enum Source {LIB,PAC};

    private boolean removeExtensions;
    private boolean installable;
    private boolean alreadyInstalled;
    private final String message;
    private final Source source;
    private final String installVersion;
    private String alreadyInstalledVersion;
    private final URL pacDownloadUrl;
    private final WorkflowExtensionsPluginInfo pluginInfo;

    private PluginInstallInfo(WorkflowExtensionsPluginInfo pluginInfo, boolean installable, boolean removeExtensions, boolean alreadyInstalled, String message, Source source, String installVersion, String alreadyInstalledVersion, URL pacDownloadUrl)
    {
        this.installable = installable;
        this.removeExtensions = removeExtensions;
        this.alreadyInstalled = alreadyInstalled;
        this.message = message;
        this.source = source;
        this.pluginInfo = pluginInfo;
        this.installVersion = installVersion;
        this.alreadyInstalledVersion = alreadyInstalledVersion;
        this.pacDownloadUrl = pacDownloadUrl;
    }

    public boolean getInstallable()
    {
        return installable;
    }

    public boolean getRemoveExtensions()
    {
        return removeExtensions;
    }

    public boolean getAlreadyInstalled()
    {
        return alreadyInstalled;
    }

    public boolean hasMessage()
    {
        return StringUtils.isNotBlank(message);
    }

    @HtmlSafe
    public String getMessage()
    {
        return message;
    }

    public Source getSource()
    {
        return source;
    }

    public String getInstallVersion()
    {
        return installVersion;
    }

    public String getAlreadyInstalledVersion()
    {
        return alreadyInstalledVersion;
    }

    public URL getPacDownloadUrl()
    {
        return pacDownloadUrl;
    }

    public WorkflowExtensionsPluginInfo getPluginInfo()
    {
        return pluginInfo;
    }

    public PluginInstallInfo notInstallable()
    {
        this.installable = false;
        return this;
    }

    public PluginInstallInfo removeExtensions()
    {
        this.removeExtensions = true;
        return this;
    }

    public PluginInstallInfo alreadyInstalled(String version)
    {
        this.alreadyInstalled = true;
        this.alreadyInstalledVersion = version;
        return this;
    }

    public static PluginInstallSourceBuilder builder(WorkflowExtensionsPluginInfo pluginInfo)
    {
        return new PluginInstallSourceBuilder(pluginInfo);
    }

    public static class PluginInstallSourceBuilder
    {
        private final WorkflowExtensionsPluginInfo pluginInfo;

        public PluginInstallSourceBuilder(WorkflowExtensionsPluginInfo pluginInfo)
        {
            this.pluginInfo = pluginInfo;
        }

        public PluginInstallInfoBuilder fromPac(String version)
        {
            return new PluginInstallInfoBuilder(this.pluginInfo,Source.PAC,version);
        }

        public PluginInstallInfoBuilder fromLib()
        {
            return new PluginInstallInfoBuilder(this.pluginInfo,Source.LIB,pluginInfo.getVersion());
        }
    }

    public static class PluginInstallInfoBuilder
    {
        private boolean removeExtensions;
        private boolean installable;
        private boolean alreadyInstalled;
        private String installVersion;
        private String alreadyInstalledVersion;
        private URL pacDownloadUrl;
        private String message;
        private  Source source;
        private final WorkflowExtensionsPluginInfo pluginInfo;

        public PluginInstallInfoBuilder(WorkflowExtensionsPluginInfo pluginInfo, Source source, String installVersion)
        {
            this.pluginInfo = pluginInfo;
            this.source = source;
            this.installVersion = installVersion;
            this.alreadyInstalledVersion = installVersion;
            this.installable = true;
            this.removeExtensions = false;
            this.alreadyInstalled = false;
            this.message = null;
            this.pacDownloadUrl = null;
        }

        public PluginInstallInfoBuilder notInstallable()
        {
            this.installable = false;
            return this;
        }

        public PluginInstallInfoBuilder removeExtensions()
        {
            this.removeExtensions = true;
            return this;
        }

        public PluginInstallInfoBuilder alreadyInstalled(String version)
        {
            this.alreadyInstalled = true;
            this.alreadyInstalledVersion = version;
            return this;
        }

        public PluginInstallInfoBuilder message(String message)
        {
            this.message = message;
            return this;
        }

        public PluginInstallInfoBuilder pacDownloadUrl(URL url)
        {
            this.pacDownloadUrl = url;
            return this;
        }
        public PluginInstallInfo build()
        {
            return new PluginInstallInfo(this.pluginInfo,this.installable,this.removeExtensions,this.alreadyInstalled,this.message,this.source,this.installVersion, this.alreadyInstalledVersion, this.pacDownloadUrl);
        }
    }
}
