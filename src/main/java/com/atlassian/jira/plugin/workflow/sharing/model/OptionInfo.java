package com.atlassian.jira.plugin.workflow.sharing.model;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since version
 */
public class OptionInfo implements Serializable
{
    private static final long serialVersionUID = 518427166830023629L;
    private String originalId;
    private String value;
    private String sequence;
    private Boolean disabled;
    private List<OptionInfo> childOptions;

    @JsonCreator
    public OptionInfo(@JsonProperty("originalId") String originalId
            ,@JsonProperty("value") String value
            ,@JsonProperty("sequence") String sequence
            ,@JsonProperty("disabled") Boolean disabled
            ,@JsonProperty("childOptions") List<OptionInfo> childOptions)
    {
        this.originalId = originalId;
        this.value = value;
        this.sequence = sequence;
        this.disabled = disabled;
        this.childOptions = childOptions;
    }

    public String getOriginalId()
    {
        return originalId;
    }

    public String getValue()
    {
        return value;
    }

    public String getSequence()
    {
        return sequence;
    }

    public Boolean getDisabled()
    {
        return disabled;
    }

    public List<OptionInfo> getChildOptions()
    {
        return childOptions;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(originalId).append(value).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this) { return true; }

        if(!(obj instanceof OptionInfo)) { return false; }

        OptionInfo info = (OptionInfo) obj;

        return new EqualsBuilder().append(this.getOriginalId(), info.getOriginalId()).append(this.getValue(), info.getValue()).isEquals();
    }

    @Override
    public String toString()
    {
        return "OptionInfo value:" + value;//NON-NLS
    }
}
