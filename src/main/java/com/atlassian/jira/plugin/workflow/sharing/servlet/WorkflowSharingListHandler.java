package com.atlassian.jira.plugin.workflow.sharing.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.plugin.workflow.sharing.ParentWorkflowChecker;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

/**
 * @since version
 */
public class WorkflowSharingListHandler  extends AbstractServletHandler
{
    public static final String RESULT_TEMPLATE = "/templates/workflow-sharing/sharing-list.vm";
    
    private final WorkflowManager workflowManager;
    private final ParentWorkflowChecker parentWorkflowChecker;
    private final VelocityRequestContextFactory requestContextFactory;
    private final TemplateRenderer renderer;
    
    protected WorkflowSharingListHandler(LoginUriProvider loginUriProvider, WebSudoManager webSudoManager, UserManager userManager, WorkflowManager workflowManager, ParentWorkflowChecker parentWorkflowChecker, VelocityRequestContextFactory requestContextFactory, TemplateRenderer renderer)
    {
        super(loginUriProvider, webSudoManager, userManager);
        this.workflowManager = workflowManager;
        this.parentWorkflowChecker = parentWorkflowChecker;
        this.requestContextFactory = requestContextFactory;
        this.renderer = renderer;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        enforceAdminLogin(request, response);

        response.setContentType("text/html;charset=utf-8");//NON-NLS

        //make sure login redirect info is cleared
        super.clearSessionAttributes(request.getSession());

        final Map<String, Object> context = new HashMap<String, Object>();

        context.put("requestContext", requestContextFactory.getJiraVelocityRequestContext());//NON-NLS
        context.put("parentWorkflowChecker",parentWorkflowChecker);//NON-NLS
        context.put("workflows",getWorkflowsIncludingDrafts());//NON-NLS

        renderer.render(RESULT_TEMPLATE, context, response.getWriter());
    }

    private List<JiraWorkflow> getWorkflowsIncludingDrafts()
    {
        final List<JiraWorkflow> allWorkflows = workflowManager.getWorkflowsIncludingDrafts();
        
        //only include drafts if their parent is active
        Iterable<JiraWorkflow> filteredWorkflows = Iterables.filter(allWorkflows,new Predicate<JiraWorkflow>() {
            @Override
            public boolean apply(JiraWorkflow jiraWorkflow)
            {
                if(null == jiraWorkflow)
                {
                    return false;
                }
                
                if(jiraWorkflow.isDraftWorkflow() && !parentWorkflowChecker.isParentWorkflowActive(jiraWorkflow))
                {
                    return false;
                }
                else 
                {
                    return true;
                }
            }
        });
                
        return ImmutableList.copyOf(filteredWorkflows);
    }
}
