package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import java.util.Collection;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.plugin.workflow.sharing.importer.WorkflowImportReport;
import com.atlassian.mail.Email;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.mail.queue.SingleMailQueueItem;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;

import com.icl.saxon.functions.Not;

import org.apache.commons.lang.StringUtils;

/**
 * @since version
 */
public class ReportEmailer
{
    private static final String EOL = System.getProperty("line.separator");
    private final MailQueue mailQueue;
    private final ApplicationProperties applicationProperties;
    private final I18nResolver i18n;

    public ReportEmailer(MailQueue mailQueue, ApplicationProperties applicationProperties, I18nResolver i18n)
    {
        this.mailQueue = mailQueue;
        this.applicationProperties = applicationProperties;
        this.i18n = i18n;
    }

    public void addMailToQueue(Collection<NotificationRecipient> recipients,String subject, WorkflowImportReport report, String senderEmail, String senderName)
    {
        if(!recipients.isEmpty())
        {
            for(NotificationRecipient recipient : recipients)
            {
                User recipientUser = recipient.getUserRecipient();
                String recipientEmail = recipient.getEmail();
                String format = recipient.getFormat();
                
                if(StringUtils.isNotBlank(recipientEmail))
                {
                    Email email = createEmail(recipientEmail,senderEmail,senderName,subject,report,format);
                    SingleMailQueueItem item = new SingleMailQueueItem(email);
                    mailQueue.addItem(item);
                }
            }
        }
    }

    private Email createEmail(String recipientEmail, String senderEmail, String senderName, String subject, WorkflowImportReport report, String format)
    {
        String mimeType = getMimeTypeForFormat(format);
        String myUrl = applicationProperties.getBaseUrl() + "/plugins/servlet/wfshare-import/view-report?report=" + report.getReportName();//NON-NLS
        String htmllink = i18n.getText("wfshare.report.you.can.view.this.report.online.at") + EOL + "<a href='" + myUrl + "'>" + myUrl + "</a>" + EOL + EOL;//NON-NLS
        String textlink = i18n.getText("wfshare.report.you.can.view.this.report.online.at") + EOL + myUrl + EOL + EOL;
        String body;
        
        if("html".equals(format))
        {
            body = htmllink + report.asHtml();
        }
        else
        {
            body = textlink + report.asMarkdown();
        }
        
        Email email = new Email(recipientEmail);
        email.setFrom(senderEmail);
        email.setFromName(senderName);
        email.setSubject(subject);
        email.setBody(body);
        email.setMimeType(mimeType);
        
        return email;
    }

    private String getMimeTypeForFormat(String format)
    {
        if ("html".equals(format))
        {
            return "text/html";
        }
        else if ("text".equals(format))
        {
            return "text/plain";
        }
        else
        {
            return format; // HACK: until we can look up the format -> mimetype mapping, allow users to specify a MIME type directly.
        }
    }
}
