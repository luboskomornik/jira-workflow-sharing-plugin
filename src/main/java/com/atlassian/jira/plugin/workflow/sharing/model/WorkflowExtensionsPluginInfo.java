package com.atlassian.jira.plugin.workflow.sharing.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since version
 */
public final class WorkflowExtensionsPluginInfo implements Serializable
{
    private static final long serialVersionUID = -143416769497598622L;
    private String name;
    private String key;
    private String description;
    private String version;
    private String vendorName;
    private String vendorUrl;
    private String artifactName;
    private List<String> classNames;

    @JsonCreator
    public WorkflowExtensionsPluginInfo(@JsonProperty("name") String name, @JsonProperty("key") String key, @JsonProperty("description") String description, @JsonProperty("version") String version, @JsonProperty("vendorName") String vendorName, @JsonProperty("vendorUrl") String vendorUrl, @JsonProperty("artifactName") String artifactName)
    {
        this.name = name;
        this.key = key;
        this.description = description;
        this.version = version;
        this.vendorName = vendorName;
        this.vendorUrl = vendorUrl;
        this.artifactName = artifactName;
        this.classNames = new ArrayList<String>();
    }

    public List<String> getClassNames()
    {
        return classNames;
    }

    public String getName()
    {
        return name;
    }

    public String getKey()
    {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public String getVersion() {
        return version;
    }

    public String getVendorName() {
        return vendorName;
    }

    public String getVendorUrl() {
        return vendorUrl;
    }

    public String getArtifactName()
    {
        return artifactName;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(name).append(key).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this) { return true; }

        if(!(obj instanceof WorkflowExtensionsPluginInfo)) { return false; }

        WorkflowExtensionsPluginInfo info = (WorkflowExtensionsPluginInfo) obj;

        return new EqualsBuilder().append(this.getName(),info.getName()).append(this.getKey(),info.getKey()).isEquals();
    }

    @Override
    public String toString()
    {
        return "WorkflowExtensionsPluginInfo name:" + name + " key:" + key;//NON-NLS
    }
}
