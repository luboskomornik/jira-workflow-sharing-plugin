package com.atlassian.jira.plugin.workflow.sharing.pac;

import java.util.List;

import com.atlassian.plugins.domain.model.plugin.PluginVersion;

/**
 * @since version
 */
public interface JWSPacClient
{
    List<PluginVersion> findCompatiblePluginVersionsByPluginKey(String pluginKey);
    boolean pacHasPlugin(String pluginKey);

    boolean isVersionCompatible(String key, String version);

    PluginVersion getLatestCompatibleVersionByKey(String key);
}
