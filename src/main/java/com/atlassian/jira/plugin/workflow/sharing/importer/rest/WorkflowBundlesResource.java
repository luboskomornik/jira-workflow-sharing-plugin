package com.atlassian.jira.plugin.workflow.sharing.importer.rest;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;

import org.apache.commons.io.IOUtils;

/**
 * @since version
 */
@Path("/workflowbundles")
public class WorkflowBundlesResource
{
    private static final String WORKFLOW_BUNDLE_URL = "https://marketplace.atlassian.com/rest/1.0/plugins/";
    private static final String WORKFLOW_BUNDLE_LIST_URL = WORKFLOW_BUNDLE_URL + "recent?category=Workflow+Bundles";
    
    private final RequestFactory<?> requestFactory;

    public WorkflowBundlesResource(RequestFactory<?> requestFactory)
    {
        this.requestFactory = requestFactory;
    }


    @Path("/summary/{page}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWorkflowBundles(@PathParam("page") Integer page)
    {
        String fullUrl = WORKFLOW_BUNDLE_LIST_URL + "&page=" + page.toString();//NON-NLS
        Request request = requestFactory.createRequest(Request.MethodType.GET, fullUrl);
        final PluginJsonHolder holder = new PluginJsonHolder();
        try
        {
            request.execute(new ResponseHandler()
            {
                @Override
                public void handle(com.atlassian.sal.api.net.Response response) throws ResponseException
                {
                    try
                    {
                        String pluginsJson = IOUtils.toString(response.getResponseBodyAsStream());
                        holder.setJson(pluginsJson);
                    }
                    catch (IOException e)
                    {
                        throw new ResponseException(e);
                    }
                }
            });
        }
        catch (ResponseException e)
        {
            return Response.serverError().build();
        }
        
        return Response.ok(holder.getJson()).build();
    }

    @Path("/details/{pluginKey}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBundleDetails(@PathParam("pluginKey") String pluginKey)
    {
        String fullUrl = WORKFLOW_BUNDLE_URL + pluginKey;
        Request request = requestFactory.createRequest(Request.MethodType.GET, fullUrl);
        final PluginJsonHolder holder = new PluginJsonHolder();
        try
        {
            request.execute(new ResponseHandler()
            {
                @Override
                public void handle(com.atlassian.sal.api.net.Response response) throws ResponseException
                {
                    try
                    {
                        String pluginsJson = IOUtils.toString(response.getResponseBodyAsStream());
                        holder.setJson(pluginsJson);
                    }
                    catch (IOException e)
                    {
                        throw new ResponseException(e);
                    }
                }
            });
        }
        catch (ResponseException e)
        {
            return Response.serverError().build();
        }

        return Response.ok(holder.getJson()).build();
    }
    
    private class PluginJsonHolder
    {
        private String json;

        private PluginJsonHolder()
        {
            this.json = "{}";
        }

        public String getJson()
        {
            return json;
        }

        public void setJson(String json)
        {
            this.json = json;
        }
    }
}
