package com.atlassian.jira.plugin.workflow.sharing.pac;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.plugins.client.service.plugin.PluginVersionServiceClientImpl;

import org.apache.commons.httpclient.methods.GetMethod;

/**
 * @since version
 */
public class CustomPluginVersionService extends PluginVersionServiceClientImpl
{
    private static final String MARKETPLACE_BROWSE = "https://marketplace.atlassian.com/plugins/"; 
    public boolean hasPlugin(String pluginKey)
    {
        String uri = MARKETPLACE_BROWSE + pluginKey;
        GetMethod method = createGetMethod(uri);
        
        executeMethodIgnoreOutput(method);
        
        return (method.getStatusCode() != HttpServletResponse.SC_NOT_FOUND);
    }
}
