package com.atlassian.jira.plugin.workflow.sharing.exporter.component;

import java.io.File;
import java.io.IOException;

public interface JiraWorkflowSharingExporter {

    File exportActiveWorkflow(String name, boolean includePluginJars,String notes,boolean allowGoogleTracking) throws Exception;
    File exportDraftWorflow(String name, boolean includePluginJars,String notes,boolean allowGoogleTracking) throws Exception;
    File getExportedWorkflowFile(String filename);
}