package com.atlassian.jira.plugin.workflow.sharing.importer;

import java.io.Serializable;

/**
 * @since version
 */
public class StatusMapping implements Serializable
{
    private static final long serialVersionUID = -3138129003596652280L;
    private String originalId;
    private String originalName;
    private String newName;
    private String newId;

    public StatusMapping(String originalId, String originalName, String newId, String newName)
    {
        this.originalId = originalId;
        this.originalName = originalName;
        this.newName = newName;
        this.newId = newId;
    }

    public String getOriginalId()
    {
        return originalId;
    }

    public String getOriginalName()
    {
        return originalName;
    }

    public String getNewName()
    {
        return newName;
    }

    public String getNewId()
    {
        return newId;
    }

    public void setNewId(String id)
    {
        this.newId = id;
    }
}
