package com.atlassian.jira.plugin.workflow.sharing;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenLayoutItem;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.plugin.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.ScreenItemInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.ScreenTabInfo;
import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.opensymphony.workflow.loader.ActionDescriptor;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * @since version
 */
public class WorkflowScreensHelperImpl implements WorkflowScreensHelper
{
    private final FieldScreenManager fieldScreenManager;
    private final WorkflowCustomFieldsHelper customFieldsHelper;

    public WorkflowScreensHelperImpl(FieldScreenManager fieldScreenManager, WorkflowCustomFieldsHelper customFieldsHelper)
    {
        this.fieldScreenManager = fieldScreenManager;
        this.customFieldsHelper = customFieldsHelper;
    }

    @Override
    public Set<String> getClassnamesForScreen(String screenId)
    {
        Set<String> classNames = new HashSet<String>();
        if(StringUtils.isNumeric(screenId))
        {
            FieldScreen screen = fieldScreenManager.getFieldScreen(Long.parseLong(screenId));

            for(FieldScreenTab tab : screen.getTabs())
            {
                List<FieldScreenLayoutItem> items = tab.getFieldScreenLayoutItems();

                for(FieldScreenLayoutItem item : items)
                {
                    String cfClassname = customFieldsHelper.getCustomFieldTypeClassname(item.getFieldId());
                    if(StringUtils.isNotBlank(cfClassname))
                    {
                        classNames.add(cfClassname);
                    }
                }
            }
        }

        return classNames;
    }

    @Override
    public Set<String> getClassnamesForScreenFromAction(ActionDescriptor action)
    {
        Set<String> classNames = new HashSet<String>();
        
        if(FIELDSCREEN_VIEW.equals(action.getView()) && action.getMetaAttributes().containsKey(WorkflowScreensHelper.FIELDSCREEN_ID_NAME))
        {
            String screenId = (String) action.getMetaAttributes().get(FIELDSCREEN_ID_NAME);
            classNames.addAll(getClassnamesForScreen(screenId));
        }
        
        return classNames;
    }
    
    @Override
    public String[] getCustomFieldIdsForWorkflowScreens(JiraWorkflow workflow)
    {
        Set<String> ids = new HashSet<String>();

        Collection<ActionDescriptor> allActions = workflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            if(FIELDSCREEN_VIEW.equals(action.getView()) && action.getMetaAttributes().containsKey(FIELDSCREEN_ID_NAME))
            {
                String screenId = (String) action.getMetaAttributes().get(FIELDSCREEN_ID_NAME);

                if(StringUtils.isNumeric(screenId))
                {
                    FieldScreen screen = fieldScreenManager.getFieldScreen(Long.parseLong(screenId));

                    for(FieldScreenTab tab : screen.getTabs())
                    {
                        List<FieldScreenLayoutItem> items = tab.getFieldScreenLayoutItems();

                        for(FieldScreenLayoutItem item : items)
                        {
                            ids.add(item.getFieldId());
                        }
                    }
                }
            }
        }
        
        return ids.toArray(new String[0]);
    }

    @Override
    public void updateWorkflowScreenIds(ConfigurableJiraWorkflow jiraWorkflow, Map<Long, Long> oldToNewIdMapping)
    {
        Collection<ActionDescriptor> allActions = jiraWorkflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            if(FIELDSCREEN_VIEW.equals(action.getView()) && action.getMetaAttributes().containsKey(FIELDSCREEN_ID_NAME))
            {
                String screenId = (String) action.getMetaAttributes().get(FIELDSCREEN_ID_NAME);

                if(StringUtils.isNumeric(screenId))
                {
                    Long oldScreenId = Long.parseLong(screenId);
                    if(oldToNewIdMapping.containsKey(oldScreenId))
                    {
                        action.getMetaAttributes().put(FIELDSCREEN_ID_NAME,Long.toString(oldToNewIdMapping.get(oldScreenId)));
                        
                    }
                }
            }
        }
    }

    @Override
    public String getScreensJson(JiraWorkflow workflow) throws IOException
    {
        List<ScreenInfo> screenInfoList = new ArrayList<ScreenInfo>();
        
        Collection<ActionDescriptor> allActions = workflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            if(FIELDSCREEN_VIEW.equals(action.getView()) && action.getMetaAttributes().containsKey(FIELDSCREEN_ID_NAME))
            {
                final String screenId = (String) action.getMetaAttributes().get(FIELDSCREEN_ID_NAME);

                if(StringUtils.isNumeric(screenId) && Collections2.filter(screenInfoList, new Predicate<ScreenInfo>()
                {
                    @Override
                    public boolean apply(ScreenInfo input)
                    {
                        return input.getOriginalId().toString().equals(screenId);
                    }
                }).isEmpty())
                {
                    FieldScreen screen = fieldScreenManager.getFieldScreen(Long.parseLong(screenId));
                    
                    List<ScreenTabInfo> tabs = new ArrayList<ScreenTabInfo>(screen.getTabs().size());
                    
                    for(FieldScreenTab tab : screen.getTabs())
                    {
                        List<FieldScreenLayoutItem> items = tab.getFieldScreenLayoutItems();
                        
                        List<ScreenItemInfo> tabItems = new ArrayList<ScreenItemInfo>(items.size());
                        
                        for(FieldScreenLayoutItem item : items)
                        {
                            tabItems.add(new ScreenItemInfo(item.getId(),item.getFieldId(),item.getPosition()));
                        }
                        
                        tabs.add(new ScreenTabInfo(tab.getId(),tab.getName(),tab.getPosition(),tabItems));
                    }
                    
                    screenInfoList.add(new ScreenInfo(screen.getId(),screen.getName(),screen.getDescription(),tabs));
                }
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        final StringWriter sw = new StringWriter();

        mapper.writeValue(sw,screenInfoList);

        return sw.toString();
    }

   
}
