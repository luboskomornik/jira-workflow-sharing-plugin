package com.atlassian.jira.plugin.workflow.sharing.servlet;

/**
 * @since version
 */
public class WorkflowSharingListServlet extends BaseHandlerServlet
{

    public WorkflowSharingListServlet(WorkflowSharingListHandler handler)
    {
        super(handler);
    }
}
