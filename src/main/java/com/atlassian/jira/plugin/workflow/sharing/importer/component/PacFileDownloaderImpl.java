package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;

import com.atlassian.plugins.client.service.HttpClientFactory;
import com.atlassian.sal.api.message.I18nResolver;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 * @since version
 */
public class PacFileDownloaderImpl implements PacFileDownloader
{
    protected final HttpClient client;
    private final I18nResolver i18n;

    public PacFileDownloaderImpl(I18nResolver i18n)
    {
        this.i18n = i18n;
        this.client = HttpClientFactory.createHttpClient();
    }

    @Override
    public void downloadFile(String downloadUri, File outputFile) throws Exception
    {
        GetMethod method = new GetMethod(downloadUri);
        int status = client.executeMethod(method);
        
        if(HttpStatus.SC_OK != status)
        {
            throw new Exception(i18n.getText("wfshare.exception.download.error",status));
        }

        InputStream is = null;
        FileOutputStream fos = null;
        
        try
        {
            is = method.getResponseBodyAsStream();
            fos = new FileOutputStream(outputFile);
            IOUtils.copy(is, fos);
        }
        finally
        {
            IOUtils.closeQuietly(fos);
            IOUtils.closeQuietly(is);
        }
    }
}
