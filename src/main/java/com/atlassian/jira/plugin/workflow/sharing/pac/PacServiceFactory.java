package com.atlassian.jira.plugin.workflow.sharing.pac;

import com.atlassian.plugins.service.plugin.PluginService;
import com.atlassian.plugins.service.plugin.PluginVersionService;
import com.atlassian.plugins.service.product.ProductService;

/**
 * @since version
 */
public interface PacServiceFactory
{
    PluginVersionService getPluginVersionService();
    PluginService getPluginService();
    ProductService getProductService();
}
