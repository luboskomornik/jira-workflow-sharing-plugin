package com.atlassian.jira.plugin.workflow.sharing;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.plugin.Plugin;

public interface WorkflowExtensionsHelper {

    String requiredPluginsToJson(Map<Plugin, Set<String>> requiredPluginsWithClassnames) throws Exception;

    Map<Plugin, Set<String>> getRequiredPlugins(JiraWorkflow workflow);
    
    List<File> getPluginArtifactFiles(Set<Plugin> plugins) throws Exception;

    JiraWorkflow getWorkflowForNameAndMode(String name, String mode);

    ExtensionRemovalResult removeExtensionsWithClassnames(JiraWorkflow jiraWorkflow,Set<String> classnames);

    ExtensionCustomFieldUpdateResult updateWorkflowCustomFieldIds(ConfigurableJiraWorkflow jiraWorkflow, Map<String, String> oldToNewIdMapping);

    public interface ExtensionRemovalResult 
    {
        Set<String> getActions();
        Map<String,Set<String>> getPostFunctions(String action);
        Map<String,Set<String>> getValidators(String action);
        Map<String,Set<String>> getConditions(String action);
    }
    
    public interface ExtensionCustomFieldUpdateResult
    {

        Set<String> getActions();

        Map<String,Map<String,String>> getPostFunctions(String action);

        Map<String,Map<String,String>> getValidators(String action);

        Map<String,Map<String,String>> getConditions(String action);
    }
    
}