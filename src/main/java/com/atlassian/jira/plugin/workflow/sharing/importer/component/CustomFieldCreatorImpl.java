package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldConfigurationScheme;
import com.atlassian.jira.plugin.workflow.sharing.model.CustomFieldInfo;

import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;

/**
 * @since version
 */
public class CustomFieldCreatorImpl implements CustomFieldCreator
{
    private final CustomFieldManager customFieldManager;
    private final FieldConfigSchemeManager fieldConfigSchemeManager;

    public CustomFieldCreatorImpl(CustomFieldManager customFieldManager, FieldConfigSchemeManager fieldConfigSchemeManager)
    {
        this.customFieldManager = customFieldManager;
        this.fieldConfigSchemeManager = fieldConfigSchemeManager;
    }

    @Override
    public CustomField createCustomField(CustomFieldInfo fieldInfo) throws GenericEntityException
    {
        CustomFieldType cfType = customFieldManager.getCustomFieldType(fieldInfo.getTypeModuleKey());
        CustomFieldSearcher searcher = null;
        
        if(StringUtils.isNotBlank(fieldInfo.getSearcherModuleKey()))
        {
            searcher = customFieldManager.getCustomFieldSearcher(fieldInfo.getSearcherModuleKey());
        }
        
        List issueTypes = new ArrayList(1);
        issueTypes.add(null);
        
        return customFieldManager.createCustomField(
                fieldInfo.getName()
                ,fieldInfo.getDescription()
                ,cfType
                ,searcher
                , Arrays.asList(GlobalIssueContext.getInstance())
                , issueTypes);
    }

    @Override
    public void removeCustomFieldAndSchemes(CustomField field) throws RemoveException
    {
        List<FieldConfigScheme> schemes = field.getConfigurationSchemes();
        
        for(FieldConfigScheme scheme : schemes)
        {
            fieldConfigSchemeManager.removeFieldConfigScheme(scheme.getId());
        }
        
        customFieldManager.removeCustomField(field);
    }
}
