package com.atlassian.jira.plugin.workflow.sharing.servlet;

import java.io.IOException;
import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;

import org.apache.commons.lang.StringUtils;

/**
 * @since version
 */
public abstract class AbstractServletHandler implements ServletHandler
{
    static final String JIRA_SERAPH_SECURITY_ORIGINAL_URL = "os_security_originalurl";
    static final String CONF_SERAPH_SECURITY_ORIGINAL_URL = "seraph_originalurl";

    private final LoginUriProvider loginUriProvider;
    private final WebSudoManager webSudoManager;
    private final UserManager userManager;

    protected AbstractServletHandler(LoginUriProvider loginUriProvider, WebSudoManager webSudoManager, UserManager userManager)
    {
        this.loginUriProvider = loginUriProvider;
        this.webSudoManager = webSudoManager;
        this.userManager = userManager;
    }

    protected boolean enforceAdminLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        boolean needsAdmin = false;
        try
        {
            webSudoManager.willExecuteWebSudoRequest(request);
            if(!isAdmin())
            {
                redirectToLogin(request,response);
                needsAdmin = true;
            }
        }
        catch (WebSudoSessionException wse)
        {
            webSudoManager.enforceWebSudoProtection(request, response);
            needsAdmin = true;
        }
        
        return needsAdmin;
    }

    protected boolean isAdmin()
    {
        if (userManager.getRemoteUsername() == null)
        {
            return false;
        }
        return userManager.isSystemAdmin(userManager.getRemoteUsername()) || userManager.isAdmin(userManager.getRemoteUsername());
    }

    protected boolean isSystemAdmin()
    {
        if (userManager.getRemoteUsername() == null)
        {
            return false;
        }
        return userManager.isSystemAdmin(userManager.getRemoteUsername());
    }

    protected void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        final URI uri = getUri(request);
        addSessionAttributes(request, uri.toASCIIString());
        response.sendRedirect(loginUriProvider.getLoginUri(uri).toASCIIString());
    }

    protected URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    protected <T> T getSessionAttribute(HttpServletRequest request, String name, String errorMsg) throws Exception
    {
        T type = null;
        try
        {
            type = (T) request.getSession().getAttribute(name);
            if(null == type)
            {
                throw new Exception(errorMsg);
            }
        }
        catch (Exception e)
        {
            throw new Exception(errorMsg);
        }

        return type;
    }

    protected <T> T getSessionAttributeOrNull(HttpServletRequest request, String name) throws Exception
    {
        T type = null;
        type = (T) request.getSession().getAttribute(name);
        
        return type;
    }

    private void addSessionAttributes(final HttpServletRequest request, final String uriString)
    {
        request.getSession().setAttribute(JIRA_SERAPH_SECURITY_ORIGINAL_URL, uriString);
        request.getSession().setAttribute(CONF_SERAPH_SECURITY_ORIGINAL_URL, uriString);
    }

    protected void clearSessionAttributes(final HttpSession session)
    {
        session.removeAttribute(JIRA_SERAPH_SECURITY_ORIGINAL_URL);
        session.removeAttribute(CONF_SERAPH_SECURITY_ORIGINAL_URL);
    }

    protected String getMappingPath(HttpServletRequest request)
    {
        String lastPath = StringUtils.substringAfterLast(request.getRequestURI(), "/");
        if (StringUtils.isBlank(lastPath))
        {
            lastPath = StringUtils.substringBeforeLast(request.getRequestURI(), "/");
        }

        return lastPath;
    }
}
