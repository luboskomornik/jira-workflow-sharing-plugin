package com.atlassian.jira.plugin.workflow.sharing.service;

import com.atlassian.configurable.ObjectConfiguration;
import com.atlassian.configurable.ObjectConfigurationException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.workflow.sharing.exporter.component.JiraWorkflowSharingExporter;
import com.atlassian.jira.plugin.workflow.sharing.util.ZipUtils;
import com.atlassian.jira.service.AbstractService;
import com.atlassian.jira.workflow.JiraWorkflow;
import org.apache.commons.lang.StringUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.StatusCommand;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
public class WorkflowSharingVersioningService extends AbstractService implements BeanFactoryAware {
    private static Logger LOG = LoggerFactory.getLogger(WorkflowSharingVersioningService.class);
    private static String PROPERTY_VERSIONING_DIRECTORY = "VERSIONING_DIRECTORY";
    private static String PROPERTY_WF_NAME = "WF_NAME";
    private static String PROPERTY_GIT_USERNAME = "GIT_USERNAME";
    private static String PROPERTY_GIT_PASSWORD = "GIT_PASSWORD";
    private JiraWorkflowSharingExporter jiraWorkflowSharingExporter;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        if (jiraWorkflowSharingExporter == null) {
            try {
                jiraWorkflowSharingExporter = (JiraWorkflowSharingExporter) beanFactory.getBean("jira-workflow-sharing-exporter");
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public void run() {
        if (ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser() == null) {
            ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(ComponentAccessor.getUserManager().getUserObject("admin"));
        }
        if (jiraWorkflowSharingExporter != null) {
            try {
                String versioningDirectory = getProperty(PROPERTY_VERSIONING_DIRECTORY);
                String wfNames = getProperty(PROPERTY_WF_NAME);
                String gitUsername = getProperty(PROPERTY_GIT_USERNAME);
                String gitPassword = getProperty(PROPERTY_GIT_PASSWORD);
                final boolean includePluginJars = true;
                final boolean allowsGoogleTracking = false;
                final String notes = null;

                if (versioningDirectory == null || !new File(versioningDirectory).exists()) {
                    LOG.error("Versioning directory must be specified and exist on filesystem!");
                    return;
                }
                List<JiraWorkflow> workflows = new ArrayList<JiraWorkflow>();
                if (!StringUtils.isBlank(wfNames)) {
                    String[] wfNamesArray = wfNames.split(",");
                    for (String wfName : wfNamesArray) {
                        JiraWorkflow wf = ComponentAccessor.getWorkflowManager().getWorkflow(wfName.trim());
                        if (wf != null) {
                            workflows.add(wf);
                        }
                    }
                } else {
                    workflows.addAll(ComponentAccessor.getWorkflowManager().getWorkflowsIncludingDrafts());
                }
                if (!workflows.isEmpty()) {
                    for (JiraWorkflow wf : workflows) {
                        File exportedZip;
                        if (wf.isDraftWorkflow()) {
                            exportedZip = jiraWorkflowSharingExporter.exportDraftWorflow(wf.getName(), includePluginJars, notes, allowsGoogleTracking);
                        } else {
                            exportedZip = jiraWorkflowSharingExporter.exportActiveWorkflow(wf.getName(), includePluginJars, notes, allowsGoogleTracking);
                        }
                        if (exportedZip != null && exportedZip.exists()) {
                            ZipUtils.unzip(exportedZip, new File(versioningDirectory, exportedZip.getName()));
                        }
                    }
                    FileRepositoryBuilder builder = new FileRepositoryBuilder();
                    Repository repository = builder.setGitDir(new File(versioningDirectory, ".git")).readEnvironment().findGitDir().build();

                    Git git = new Git(repository);
                    StatusCommand sc = git.status();
                    Status status = sc.call();
                    if (!CollectionUtils.isEmpty(status.getAdded()) || !CollectionUtils.isEmpty(status.getChanged()) ||
                            !CollectionUtils.isEmpty(status.getModified()) || !CollectionUtils.isEmpty(status.getRemoved())) {
                        git.add().addFilepattern(".").call(); // '.' means addAll
                        git.commit().setMessage("WF Export at " + new Date()).call();
                        if (gitUsername != null && gitPassword != null) {
                            CredentialsProvider.setDefault(new UsernamePasswordCredentialsProvider(gitUsername, gitPassword));
                            git.push().call();
                        } else {
                            LOG.warn("For pushing commits to github server must be specified github account credentials!");
                        }
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public ObjectConfiguration getObjectConfiguration() throws ObjectConfigurationException {
        return getObjectConfiguration("ws-versioningservice", "/com/atlassian/jira/plugin/workflow/sharing/service/ws-versioning.xml", null);
    }
}
