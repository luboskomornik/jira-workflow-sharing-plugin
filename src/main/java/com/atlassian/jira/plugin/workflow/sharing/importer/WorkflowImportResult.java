package com.atlassian.jira.plugin.workflow.sharing.importer;

import com.atlassian.jira.workflow.JiraWorkflow;

/**
 * @since version
 */
public class WorkflowImportResult
{
    private JiraWorkflow jiraWorkflow;
    private WorkflowImportReport report;

    public WorkflowImportResult(JiraWorkflow jiraWorkflow, WorkflowImportReport report)
    {
        this.jiraWorkflow = jiraWorkflow;
        this.report = report;
    }

    public JiraWorkflow getJiraWorkflow()
    {
        return jiraWorkflow;
    }

    public WorkflowImportReport getReport()
    {
        return report;
    }
}
