package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import java.io.File;
import java.util.*;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugin.workflow.sharing.importer.StatusMapping;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.atlassian.sal.api.message.I18nResolver;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.sysbliss.jira.plugins.workflow.util.StatusUtils;

import org.apache.commons.io.FileUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;

/**
 * @since version
 */
public class WorkflowStatusHelperImpl implements WorkflowStatusHelper
{
    private final ConstantsManager constantsManager;
    private final StatusUtils statusUtils;
    private final I18nResolver i18n;
    
    public WorkflowStatusHelperImpl(ConstantsManager constantsManager, I18nResolver i18n)
    {
        this.constantsManager = constantsManager; 
        this.i18n = i18n;
        this.statusUtils = new StatusUtils();
    }

    @Override
    public Map<String,StatusMapping> getStatusHolders(File xmlFile) throws Exception
    {
        Collection<Status> jiraStatuses = constantsManager.getStatusObjects();

        Map<String,StatusMapping> holders = new HashMap<String, StatusMapping>();

        String xml = FileUtils.readFileToString(xmlFile);
        WorkflowDescriptor descriptor = WorkflowUtil.convertXMLtoWorkflowDescriptor(xml);

        List<StepDescriptor> steps = descriptor.getSteps();

        for(StepDescriptor step : steps)
        {
            if ((step.getMetaAttributes() != null) && step.getMetaAttributes().containsKey(JiraWorkflow.STEP_STATUS_KEY))
            {
                String statusId = (String) step.getMetaAttributes().get(JiraWorkflow.STEP_STATUS_KEY);
                try
                {
                    Status found = Iterables.find(jiraStatuses,new StatusNamePredicate(step.getName()));
                    holders.put(statusId,new StatusMapping(statusId,step.getName(),found.getId(),getSuggestedName(jiraStatuses,found.getName())));
                }
                catch (NoSuchElementException e)
                {
                    holders.put(statusId, new StatusMapping(statusId, step.getName(), "-1", step.getName()));
                }
            }
        }

        return holders;
    }

    private String getSuggestedName(Collection<Status> jiraStatuses, String name)
    {
        List<String> statusNames = new ArrayList<String>(Collections2.transform(jiraStatuses, new Function<Status, String>()
        {
            @Override
            public String apply(Status status)
            {
                return (null == status) ? "" : status.getName();
            }
        }));
        
        String suggestedName = name;

        if(statusNames.contains(suggestedName))
        {
            int x = 2;
            while (statusNames.contains(suggestedName))
            {
                suggestedName = name + " " + String.valueOf(x);
                x++;
            }
        }
        
        return suggestedName;
    }

    @Override
    public List<Status> getJiraStatuses()
    {
        return ImmutableList.copyOf(constantsManager.getStatusObjects());
    }

    @Override
    public void removeStatus(String id) throws GenericEntityException
    {
        statusUtils.deleteStatus(id);
    }

    @Override
    public Status createStatus(String newName, String desc, String newStatusDefaultIcon) throws GenericEntityException
    {
        GenericValue gv = statusUtils.addStatus(newName,desc,newStatusDefaultIcon);
        return constantsManager.getStatusObject(gv.getString("id"));//NON-NLS
    }

    @Override
    public String getNameForStatusId(String id)
    {
        String name = i18n.getText("wfshare.exception.status.not.found");
        
        Status status = constantsManager.getStatusObject(id);
        if(null != status)
        {
            name = status.getName();
        }
        
        return name;
    }

    private class StatusNamePredicate implements Predicate<Status>
    {
        private final String name;

        private StatusNamePredicate(String name)
        {
            this.name = name;
        }

        @Override
        public boolean apply(Status status)
        {
            return name.equals(status.getName());
        }
    }
    
}
