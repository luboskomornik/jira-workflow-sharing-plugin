package com.atlassian.jira.plugin.workflow.sharing;

import java.io.File;
import java.io.StringWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.workflow.WorkflowService;
import com.atlassian.jira.plugin.workflow.sharing.model.WorkflowExtensionsPluginInfo;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.sal.api.message.I18nResolver;

import com.google.common.collect.Sets;
import com.opensymphony.workflow.loader.*;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WorkflowExtensionsHelperImpl implements WorkflowExtensionsHelper
{
    private static final Logger log = LoggerFactory.getLogger("atlassian-plugin");//NON-NLS
    
    private final ModuleDescriptorLocator moduleDescriptorLocator;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final WorkflowService workflowService;
    private final WorkflowScreensHelper workflowScreensHelper;
    private final WorkflowCustomFieldsHelper workflowCustomFieldsHelper;
    private final PluginArtifactFinder pluginArtifactFinder;
    private final I18nResolver i18n;

    public WorkflowExtensionsHelperImpl(ModuleDescriptorLocator moduleDescriptorLocator, JiraAuthenticationContext jiraAuthenticationContext, WorkflowService workflowService, WorkflowScreensHelper workflowScreensHelper, WorkflowCustomFieldsHelper workflowCustomFieldsHelper, PluginArtifactFinder pluginArtifactFinder, I18nResolver i18n) {

        this.moduleDescriptorLocator = moduleDescriptorLocator;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.workflowService = workflowService;
        this.workflowScreensHelper = workflowScreensHelper;
        this.workflowCustomFieldsHelper = workflowCustomFieldsHelper;
        this.pluginArtifactFinder = pluginArtifactFinder;
        this.i18n = i18n;
    }

    @Override
    public JiraWorkflow getWorkflowForNameAndMode(String name, String mode) {
        JiraWorkflow workflow = null;
        JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(jiraAuthenticationContext.getLoggedInUser());

        if ("draft".equalsIgnoreCase(mode))//NON-NLS
        {
            workflow = workflowService.getDraftWorkflow(jiraServiceContext, name);
        }
        else
        {
            workflow = workflowService.getWorkflow(jiraServiceContext, name);
        }

        return workflow;
    }

    @Override
    public ExtensionRemovalResult removeExtensionsWithClassnames(JiraWorkflow jiraWorkflow, Set<String> classesToRemove)
    {
        ExtensionRemovalResultImpl result = new ExtensionRemovalResultImpl();
        
        Collection<ActionDescriptor> allActions = jiraWorkflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            Map<String,Set<String>> removedFunctions = removePostFunctionsWithClassnames(action,classesToRemove);
            Map<String,Set<String>> removedValidators = removeValidatorsWithClassnames(action, classesToRemove);
            Map<String,Set<String>> removedConditions = removeConditionsWithClassnames(action, classesToRemove);
            
            result.addAction(action.getName() + "(" + action.getId() + ")",removedFunctions,removedValidators,removedConditions);
        }
        
        return result;
    }

    @Override
    public ExtensionCustomFieldUpdateResult updateWorkflowCustomFieldIds(ConfigurableJiraWorkflow jiraWorkflow, Map<String, String> oldToNewIdMapping)
    {
        ExtensionCustomFieldUpdateResultImpl result = new ExtensionCustomFieldUpdateResultImpl();
        
        Collection<ActionDescriptor> allActions = jiraWorkflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            Map<String,Map<String,String>> functions = updatePostFunctionCustomFieldIds(action,oldToNewIdMapping);
            Map<String,Map<String,String>> validators = updateValidatorCustomFieldIds(action,oldToNewIdMapping);
            Map<String,Map<String,String>> conditions = updateConditionsCustomFieldIds(action,oldToNewIdMapping);

            result.addAction(action.getName() + "(" + action.getId() + ")",functions,validators,conditions);
        }
        
        return result;
    }

    private Map<String,Set<String>> removeConditionsWithClassnames(ActionDescriptor action, Set<String> classesToRemove)
    {
        Map<String,Set<String>> result = new HashMap<String, Set<String>>();
        if(null != action.getRestriction())
        {
            ConditionsDescriptor rootConditions = action.getRestriction().getConditionsDescriptor();
            if(null != rootConditions)
            {
                
                removeConditionWithClassnames(rootConditions,classesToRemove,result);
            }
        }
        
        return result;
    }

    private Map<String,Set<String>> removeConditionWithClassnames(ConditionsDescriptor rootConditions, Set<String> classesToRemove, Map<String,Set<String>> result)
    {
        final List nestedConditions = rootConditions.getConditions();
        for (final Iterator iterator = nestedConditions.iterator(); iterator.hasNext(); )
        {
            final Object o = iterator.next();

            if (o instanceof ConditionDescriptor)
            {
                Set<Map.Entry<String,String>> args = (Set<Map.Entry<String,String>>) ((ConditionDescriptor) o).getArgs().entrySet();
                Set<String> usedClassnames = getClassnamesFromArgs(args);

                if(!Sets.intersection(classesToRemove,usedClassnames).isEmpty())
                {
                    String extensionClass = StringUtils.defaultIfEmpty((String)((ConditionDescriptor) o).getArgs().get("class.name"),((ConditionDescriptor) o).toString());//NON-NLS
                    result.put(extensionClass,classesToRemove);
                    
                    iterator.remove();
                }
            }
            else if (o instanceof ConditionsDescriptor)
            {
                removeConditionWithClassnames((ConditionsDescriptor) o,classesToRemove,result);
            }
        }
        
        return result;
    }

    private Map<String,Set<String>> removeValidatorsWithClassnames(ActionDescriptor action, Set<String> classesToRemove)
    {
        Map<String,Set<String>> result = new HashMap<String,Set<String>>();
        
        List<ValidatorDescriptor> validators = action.getUnconditionalResult().getValidators();
        validators.addAll(action.getValidators());

        Iterator<ValidatorDescriptor> it = validators.iterator();
        while(it.hasNext())
        {
            ValidatorDescriptor validator = it.next();
            Set<Map.Entry<String,String>> args = (Set<Map.Entry<String,String>>) validator.getArgs().entrySet();
            Set<String> usedClassnames = getClassnamesFromArgs(args);

            if(!Sets.intersection(classesToRemove,usedClassnames).isEmpty())
            {
                String extensionClass = StringUtils.defaultIfEmpty((String)validator.getArgs().get("class.name"),validator.toString());//NON-NLS
                result.put(extensionClass,classesToRemove);
                
                it.remove();
            }
        }
        
        return result;
    }

    private Map<String,Set<String>> removePostFunctionsWithClassnames(ActionDescriptor action, Set<String> classesToRemove)
    {
        Map<String,Set<String>> result = new HashMap<String,Set<String>>();
        
        Collection<FunctionDescriptor> postFunctions = action.getUnconditionalResult().getPostFunctions();
        postFunctions.addAll(action.getPostFunctions());

        Iterator<FunctionDescriptor> it = postFunctions.iterator();
        while(it.hasNext())
        {
            FunctionDescriptor postFunction = it.next();

            Set<Map.Entry<String,String>> args = (Set<Map.Entry<String,String>>) postFunction.getArgs().entrySet();
            Set<String> usedClassnames = getClassnamesFromArgs(args);
            
            if(!Sets.intersection(classesToRemove,usedClassnames).isEmpty())
            {
                String extensionClass = StringUtils.defaultIfEmpty((String)postFunction.getArgs().get("class.name"),postFunction.toString());//NON-NLS
                result.put(extensionClass,classesToRemove);
                
                it.remove();
            }
        }
        
        return result;
    }

    @Override
    public String requiredPluginsToJson(Map<Plugin, Set<String>> requiredPluginsWithClassnames) throws Exception
    {
        List<WorkflowExtensionsPluginInfo> infoList = getWorkflowExtensionsInfo(requiredPluginsWithClassnames);

        ObjectMapper mapper = new ObjectMapper();
        final StringWriter sw = new StringWriter();

        mapper.writeValue(sw,infoList);

        return sw.toString();
    }
    
    private List<WorkflowExtensionsPluginInfo> getWorkflowExtensionsInfo(Map<Plugin, Set<String>> requiredPluginsWithClassnames) throws Exception
    {
        List<WorkflowExtensionsPluginInfo> infoList = new ArrayList<WorkflowExtensionsPluginInfo>(requiredPluginsWithClassnames.size());
        for (Map.Entry<Plugin, Set<String>> entry : requiredPluginsWithClassnames.entrySet())
        {
            Plugin plugin = entry.getKey();
            
            if(!(plugin instanceof OsgiPlugin))
            {
                throw new Exception(i18n.getText("wfshare.exception.cannot.include.v1.plugin",plugin.getName()));
            }
            
            String artifactName = pluginArtifactFinder.findArtifact(plugin).getName();
            Set<String> classnames = entry.getValue();
            PluginInformation pinfo = plugin.getPluginInformation();

            WorkflowExtensionsPluginInfo info = new WorkflowExtensionsPluginInfo(plugin.getName(), plugin.getKey(), pinfo.getDescription(), pinfo.getVersion(), pinfo.getVendorName(), pinfo.getVendorUrl(),artifactName);
            for (String cname : classnames)
            {
                info.getClassNames().add(cname);
            }

            infoList.add(info);
        }

        return infoList;
    }

    @Override
    public Map<Plugin, Set<String>> getRequiredPlugins(JiraWorkflow workflow)
    {
        Map<Plugin, Set<String>> pluginExtensionMap = new HashMap<Plugin, Set<String>>();

        Set<String> extensionClassnames = getExtensionClassnames(workflow);
        for (String classname : extensionClassnames)
        {
            Collection<ModuleDescriptor> moduleDescriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleClassname(classname);
            for (ModuleDescriptor moduleDescriptor : moduleDescriptors)
            {
                Plugin plugin = moduleDescriptor.getPlugin();

                //make sure we ignore old v1 SYSTEM plugins (may not have the system flag set)
                if(plugin.getKey().startsWith("com.atlassian.jira.plugin.system"))//NON-NLS
                {
                    continue;
                }
                
                if (!pluginExtensionMap.containsKey(plugin))
                {
                    pluginExtensionMap.put(plugin, new HashSet<String>());
                }
                pluginExtensionMap.get(plugin).add(classname);
            }
        }

        return pluginExtensionMap;
    }

    @Override
    public List<File> getPluginArtifactFiles(Set<Plugin> plugins) throws Exception
    {
        List<File> artifacts = new ArrayList<File>();
        for (Plugin plugin : plugins)
        {

            if ((plugin instanceof OsgiPlugin))
            {
                File artifact = pluginArtifactFinder.findArtifact(plugin);
                
                artifacts.add(artifact);
            }
        }

        return artifacts;
    }

    private Set<String> getExtensionClassnames(JiraWorkflow workflow)
    {
        Set<String> uniqueClassnames = new HashSet<String>();
        
        Collection<ActionDescriptor> allActions = workflow.getAllActions();
        for (ActionDescriptor action : allActions)
        {
            uniqueClassnames.addAll(workflowScreensHelper.getClassnamesForScreenFromAction(action));
            
            uniqueClassnames.addAll(getFunctionClassnames(action));
            uniqueClassnames.addAll(getValidatorClassnames(action));

            if(null != action.getRestriction())
            {
                uniqueClassnames.addAll(getConditionClassnames(action.getRestriction().getConditionsDescriptor()));
            }
        }
        
        return uniqueClassnames;
    }

    private Set<String> getFunctionClassnames(ActionDescriptor action)
    {
        List<FunctionDescriptor> functions = action.getUnconditionalResult().getPostFunctions();
        functions.addAll(action.getPostFunctions());
        
        Set<String> classNames = new HashSet<String>();
        for (FunctionDescriptor function : functions)
        {
            Set<Map.Entry<String,String>> args = (Set<Map.Entry<String,String>>) function.getArgs().entrySet();
            classNames.addAll(getClassnamesFromArgs(args));
        }

        return classNames;
    }

    private Set<String> getValidatorClassnames(ActionDescriptor action)
    {
        List<ValidatorDescriptor> validators = action.getUnconditionalResult().getValidators();
        validators.addAll(action.getValidators());

        Set<String> classNames = new HashSet<String>();
        for (ValidatorDescriptor validator : validators)
        {
            Set<Map.Entry<String,String>> args = (Set<Map.Entry<String,String>>) validator.getArgs().entrySet();
            classNames.addAll(getClassnamesFromArgs(args));
        }

        return classNames;
    }

    private Set<String> getConditionClassnames(ConditionsDescriptor rootConditions)
    {
        Set<String> classNames = new HashSet<String>();

        if(null == rootConditions)
        {
            return classNames;
        }

        final List nestedConditions = rootConditions.getConditions();
        for (final Iterator iterator = nestedConditions.iterator(); iterator.hasNext(); )
        {
            final Object o = iterator.next();

            if (o instanceof ConditionDescriptor)
            {
                Set<Map.Entry<String,String>> args = (Set<Map.Entry<String,String>>) ((ConditionDescriptor) o).getArgs().entrySet();
                classNames.addAll(getClassnamesFromArgs(args));
                
            }
            else if (o instanceof ConditionsDescriptor)
            {
                classNames.addAll(getConditionClassnames((ConditionsDescriptor) o));
            }
        }

        return classNames;
    }

    private Map<String, Map<String, String>> updateConditionsCustomFieldIds(ActionDescriptor action, Map<String, String> oldToNewIdMapping)
    {
        Map<String,Map<String, String>> result = new HashMap<String, Map<String, String>>();
        
        if(null != action.getRestriction())
        {
            ConditionsDescriptor rootConditions = action.getRestriction().getConditionsDescriptor();
            if(null != rootConditions)
            {
                updateConditionsCustomFieldIds(rootConditions, oldToNewIdMapping, result);
            }
        }

        return result;
    }

    private void updateConditionsCustomFieldIds(ConditionsDescriptor rootConditions, Map<String, String> oldToNewIdMapping, Map<String, Map<String, String>> result)
    {
        if(null == rootConditions)
        {
            return;
        }

        final List nestedConditions = rootConditions.getConditions();
        for (final Iterator iterator = nestedConditions.iterator(); iterator.hasNext(); )
        {
            final Object o = iterator.next();

            if (o instanceof ConditionDescriptor)
            {
                Map<String,String> args = (Map<String,String>) ((ConditionDescriptor) o).getArgs();
                Map<String,String> updateMap = new HashMap<String, String>();
                
                for(Map.Entry<String,String> entry : args.entrySet())
                {
                    if(entry.getValue().startsWith(WorkflowCustomFieldsHelper.CUSTOM_FIELD_PREFIX) && oldToNewIdMapping.containsKey(entry.getValue()))
                    {
                        updateMap.put(entry.getKey(),oldToNewIdMapping.get(entry.getValue()));
                    }
                }
                
                if(!updateMap.isEmpty())
                {
                    for(Map.Entry<String,String> updateEntry : updateMap.entrySet())
                    {
                        args.put(updateEntry.getKey(),updateEntry.getValue());
                    }

                    String extensionClass = StringUtils.defaultIfEmpty((String)((ConditionDescriptor) o).getArgs().get("class.name"),((ConditionDescriptor) o).toString());//NON-NLS
                    result.put(extensionClass,updateMap);
                }

            }
            else if (o instanceof ConditionsDescriptor)
            {
                updateConditionsCustomFieldIds((ConditionsDescriptor) o,oldToNewIdMapping,result);
            }
        }
    }

    private Map<String, Map<String, String>> updateValidatorCustomFieldIds(ActionDescriptor action, Map<String, String> oldToNewIdMapping)
    {
        Map<String, Map<String, String>> result = new HashMap<String, Map<String, String>>();

        List<ValidatorDescriptor> validators = action.getUnconditionalResult().getValidators();
        validators.addAll(action.getValidators());
        
        for(ValidatorDescriptor validator : validators)
        {
            Map<String,String> args = (Map<String,String>) validator.getArgs();

            Map<String,String> updateMap = new HashMap<String, String>();

            for(Map.Entry<String,String> entry : args.entrySet())
            {
                if(entry.getValue().startsWith(WorkflowCustomFieldsHelper.CUSTOM_FIELD_PREFIX) && oldToNewIdMapping.containsKey(entry.getValue()))
                {
                    updateMap.put(entry.getKey(),oldToNewIdMapping.get(entry.getValue()));
                }
            }

            if(!updateMap.isEmpty())
            {
                for(Map.Entry<String,String> updateEntry : updateMap.entrySet())
                {
                    args.put(updateEntry.getKey(),updateEntry.getValue());
                }

                String extensionClass = StringUtils.defaultIfEmpty((String)validator.getArgs().get("class.name"),validator.toString());//NON-NLS
                result.put(extensionClass,updateMap);
            }
        }

        return result;
    }

    private Map<String, Map<String, String>> updatePostFunctionCustomFieldIds(ActionDescriptor action, Map<String, String> oldToNewIdMapping)
    {
        Map<String, Map<String, String>> result = new HashMap<String, Map<String, String>>();

        Collection<FunctionDescriptor> postFunctions = action.getUnconditionalResult().getPostFunctions();
        postFunctions.addAll(action.getPostFunctions());
        
        for(FunctionDescriptor function : postFunctions)
        {
            Map<String,String> args = (Map<String,String>) function.getArgs();

            Map<String,String> updateMap = new HashMap<String, String>();

            for(Map.Entry<String,String> entry : args.entrySet())
            {
                if(entry.getValue().startsWith(WorkflowCustomFieldsHelper.CUSTOM_FIELD_PREFIX) && oldToNewIdMapping.containsKey(entry.getValue()))
                {
                    updateMap.put(entry.getKey(),oldToNewIdMapping.get(entry.getValue()));
                }
            }

            if(!updateMap.isEmpty())
            {
                for(Map.Entry<String,String> updateEntry : updateMap.entrySet())
                {
                    args.put(updateEntry.getKey(),updateEntry.getValue());
                }

                String extensionClass = StringUtils.defaultIfEmpty((String)function.getArgs().get("class.name"),function.toString());//NON-NLS
                result.put(extensionClass,updateMap);
            }
        }

        return result;
    }

    private Set<String> getClassnamesFromArgs(Set<Map.Entry<String,String>> args)
    {
        Set<String> classNames = new HashSet<String>();
        
        for(Map.Entry<String,String> entry : args)
        {
            if(entry.getKey().equals("class.name") && StringUtils.isNotBlank(entry.getValue()))//NON-NLS
            {
                classNames.add(entry.getValue());
            }
            else
            {
                Matcher matcher = WorkflowCustomFieldsHelper.CUSTOM_FIELD_PATTERN.matcher(entry.getValue());
                while (matcher.find())
                {
                    String fieldName = matcher.group();
                    String cfClassname = workflowCustomFieldsHelper.getCustomFieldTypeClassname(fieldName);
                    if(StringUtils.isNotBlank(cfClassname))
                    {
                        classNames.add(cfClassname);
                    }
                }
            }
        }
        
        return classNames;
    }

    public class ExtensionRemovalResultImpl implements ExtensionRemovalResult
    {
        private static final String POST_FUNCTIONS = "postFunctions";
        private static final String CONDITIONS = "conditions";
        private static final String VALIDATORS = "validators";
        
        private Map<String, Map<String,Map<String, Set<String>>>> actions;

        public ExtensionRemovalResultImpl()
        {
            this.actions = new HashMap<String, Map<String, Map<String, Set<String>>>>();
        }
        
        public void addAction(String action,Map<String, Set<String>> functions, Map<String, Set<String>> validators, Map<String, Set<String>> conditions)
        {
            Map<String,Map<String, Set<String>>> extensions = new HashMap<String, Map<String, Set<String>>>();
            extensions.put(POST_FUNCTIONS,functions);
            extensions.put(CONDITIONS,conditions);
            extensions.put(VALIDATORS,validators);
            
            actions.put(action,extensions);
        }
        

        @Override
        public Set<String> getActions()
        {
            return actions.keySet();
        }

        @Override
        public Map<String, Set<String>> getPostFunctions(String action)
        {
            Map<String, Set<String>> extension;
            if(actions.containsKey(action))
            {
                extension = actions.get(action).get(POST_FUNCTIONS);
            }
            else
            {
                extension = Collections.<String, Set<String>> emptyMap();
            }
            
            return extension;
        }

        @Override
        public Map<String, Set<String>> getValidators(String action)
        {
            Map<String, Set<String>> extension;
            if(actions.containsKey(action))
            {
                extension = actions.get(action).get(VALIDATORS);
            }
            else
            {
                extension = Collections.<String, Set<String>> emptyMap();
            }

            return extension;
        }

        @Override
        public Map<String, Set<String>> getConditions(String action)
        {
            Map<String, Set<String>> extension;
            if(actions.containsKey(action))
            {
                extension = actions.get(action).get(CONDITIONS);
            }
            else
            {
                extension = Collections.<String, Set<String>> emptyMap();
            }

            return extension;
        }
    }

    public class ExtensionCustomFieldUpdateResultImpl implements ExtensionCustomFieldUpdateResult 
    {
        private static final String POST_FUNCTIONS = "postFunctions";
        private static final String CONDITIONS = "conditions";
        private static final String VALIDATORS = "validators";
        
        private Map<String, Map<String,Map<String, Map<String, String>>>> actions;

        public ExtensionCustomFieldUpdateResultImpl()
        {
            this.actions = new HashMap<String, Map<String, Map<String, Map<String, String>>>>();
        }

        public void addAction(String action, Map<String, Map<String,String>> functions, Map<String, Map<String,String>> validators, Map<String, Map<String,String>> conditions)
        {
            Map<String,Map<String, Map<String, String>>> extensions = new HashMap<String, Map<String, Map<String, String>>>();
            extensions.put(POST_FUNCTIONS,functions);
            extensions.put(CONDITIONS,conditions);
            extensions.put(VALIDATORS,validators);

            actions.put(action,extensions);
        }

        @Override
        public Set<String> getActions()
        {
            return actions.keySet();
        }

        @Override
        public Map<String,Map<String,String>> getPostFunctions(String action)
        {
            Map<String, Map<String,String>> extension;
            if(actions.containsKey(action))
            {
                extension = actions.get(action).get(POST_FUNCTIONS);
            }
            else
            {
                extension = Collections.<String, Map<String,String>> emptyMap();
            }

            return extension;
        }

        @Override
        public Map<String,Map<String,String>> getValidators(String action)
        {
            Map<String, Map<String,String>> extension;
            if(actions.containsKey(action))
            {
                extension = actions.get(action).get(VALIDATORS);
            }
            else
            {
                extension = Collections.<String, Map<String,String>> emptyMap();
            }

            return extension;
        }

        @Override
        public Map<String,Map<String,String>> getConditions(String action)
        {
            Map<String, Map<String,String>> extension;
            if(actions.containsKey(action))
            {
                extension = actions.get(action).get(CONDITIONS);
            }
            else
            {
                extension = Collections.<String, Map<String,String>> emptyMap();
            }

            return extension;
        }
    }
}