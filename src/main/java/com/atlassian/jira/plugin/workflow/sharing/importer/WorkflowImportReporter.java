package com.atlassian.jira.plugin.workflow.sharing.importer;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.plugin.workflow.sharing.WorkflowExtensionsHelper;
import com.atlassian.sal.api.message.I18nResolver;

/**
 * @since version
 */
public class WorkflowImportReporter
{
    private final I18nResolver i18n;
    private String workingFolder;
    private WorkflowExtensionsHelper.ExtensionRemovalResult removedExtensions;
    private List<PluginInstallInfo> uninstalledPlugins;
    private List<PluginInstallInfo> installedPlugins;
    private List<PluginInstallInfo> alreadyInstalledPlugins;
    private Map<String,StatusMapping> createdStatuses;
    private Map<String,StatusMapping> mappedStatuses;
    private WorkflowExtensionsHelper.ExtensionCustomFieldUpdateResult customFieldMappings;
    private List<CustomField> createdCustomFields;
    private List<FieldScreen> createdScreens;
    private List<ScreenMapping> screenMappings;
    private String workflowName;
    private String notes;
    private User user;

    public WorkflowImportReporter(I18nResolver i18n)
    {
        this.i18n = i18n;
        this.uninstalledPlugins = new ArrayList<PluginInstallInfo>();
        this.installedPlugins = new ArrayList<PluginInstallInfo>();
        this.alreadyInstalledPlugins = new ArrayList<PluginInstallInfo>();
        this.createdStatuses = new HashMap<String, StatusMapping>();
        this.mappedStatuses = new HashMap<String, StatusMapping>();
        this.createdCustomFields = new ArrayList<CustomField>();
        this.createdScreens = new ArrayList<FieldScreen>();
        this.screenMappings = new ArrayList<ScreenMapping>();
    }

    public WorkflowImportReport getReport()
    {
        return new WorkflowImportReport(workingFolder,removedExtensions,uninstalledPlugins,installedPlugins,alreadyInstalledPlugins,createdStatuses,mappedStatuses,customFieldMappings,createdCustomFields,createdScreens,screenMappings,workflowName,notes,user,i18n);
    }

    public void setWorkingFolder(File workingFolder)
    {
        this.workingFolder = workingFolder.getAbsolutePath();
    }

    public void addRemovedExtensions(WorkflowExtensionsHelper.ExtensionRemovalResult result)
    {
        this.removedExtensions = result;
    }

    public void addUninstalledPlugin(PluginInstallInfo installInfo)
    {
        uninstalledPlugins.add(installInfo);
    }

    public void addInstalledPlugin(PluginInstallInfo installInfo)
    {
        installedPlugins.add(installInfo);
    }

    public void addAlreadyInstalledPlugin(PluginInstallInfo installInfo)
    {
        alreadyInstalledPlugins.add(installInfo);
    }

    public void addCreatedStatus(String stepName, StatusMapping mapping)
    {
        createdStatuses.put(stepName,mapping);
    }

    public void addMappedStatus(String stepName, StatusMapping mapping)
    {
        mappedStatuses.put(stepName,mapping);
    }

    public void addCustomFieldMappings(WorkflowExtensionsHelper.ExtensionCustomFieldUpdateResult result)
    {
        this.customFieldMappings = result;
    }

    public void addCreatedCustomField(CustomField cf)
    {
        createdCustomFields.add(cf);
    }

    public void addCreatedScreen(FieldScreen screen)
    {
        createdScreens.add(screen);
    }

    public void addScreenMapping(String name, Long originalId, Long id)
    {
        screenMappings.add(new ScreenMapping(name,originalId,id));
    }

    public void addCreatedWorkflow(String name)
    {
        this.workflowName = name;
    }

    public void setNotes(String notesAsString)
    {
        this.notes = notesAsString;
    }

    public void setUser(User loggedInUser)
    {
        this.user = loggedInUser;
    }

    public class ScreenMapping
    {
        private String screenName;
        private Long oldId;
        private Long newId;

        ScreenMapping(String screenName, Long oldId, Long newId)
        {
            this.screenName = screenName;
            this.oldId = oldId;
            this.newId = newId;
        }

        public String getScreenName()
        {
            return screenName;
        }

        public Long getOldId()
        {
            return oldId;
        }

        public Long getNewId()
        {
            return newId;
        }
    }
}
