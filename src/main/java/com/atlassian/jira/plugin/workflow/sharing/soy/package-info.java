/**
 * This entire package is here to support google closure web-resource-transformations across JIRA 4.4 and JIRA 5.x
 * 4.4 had support built in a jira-soy-plugin but 5.x changed to using the soy-template-plugin.
 * Unfortunately due to package naming, class naming, and differences in custom closure functions, these 2 implementations are NOT interchangeable.
 * Hence, we've added our own local version here so we don't have to use reflection or some other hacky workaround.
 * 
 * If this ever gets bundles with JIRA, we should replace this with the soy-template-plugin
 */
package com.atlassian.jira.plugin.workflow.sharing.soy;

