package com.atlassian.jira.plugin.workflow.sharing;

import java.util.Collection;

import com.atlassian.plugin.ModuleDescriptor;

/**
 * @since version
 */
public interface ModuleDescriptorLocator
{

    Collection<ModuleDescriptor> getEnabledModuleDescriptorsByModuleClassname(String moduleClassname);
}
