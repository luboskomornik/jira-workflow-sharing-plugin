package com.atlassian.jira.plugin.workflow.sharing.importer.servlet;

/**
 * @since version
 */
public enum ImportServletMapping
{
    IMPORT_CHOOSE_ZIP("wfshare-import", "importer/import-choose-zip.vm")//NON-NLS
    ,IMPORT_SET_NAME("set-workflow-name","importer/import-choose-name.vm")//NON-NLS
    ,IMPORT_MAP_STATUSES("map-statuses","importer/import-map-statuses.vm")//NON-NLS
    ,IMPORT_VERIFY_PLUGINS("verify-plugins","importer/import-verify-plugins.vm")//NON-NLS
    ,IMPORT_VIEW_SUMMARY("summary","importer/import-summary.vm")//NON-NLS
    ,IMPORT_WORKFLOW("import-workflow","importer/import-success.vm")//NON-NLS
    ,IMPORT_VIEW_REPORT("view-report","importer/import-view-report.vm")//NON-NLS
    ,REQUIRES_SYSADMIN("","importer/import-requires-sysadmin.vm")//NON-NLS
    ,ERROR("","import-error.vm")//NON-NLS
    ,NOT_MAPPED("","");

    public static final String TEMPLATE_PREFIX = "/templates/workflow-sharing/";//NON-NLS

    private String path;
    private String resultTemplate;

    private ImportServletMapping(String path, String template)
    {
        this.path = path;
        this.resultTemplate = TEMPLATE_PREFIX + template;
    }

    public String getPath()
    {
        return path;
    }

    public String getResultTemplate()
    {
        return resultTemplate;
    }

    public ImportServletMapping next()
    {
        if(this.ordinal() < (ImportServletMapping.values().length - 1))
        {
            return ImportServletMapping.values()[this.ordinal() + 1];
        }
        else
        {
            return null;
        }
    }

    public ImportServletMapping previous()
    {
        if(this.ordinal() > 0)
        {
            return ImportServletMapping.values()[this.ordinal() - 1];
        }
        else
        {
            return null;
        }
    }
    
    public static ImportServletMapping fromPath(String path)
    {
        for(ImportServletMapping mapping : ImportServletMapping.values())
        {
            if(mapping.getPath().equals(path))
            {
                return mapping;
            }
        }

        return NOT_MAPPED;
    }
}
