package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.plugin.workflow.sharing.WorkflowCustomFieldsHelper;
import com.atlassian.jira.plugin.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.ScreenItemInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.ScreenTabInfo;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

/**
 * @since version
 */
public class ScreenCreatorImpl implements ScreenCreator
{
    private final FieldScreenManager fieldScreenManager;

    public ScreenCreatorImpl(FieldScreenManager fieldScreenManager)
    {
        this.fieldScreenManager = fieldScreenManager;
    }

    @Override
    public void removeScreen(Long screenId)
    {
        fieldScreenManager.removeFieldScreen(screenId);
    }

    @Override
    public FieldScreen createScreen(ScreenInfo screenInfo)
    {

        String validName = getValidName(screenInfo.getName(), fieldScreenManager.getFieldScreens());
        FieldScreen screen = new FieldScreenImpl(fieldScreenManager);

        screen.setName(validName);
        screen.setDescription(screenInfo.getDescription());
        screen.store();

        return screen;
    }

    @Override
    public void addScreenTabs(FieldScreen screen, ScreenInfo screenInfo, Map<String, String> createdFieldsMapping)
    {
        for (ScreenTabInfo tabInfo : screenInfo.getTabs())
        {
            FieldScreenTab tab = screen.addTab(tabInfo.getName());
            tab.setPosition(tabInfo.getPosition());

            List<ScreenItemInfo> sortedItems = Ordering.from(new Comparator<ScreenItemInfo>() {
                @Override
                public int compare(ScreenItemInfo o1, ScreenItemInfo o2)
                {
                    return o1.getPosition().compareTo(o2.getPosition());
                }
            }).sortedCopy(tabInfo.getItems());


            for (ScreenItemInfo itemInfo : sortedItems)
            {
                if (!itemInfo.getFieldId().startsWith(WorkflowCustomFieldsHelper.CUSTOM_FIELD_PREFIX) || createdFieldsMapping.containsKey(itemInfo.getFieldId()))
                {
                    String newId = itemInfo.getFieldId();
                    
                    if(itemInfo.getFieldId().startsWith(WorkflowCustomFieldsHelper.CUSTOM_FIELD_PREFIX))
                    {
                        newId = createdFieldsMapping.get(itemInfo.getFieldId());
                    }
                    
                    tab.addFieldScreenLayoutItem(newId, itemInfo.getPosition());
                }
            }
        }
    }

    private String getValidName(String name, Collection<FieldScreen> fieldScreens)
    {
        String suggestedName = name;
        
        if(screenNameAlreadyExists(suggestedName,fieldScreens))
        {
            int x = 2;
            suggestedName = name + " " + x;
            
            while(screenNameAlreadyExists(suggestedName,fieldScreens))
            {
                suggestedName = suggestedName = name + " " + x;
                x++;
            }
        }
        
        return suggestedName;
    }

    private boolean screenNameAlreadyExists(final String name, Collection<FieldScreen> fieldScreens)
    {

        return !ImmutableList.copyOf(Iterables.filter(fieldScreenManager.getFieldScreens(), new Predicate<FieldScreen>()
        {
            @Override
            public boolean apply(FieldScreen fieldScreen)
            {
                return name.equals(fieldScreen.getName());
            }
        })).isEmpty();
    }
}
