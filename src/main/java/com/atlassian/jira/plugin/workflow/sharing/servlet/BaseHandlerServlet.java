package com.atlassian.jira.plugin.workflow.sharing.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since version
 */
public class BaseHandlerServlet extends HttpServlet
{
    private final ServletHandler handler;

    public BaseHandlerServlet(ServletHandler handler)
    {
        this.handler = checkNotNull(handler,"handler"); //NON-NLS
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        handler.handle(request,response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        handler.handle(request,response);
    }
}

