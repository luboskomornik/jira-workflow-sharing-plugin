package com.atlassian.jira.plugin.workflow.sharing.importer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.plugin.workflow.sharing.WorkflowExtensionsHelper;
import com.atlassian.jira.plugin.workflow.sharing.model.WorkflowExtensionsPluginInfo;
import com.atlassian.sal.api.message.I18nResolver;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.pegdown.Extensions;
import org.pegdown.PegDownProcessor;

/**
 * @since version
 */
public class WorkflowImportReport
{
    private static final String EOL = System.getProperty("line.separator");
    private static final String P = EOL.concat(EOL);
    private static final String HR = P.concat(StringUtils.repeat("-",80)).concat(P);
    private static final String CLEAN_FILENAME_PATTERN = "[:\\\\/*?|<> _]";

    private final I18nResolver i18n;
    private final String workingFolder;
    private final WorkflowExtensionsHelper.ExtensionRemovalResult removedExtensions;
    private final List<PluginInstallInfo> uninstalledPlugins;
    private final List<PluginInstallInfo> installedPlugins;
    private final List<PluginInstallInfo> alreadyInstalledPlugins;
    private final Map<String,StatusMapping> createdStatuses;
    private final Map<String,StatusMapping> mappedStatuses;
    private final WorkflowExtensionsHelper.ExtensionCustomFieldUpdateResult customFieldMappings;
    private final List<CustomField> createdCustomFields;
    private final List<FieldScreen> createdScreens;
    private final List<WorkflowImportReporter.ScreenMapping> screenMappings;
    private final String workflowName;
    private final String notes;
    private final PegDownProcessor pegDown;
    private final StringBuilder buffer;
    private User user;
    private String reportName;
    
    private String markdown;

    public WorkflowImportReport(String workingFolder, WorkflowExtensionsHelper.ExtensionRemovalResult removedExtensions, List<PluginInstallInfo> uninstalledPlugins, List<PluginInstallInfo> installedPlugins, List<PluginInstallInfo> alreadyInstalledPlugins, Map<String, StatusMapping> createdStatuses, Map<String, StatusMapping> mappedStatuses, WorkflowExtensionsHelper.ExtensionCustomFieldUpdateResult customFieldMappings, List<CustomField> createdCustomFields, List<FieldScreen> createdScreens, List<WorkflowImportReporter.ScreenMapping> screenMappings, String workflowName, String notes, User user, I18nResolver i18n)
    {
        this.workingFolder = workingFolder;
        this.removedExtensions = removedExtensions;
        this.uninstalledPlugins = uninstalledPlugins;
        this.installedPlugins = installedPlugins;
        this.alreadyInstalledPlugins = alreadyInstalledPlugins;
        this.createdStatuses = createdStatuses;
        this.mappedStatuses = mappedStatuses;
        this.customFieldMappings = customFieldMappings;
        this.createdCustomFields = createdCustomFields;
        this.createdScreens = createdScreens;
        this.screenMappings = screenMappings;
        this.workflowName = workflowName;
        this.notes = notes;
        this.pegDown = new PegDownProcessor(Extensions.ALL);
        this.buffer = new StringBuilder();
        this.user = user;
        this.i18n = i18n;
        
        this.markdown = generateMarkdown();
    }

    public String asMarkdown()
    {
        return Jsoup.clean(markdown, Whitelist.relaxed());
    }

    public String asHtml()
    {
        return Jsoup.clean(pegDown.markdownToHtml(markdown),Whitelist.relaxed());
    }

    public String getReportName()
    {
        return reportName;
    }

    private String generateMarkdown()
    {
        Date now = new Date();
        SimpleDateFormat displayFormat = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss zzz");
        SimpleDateFormat fileFormat = new SimpleDateFormat("MM-dd-yyyy-HH-mm-ss");
        this.reportName = workflowName.replaceAll(CLEAN_FILENAME_PATTERN, "-") + "-" + fileFormat.format(now);
        
        buffer.append(i18n.getText("wfshare.report.md.workflow.import.report"))
              .append(EOL)
              .append("## ").append(workflowName)
              .append(EOL)
              .append(i18n.getText("wfshare.report.md.imported.on")).append(displayFormat.format(now))
              .append(EOL);
         
        if(null != user)
        {
            buffer.append(i18n.getText("wfshare.report.md.imported.by")).append(user.getDisplayName()).append(" (").append(user.getName()).append(")")
                .append(EOL);
        }
              buffer.append(i18n.getText("wfshare.report.md.extracted.to", workingFolder))
              .append(EOL)
              .append(HR);
        
        generateNotes();
        generatePlugins();
        generateStatuses();
        generateCreatedCustomFields();
        generateCreatedScreens();
        
        return buffer.toString();
    }

    private void generateNotes()
    {
        if(StringUtils.isNotBlank(notes))
        {
            buffer.append(i18n.getText("wfshare.report.md.author.notes"))
                  .append(EOL)
                  .append(i18n.getText("wfshare.report.md.below.are.notes.that.were.entered"))
                  .append(i18n.getText("wfshare.report.md.these.usually.contain.helpful.tips"))
                  .append(P)
                  .append(notes)
                  .append(HR);
        }
    }

    private void generatePlugins()
    {
        if(!installedPlugins.isEmpty() || !uninstalledPlugins.isEmpty() || !alreadyInstalledPlugins.isEmpty())
        {
            buffer.append(i18n.getText("wfshare.report.md.plugins")).append(P);
            
            generateInstalledPlugins();
            generateNotInstalledPlugins();
            generateAlreadyInstalledPlugins();
            
            buffer.append(HR);
        }
    }

    private void generateInstalledPlugins()
    {
        if(!installedPlugins.isEmpty())
        {
            buffer.append(i18n.getText("wfshare.report.md.installed.plugins"))
                  .append(EOL)
                  .append(i18n.getText("wfshare.report.md.below.is.the.list.of.plugins.that.were.newly.installed"))
                  .append(P);
            
            generatePluginTable(installedPlugins);

        }
    }

    private void generateNotInstalledPlugins()
    {
        if(!uninstalledPlugins.isEmpty())
        {
            buffer.append(i18n.getText("wfshare.report.md.not.installed.plugins"))
                  .append(EOL)
                  .append(i18n.getText("wfshare.report.md.below.is.the.list.of.plugins.that.were.not.installed"))
                  .append(P);

            generatePluginTable(uninstalledPlugins);

        }
    }

    private void generateAlreadyInstalledPlugins()
    {
        if(!alreadyInstalledPlugins.isEmpty())
        {
            buffer.append(i18n.getText("wfshare.report.md.existing.plugins"))
                  .append(EOL)
                  .append(i18n.getText("wfshare.report.md.below.is.the.list.of.plugins.that.were.already.installed"))
                  .append(P);

            generateAlreadyInstalledPluginTable(alreadyInstalledPlugins);

        }
    }

    private void generatePluginTable(List<PluginInstallInfo> plugins)
    {
        String nameHeader = i18n.getText("wfshare.report.md.plugin.name");
        String versionHeader = i18n.getText("wfshare.report.md.installed.version");
        String descHeader = i18n.getText("wfshare.report.md.description");

        PluginInfoMaxLengths maxLengths = getPluginInfoMaxLengths(plugins,nameHeader,versionHeader,descHeader);
        int maxName = maxLengths.getNameLength();
        int maxVer = maxLengths.getVersionLength();
        int maxDesc = maxLengths.getDescriptionLength();

        buffer.append(StringUtils.rightPad(nameHeader,maxName))
              .append(" | ")
              .append(StringUtils.rightPad(versionHeader,maxVer))
              .append(" | ")
              .append(StringUtils.rightPad(descHeader,maxDesc))
              .append(EOL)
              .append(StringUtils.repeat("-",maxName))
              .append(" | ")
              .append(StringUtils.repeat("-",maxVer))
              .append(" | ")
              .append(StringUtils.repeat("-",maxDesc))
              .append(EOL);

        for(PluginInstallInfo info : plugins)
        {
            WorkflowExtensionsPluginInfo pluginInfo = info.getPluginInfo();
            buffer.append(StringUtils.rightPad(pluginInfo.getName(),maxName))
                  .append(" | ")
                  .append(StringUtils.rightPad(info.getInstallVersion(),maxVer))
                  .append(" | ")
                  .append(StringUtils.rightPad(StringUtils.abbreviate(pluginInfo.getDescription(),maxDesc),maxDesc))
                  .append(EOL);
        }
    }

    private void generateAlreadyInstalledPluginTable(List<PluginInstallInfo> plugins)
    {
        String nameHeader = i18n.getText("wfshare.report.md.plugin.name");
        String bundleVersionHeader = i18n.getText("wfshare.report.md.bundle.version");
        String installedVersionHeader = i18n.getText("wfshare.report.md.installed.version");
        String descHeader = i18n.getText("wfshare.report.md.description");

        AlreadyInstalledPluginInfoMaxLengths maxLengths = getAlreadyInstalledPluginInfoMaxLengths(plugins,nameHeader,bundleVersionHeader,installedVersionHeader,descHeader);
        int maxName = maxLengths.getNameLength();
        int maxBundleVer = maxLengths.getBundleVersionLength();
        int maxInstallVer = maxLengths.getInstalledVersionLength();
        int maxDesc = maxLengths.getDescriptionLength();

        buffer.append(StringUtils.rightPad(nameHeader,maxName))
              .append(" | ")
              .append(StringUtils.rightPad(bundleVersionHeader,maxBundleVer))
              .append(" | ")
              .append(StringUtils.rightPad(installedVersionHeader,maxInstallVer))
              .append(" | ")
              .append(StringUtils.rightPad(descHeader,maxDesc))
              .append(EOL)
              .append(StringUtils.repeat("-",maxName))
              .append(" | ")
              .append(StringUtils.repeat("-",maxBundleVer))
              .append(" | ")
              .append(StringUtils.repeat("-",maxInstallVer))
              .append(" | ")
              .append(StringUtils.repeat("-",maxDesc))
              .append(EOL);

        for(PluginInstallInfo info : plugins)
        {
            WorkflowExtensionsPluginInfo pluginInfo = info.getPluginInfo();
            buffer.append(StringUtils.rightPad(pluginInfo.getName(),maxName))
                  .append(" | ")
                  .append(StringUtils.rightPad(info.getInstallVersion(),maxBundleVer))
                  .append(" | ")
                  .append(StringUtils.rightPad(info.getAlreadyInstalledVersion(),maxInstallVer))
                  .append(" | ")
                  .append(StringUtils.rightPad(StringUtils.abbreviate(pluginInfo.getDescription(),maxDesc),maxDesc))
                  .append(EOL);
        }
    }

    private void generateStatuses()
    {
        if(!createdStatuses.isEmpty() || !mappedStatuses.isEmpty())
        {
            buffer.append(i18n.getText("wfshare.report.md.statuses")).append(P);

            generateCreatedStatuses();
            generateMappedStatuses();
        }

        buffer.append(HR);
    }

    private void generateCreatedStatuses()
    {
        if(!createdStatuses.isEmpty())
        {
            buffer.append(i18n.getText("wfshare.report.md.created.statuses"))
                  .append(EOL)
                  .append(i18n.getText("wfshare.report.md.below.is.the.list.of.statuses.that.were.newly.created"))
                  .append(P);

            String nameHeader = i18n.getText("wfshare.report.md.status.name");
            String idHeader = i18n.getText("wfshare.report.md.new.id");

            StatusMaxLengths maxLengths = getMaxStatusLengths(createdStatuses, nameHeader, "", idHeader);
            int maxName = maxLengths.getNameLength();
            int maxNewId = maxLengths.getNewIdLength();

            buffer.append(StringUtils.rightPad(nameHeader, maxName))
                  .append(" | ")
                  .append(StringUtils.rightPad(idHeader, maxNewId))
                  .append(EOL)
                  .append(StringUtils.repeat("-",maxName))
                  .append(" | ")
                  .append(StringUtils.repeat("-",maxNewId))
                  .append(EOL);
            
            for(Map.Entry<String,StatusMapping> entry : createdStatuses.entrySet())
            {
                buffer.append(StringUtils.rightPad(entry.getValue().getNewName(), maxName))
                      .append(" | ")
                      .append(StringUtils.rightPad(entry.getValue().getNewId(),maxNewId))
                      .append(EOL);
            }
        }
    }

    private void generateMappedStatuses()
    {
        if(!mappedStatuses.isEmpty())
        {
            buffer.append(i18n.getText("wfshare.report.md.mapped.statuses"))
                  .append(EOL)
                  .append(i18n.getText("wfshare.report.md.below.is.the.list.of.statuses.that.were.mapped"))
                  .append(P);

            String nameHeader = i18n.getText("wfshare.report.md.status.name");
            String oldIdHeader = i18n.getText("wfshare.report.md.old.id");
            String newIdHeader = i18n.getText("wfshare.report.md.new.id");

            StatusMaxLengths maxLengths = getMaxStatusLengths(mappedStatuses,nameHeader,oldIdHeader,newIdHeader);
            int maxName = maxLengths.getNameLength();
            int maxOldId = maxLengths.getOldIdLength();
            int maxNewId = maxLengths.getNewIdLength();

            buffer.append(StringUtils.rightPad(nameHeader,maxName))
                  .append(" | ")
                  .append(StringUtils.rightPad(oldIdHeader, maxOldId))
                  .append(" | ")
                  .append(StringUtils.rightPad(newIdHeader, maxNewId))
                  .append(EOL)
                  .append(StringUtils.repeat("-", maxName))
                  .append(" | ")
                  .append(StringUtils.repeat("-",maxOldId))
                  .append(" | ")
                  .append(StringUtils.repeat("-",maxNewId))
                  .append(EOL);

            for(Map.Entry<String,StatusMapping> entry : mappedStatuses.entrySet())
            {
                buffer.append(StringUtils.rightPad(entry.getValue().getNewName(),maxName))
                      .append(" | ")
                      .append(StringUtils.rightPad(entry.getValue().getOriginalId(), maxOldId))
                      .append(" | ")
                      .append(StringUtils.rightPad(entry.getValue().getNewId(),maxNewId))
                      .append(EOL);
            }
        }
    }

    private void generateCreatedCustomFields()
    {
        if(!createdCustomFields.isEmpty())
        {
            buffer.append(i18n.getText("wfshare.report.md.custom.fields")).append(P)
              .append(i18n.getText("wfshare.report.md.created.custom.fields"))
              .append(EOL)
              .append(i18n.getText("wfshare.report.md.below.is.the.list.of.custom.fields.that.were.newly.created"))
              .append(P);

            String nameHeader = i18n.getText("wfshare.report.md.custom.field.name");
            String newIdHeader = i18n.getText("wfshare.report.md.new.id");
            
            NameAndIdMaxLengths maxLengths = getMaxCustomFieldLengths(createdCustomFields,nameHeader,newIdHeader);
            int maxName = maxLengths.getNameLength();
            int maxNewId = maxLengths.getNewIdLength();

            buffer.append(StringUtils.rightPad(nameHeader, maxName))
                  .append(" | ")
                  .append(StringUtils.rightPad(newIdHeader, maxNewId))
                  .append(EOL)
                  .append(StringUtils.repeat("-",maxName))
                  .append(" | ")
                  .append(StringUtils.repeat("-",maxNewId))
                  .append(EOL);

            for(CustomField field : createdCustomFields)
            {
                buffer.append(StringUtils.rightPad(field.getName(), maxName))
                      .append(" | ")
                      .append(StringUtils.rightPad(field.getId(),maxNewId))
                      .append(EOL);
            }
            
            buffer.append(HR);
        }

    }

    private void generateCreatedScreens()
    {
        if(!createdScreens.isEmpty())
        {
            buffer.append(i18n.getText("wfshare.report.md.screens")).append(P)
                  .append(i18n.getText("wfshare.report.md.created.screens"))
                  .append(EOL)
                  .append(i18n.getText("wfshare.report.md.below.is.the.list.of.screens.that.were.newly.created"))
                  .append(P);

            String nameHeader = i18n.getText("wfshare.report.md.screen.name");
            String newIdHeader = i18n.getText("wfshare.report.md.new.id");

            NameAndIdMaxLengths maxLengths = getMaxScreenLengths(createdScreens, nameHeader, newIdHeader);
            int maxName = maxLengths.getNameLength();
            int maxNewId = maxLengths.getNewIdLength();

            buffer.append(StringUtils.rightPad(nameHeader, maxName))
                  .append(" | ")
                  .append(StringUtils.rightPad(newIdHeader, maxNewId))
                  .append(EOL)
                  .append(StringUtils.repeat("-",maxName))
                  .append(" | ")
                  .append(StringUtils.repeat("-",maxNewId))
                  .append(EOL);

            for(FieldScreen screen : createdScreens)
            {
                buffer.append(StringUtils.rightPad(screen.getName(), maxName))
                      .append(" | ")
                      .append(StringUtils.rightPad(Long.toString(screen.getId()),maxNewId))
                      .append(EOL);
            }

            buffer.append(HR);
        }

    }

    public String getNotesAsHtml()
    {
        if(StringUtils.isNotBlank(notes))
        {
            return pegDown.markdownToHtml(notes);
        }
        
        return notes;
    }

    private StatusMaxLengths getMaxStatusLengths(Map<String, StatusMapping> createdStatuses, String nameHeader, String oldIdHeader, String newIdHeader)
    {
        int maxName = 0;
        int maxOldId = 0;
        int maxNewId = 0;
        
        for(Map.Entry<String,StatusMapping> entry : createdStatuses.entrySet())
        {
            maxName = (entry.getValue().getNewName().length() > maxName) ? entry.getValue().getNewName().length() : maxName;
            maxOldId = (entry.getValue().getOriginalId().length() > maxOldId) ? entry.getValue().getOriginalId().length() : maxOldId;
            maxNewId = (entry.getValue().getNewId().length() > maxNewId) ? entry.getValue().getNewId().length() : maxNewId;
        }
        
        maxName = (maxName > nameHeader.length()) ? maxName : nameHeader.length();
        maxOldId = (maxOldId > oldIdHeader.length()) ? maxOldId : oldIdHeader.length();
        maxNewId = (maxNewId > newIdHeader.length()) ? maxNewId : newIdHeader.length();
        
        return new StatusMaxLengths(maxName,maxOldId,maxNewId);
    }
    
    private PluginInfoMaxLengths getPluginInfoMaxLengths(List<PluginInstallInfo> infoList,String nameHeader,String versionHeader,String descHeader)
    {
        int maxName = 0;
        int maxVersion = 0;
        int maxDesc = 0;
        
        for(PluginInstallInfo installInfo : infoList)
        {
            maxName = (installInfo.getPluginInfo().getName().length() > maxName) ? installInfo.getPluginInfo().getName().length() : maxName;
            maxVersion = (installInfo.getInstallVersion().length() > maxVersion) ? installInfo.getInstallVersion().length() : maxVersion;
            maxDesc = (installInfo.getPluginInfo().getDescription().length() > maxDesc) ? installInfo.getPluginInfo().getDescription().length() : maxDesc;
        }

        maxName = (maxName > nameHeader.length()) ? maxName : nameHeader.length();
        maxVersion = (maxVersion > versionHeader.length()) ? maxVersion : versionHeader.length();
        maxDesc = (maxDesc > descHeader.length()) ? maxDesc : descHeader.length();
        
        if(maxDesc > 100)
        {
            maxDesc = 100;
        }
        
        return new PluginInfoMaxLengths(maxName,maxVersion,maxDesc);
    }

    private AlreadyInstalledPluginInfoMaxLengths getAlreadyInstalledPluginInfoMaxLengths(List<PluginInstallInfo> infoList,String nameHeader,String bundleVersionHeader, String installedVersionHeader,String descHeader)
    {
        int maxName = 0;
        int maxBundleVersion = 0;
        int maxInstallVersion = 0;
        int maxDesc = 0;

        for(PluginInstallInfo installInfo : infoList)
        {
            maxName = (installInfo.getPluginInfo().getName().length() > maxName) ? installInfo.getPluginInfo().getName().length() : maxName;
            maxBundleVersion = (installInfo.getInstallVersion().length() > maxBundleVersion) ? installInfo.getInstallVersion().length() : maxBundleVersion;
            maxInstallVersion = (installInfo.getAlreadyInstalledVersion().length() > maxInstallVersion) ? installInfo.getAlreadyInstalledVersion().length() : maxInstallVersion;
            maxDesc = (installInfo.getPluginInfo().getDescription().length() > maxDesc) ? installInfo.getPluginInfo().getDescription().length() : maxDesc;
        }

        maxName = (maxName > nameHeader.length()) ? maxName : nameHeader.length();
        maxBundleVersion = (maxBundleVersion > bundleVersionHeader.length()) ? maxBundleVersion : bundleVersionHeader.length();
        maxInstallVersion = (maxInstallVersion > installedVersionHeader.length()) ? maxInstallVersion : installedVersionHeader.length();
        maxDesc = (maxDesc > descHeader.length()) ? maxDesc : descHeader.length();

        if(maxDesc > 100)
        {
            maxDesc = 100;
        }

        return new AlreadyInstalledPluginInfoMaxLengths(maxName,maxBundleVersion,maxInstallVersion,maxDesc);
    }

    private NameAndIdMaxLengths getMaxCustomFieldLengths(List<CustomField> customFields, String nameHeader, String newIdHeader)
    {
        int maxName = 0;
        int maxNewId = 0;

        for(CustomField field : customFields)
        {
            maxName = (field.getName().length() > maxName) ? field.getName().length() : maxName;
            maxNewId = (field.getId().length() > maxNewId) ? field.getId().length() : maxNewId;
        }

        maxName = (maxName > nameHeader.length()) ? maxName : nameHeader.length();
        maxNewId = (maxNewId > newIdHeader.length()) ? maxNewId : newIdHeader.length();

        return new NameAndIdMaxLengths(maxName,maxNewId);
    }

    private NameAndIdMaxLengths getMaxScreenLengths(List<FieldScreen> screens, String nameHeader, String newIdHeader)
    {
        int maxName = 0;
        int maxNewId = 0;

        for(FieldScreen screen : screens)
        {
            maxName = (screen.getName().length() > maxName) ? screen.getName().length() : maxName;
            maxNewId = (Long.toString(screen.getId()).length() > maxNewId) ? Long.toString(screen.getId()).length() : maxNewId;
        }

        maxName = (maxName > nameHeader.length()) ? maxName : nameHeader.length();
        maxNewId = (maxNewId > newIdHeader.length()) ? maxNewId : newIdHeader.length();

        return new NameAndIdMaxLengths(maxName,maxNewId);
    }
    
    private class PluginInfoMaxLengths 
    {
        private int nameLength;
        private int versionLength;
        private int descriptionLength;

        private PluginInfoMaxLengths(int nameLength, int versionLength, int descriptionLength)
        {
            this.nameLength = nameLength;
            this.versionLength = versionLength;
            this.descriptionLength = descriptionLength;
        }

        public int getNameLength()
        {
            return nameLength;
        }

        public int getVersionLength()
        {
            return versionLength;
        }

        public int getDescriptionLength()
        {
            return descriptionLength;
        }
    }

    private class AlreadyInstalledPluginInfoMaxLengths
    {
        private int nameLength;
        private int bundleVersionLength;
        private int installedVersionLength;
        private int descriptionLength;

        private AlreadyInstalledPluginInfoMaxLengths(int nameLength, int bundleVersionLength, int installedVersionLength, int descriptionLength)
        {
            this.nameLength = nameLength;
            this.bundleVersionLength = bundleVersionLength;
            this.installedVersionLength = installedVersionLength;
            this.descriptionLength = descriptionLength;
        }

        public int getNameLength()
        {
            return nameLength;
        }

        public int getBundleVersionLength()
        {
            return bundleVersionLength;
        }

        public int getInstalledVersionLength()
        {
            return installedVersionLength;
        }

        public int getDescriptionLength()
        {
            return descriptionLength;
        }
    }

    private class StatusMaxLengths 
    {
        private int nameLength;
        private int oldIdLength;
        private int newIdLength;

        private StatusMaxLengths(int nameLength, int oldIdLength, int newIdLength)
        {
            this.nameLength = nameLength;
            this.oldIdLength = oldIdLength;
            this.newIdLength = newIdLength;
        }

        public int getNameLength()
        {
            return nameLength;
        }

        public int getOldIdLength()
        {
            return oldIdLength;
        }

        public int getNewIdLength()
        {
            return newIdLength;
        }
    }

    private class NameAndIdMaxLengths 
    {
        private int nameLength;
        private int newIdLength;

        private NameAndIdMaxLengths(int nameLength, int newIdLength)
        {
            this.nameLength = nameLength;
            this.newIdLength = newIdLength;
        }

        public int getNameLength()
        {
            return nameLength;
        }

        public int getNewIdLength()
        {
            return newIdLength;
        }
    }
}
