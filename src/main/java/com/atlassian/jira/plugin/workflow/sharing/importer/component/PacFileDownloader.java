package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import java.io.File;
import java.net.URI;

import com.atlassian.plugins.PacException;

/**
 * @since version
 */
public interface PacFileDownloader
{
    void downloadFile(String downloadUri, File outputFile) throws Exception;
}
