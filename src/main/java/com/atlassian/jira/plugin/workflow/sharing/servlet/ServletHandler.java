package com.atlassian.jira.plugin.workflow.sharing.servlet;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @since version
 */
public interface ServletHandler
{
    public static final Set<String> CONTENT_TYPE_ZIP = new HashSet<String>()
    {{
            add("application/zip");//NON-NLS
            add("application/x-zip-compressed");//NON-NLS
            add("multipart/x-zip");//NON-NLS
    }};

    void handle(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
}
