package com.atlassian.jira.plugin.workflow.sharing.pac;

/**
 * @since version
 */
public interface JWSPacClientFactory
{
    JWSPacClient getPacClient();
}
