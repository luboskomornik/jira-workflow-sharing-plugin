package com.atlassian.jira.plugin.workflow.sharing.pac;

import com.atlassian.plugins.client.service.ClientContextFactory;
import com.atlassian.plugins.service.ClientContext;
import com.atlassian.sal.api.ApplicationProperties;

/**
 * @since version
 */
public class WSPacClientContextFactory implements ClientContextFactory
{
    private final ApplicationProperties applicationProperties;
    private static final String UPM_CLIENT_TYPE = "upm";

    public WSPacClientContextFactory(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public ClientContext getClientContext()
    {
        return new ClientContext.Builder()
            .clientType(UPM_CLIENT_TYPE)
            .productName(applicationProperties.getDisplayName())
            .productVersion(applicationProperties.getVersion())
            .build();
    }
}
