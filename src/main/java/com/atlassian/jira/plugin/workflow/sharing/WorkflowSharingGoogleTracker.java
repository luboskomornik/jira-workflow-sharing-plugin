package com.atlassian.jira.plugin.workflow.sharing;

import com.atlassian.sal.api.message.I18nResolver;

import com.dmurph.tracking.AnalyticsConfigData;
import com.dmurph.tracking.JGoogleAnalyticsTracker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @since version
 */
public class WorkflowSharingGoogleTracker
{
    private static final Logger log = LoggerFactory.getLogger(WorkflowSharingGoogleTracker.class);
    private static final String TRACKING_CODE = "UA-6032469-43";
    private static final String JWSP = "WorkflowSharing";
    private static final String EVENT_PREFIX = JWSP + ":";

    public static final String EXPORT = "Export";
    public static final String IMPORT = "Import";
    
    private final AnalyticsConfigData config;
    private final JGoogleAnalyticsTracker tracker;
    private final I18nResolver i18n;

    public WorkflowSharingGoogleTracker(I18nResolver i18n)
    {
        this.i18n = i18n;
        this.config = new AnalyticsConfigData(TRACKING_CODE);
        this.tracker = new JGoogleAnalyticsTracker(config, JGoogleAnalyticsTracker.GoogleAnalyticsVersion.V_4_7_2);
        tracker.setDispatchMode(JGoogleAnalyticsTracker.DispatchMode.MULTI_THREAD);
    }

    public void trackImport(String workflowName)
    {
        track(EVENT_PREFIX + IMPORT,workflowName);
    }

    public void trackExport(String workflowName)
    {
        track(EVENT_PREFIX + EXPORT,workflowName);
    }

    private void track(String category, String workflowName)
    {
        if (tracker.isEnabled())
        {
            log.info(i18n.getText("wfshare.log.sending.event.to.google.analytics", category, workflowName));
            tracker.trackEvent(category,workflowName);
        }
    }

    public void setEnabled(boolean enabled) {
        tracker.setEnabled(enabled);
    }
}
