package com.atlassian.jira.plugin.workflow.sharing.importer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.notification.NotificationRecipient;
import com.atlassian.jira.plugin.workflow.sharing.ImportableJiraWorkflow;
import com.atlassian.jira.plugin.workflow.sharing.WorkflowExtensionsHelper;
import com.atlassian.jira.plugin.workflow.sharing.WorkflowScreensHelper;
import com.atlassian.jira.plugin.workflow.sharing.WorkflowSharingGoogleTracker;
import com.atlassian.jira.plugin.workflow.sharing.exception.WorkflowSharingImportException;
import com.atlassian.jira.plugin.workflow.sharing.importer.component.*;
import com.atlassian.jira.plugin.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugin.workflow.sharing.util.ZipUtils;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.atlassian.mail.MailException;
import com.atlassian.mail.MailFactory;
import com.atlassian.mail.server.SMTPMailServer;
import com.atlassian.plugin.*;
import com.atlassian.sal.api.message.I18nResolver;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import com.sysbliss.jira.plugins.workflow.WorkflowDesignerConstants;
import com.sysbliss.jira.plugins.workflow.util.WorkflowDesignerPropertySet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.tuckey.web.filters.urlrewrite.utils.StringUtils;

/**
 * @since version
 */
public class JiraWorkflowSharingImporterImpl implements JiraWorkflowSharingImporter
{
    private static final String JIRA_STATUS_ID = "jira.status.id";
    private static final String NEW_STATUS_DEFAULT_ICON = "/images/icons/status_generic.gif";
    private static final String CLEAN_FILENAME_PATTERN = "[:\\\\/*?|<> _]";
    private static final String BASE_CACHE_FOLDER = "workflowimports";
    private final JiraHome jiraHome;
    private final WorkflowStatusHelper workflowStatusHelper;
    private final PluginController pluginController;
    private final WorkflowManager workflowManager;
    private final WorkflowExtensionsHelper workflowExtensionsHelper;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final PluginAccessor pluginAccessor;
    private final WorkflowDesignerPropertySet workflowDesignerPropertySet;
    private final CustomFieldCreator customFieldCreator;
    private final ScreenCreator screenCreator;
    private final WorkflowScreensHelper workflowScreensHelper;
    private final WorkflowImportReporter reporter;
    private final ReportEmailer reportEmailer;
    private final UserUtil userUtil;
    private final WorkflowSharingGoogleTracker googleTracker;
    private final PacFileDownloader pacFileDownloader;
    private final I18nResolver i18n;
    
    public JiraWorkflowSharingImporterImpl(JiraHome jiraHome, WorkflowStatusHelper workflowStatusHelper, PluginController pluginController, WorkflowManager workflowManager, WorkflowExtensionsHelper workflowExtensionsHelper, JiraAuthenticationContext jiraAuthenticationContext, PluginAccessor pluginAccessor, WorkflowDesignerPropertySet workflowDesignerPropertySet, CustomFieldCreator customFieldCreator, ScreenCreator screenCreator, WorkflowScreensHelper workflowScreensHelper, ReportEmailer reportEmailer, UserUtil userUtil, WorkflowSharingGoogleTracker googleTracker, PacFileDownloader pacFileDownloader, I18nResolver i18n)
    {
        
        this.jiraHome = jiraHome;
        this.workflowStatusHelper = workflowStatusHelper;
        this.pluginController = pluginController;
        this.workflowManager = workflowManager;
        this.workflowExtensionsHelper = workflowExtensionsHelper;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.pluginAccessor = pluginAccessor;
        this.workflowDesignerPropertySet = workflowDesignerPropertySet;
        this.customFieldCreator = customFieldCreator;
        this.screenCreator = screenCreator;
        this.workflowScreensHelper = workflowScreensHelper;
        this.reportEmailer = reportEmailer;
        this.userUtil = userUtil; 
        this.googleTracker = googleTracker; 
        this.pacFileDownloader = pacFileDownloader; 
        this.i18n = i18n;
        this.reporter = new WorkflowImportReporter(i18n);
    }

    @Override
    public File unzipToTempFolder(File workflowZip) throws IOException
    {
        File unzipTo = getTempFolder(FilenameUtils.removeExtension(workflowZip.getName()));
        ZipUtils.unzip(workflowZip, unzipTo);

        return unzipTo;
    }

    public File getBaseCacheFolder() throws IOException
    {
        File baseFolder = new File(jiraHome.getImportDirectory(), BASE_CACHE_FOLDER);
        FileUtils.forceMkdir(baseFolder);

        return baseFolder;
    }

    @Override
    public WorkflowImportResult importWorkflow(SharedWorkflowImportPlan plan, boolean allowGoogleTracking) throws WorkflowSharingImportException
    {
        ImportableJiraWorkflow jiraWorkflow;
        Set<Status> createdStatuses = Collections.<Status>emptySet();
        Set<String> installedPluginKeys = Collections.<String>emptySet();
        Set<CustomField> createdCustomFields = Collections.<CustomField>emptySet();
        Set<FieldScreen> createdScreens = Collections.<FieldScreen>emptySet();
        
        reporter.setUser(jiraAuthenticationContext.getLoggedInUser());
        
        reporter.setWorkingFolder(plan.getWorkingFolder());
        reporter.setNotes(plan.getNotesAsMarkdown());
        
        try
        {
            jiraWorkflow = getJiraWorkflowFromPlan(plan);

            createdStatuses = importStatuses(plan,jiraWorkflow);

            installedPluginKeys = importPlugins(plan,jiraWorkflow);

            CustomFieldCreationResult customFieldCreationResult = importCustomFields(plan,jiraWorkflow);
            createdCustomFields = customFieldCreationResult.getCreatedFields();

            updateWorkflowCustomFieldIds(jiraWorkflow, customFieldCreationResult.getOldToNewIdMapping());

            ScreenCreationResult screenCreationResult = importScreens(plan,jiraWorkflow,customFieldCreationResult.getOldToNewIdMapping());
            createdScreens = screenCreationResult.getCreatedScreens();
            
            updateWorkflowScreenIds(jiraWorkflow,screenCreationResult.getOldToNewIdMapping());

            createWorkflow(jiraWorkflow);

            //save the layout
            importLayoutAndAnnotations(plan);
        }
        catch (Throwable t)
        {
            rollback(createdStatuses,installedPluginKeys,createdCustomFields,createdScreens);
            
            throw new WorkflowSharingImportException(t.getMessage(),t);
        }
        
        WorkflowImportReport report = reporter.getReport();

        SMTPMailServer defaultMailServer;
        try
        {
            defaultMailServer= MailFactory.getServerManager().getDefaultSMTPMailServer();
        }
        catch (MailException e)
        {
            defaultMailServer = null;
        }

        if(!MailFactory.isSendingDisabled() &&  null != defaultMailServer)
        {
            queueEmail(report);
        }
        
        if(allowGoogleTracking)
        {
            googleTracker.trackImport(jiraWorkflow.getName());
        }
        
        return new WorkflowImportResult(jiraWorkflow,report);
    }

    private void queueEmail(WorkflowImportReport report)
    {
        try
        {
            //Collection<User> admins = new ArrayList<User>(userUtil.getJiraAdministrators());
            Collection<User> sysAdmins = new ArrayList<User>(userUtil.getJiraSystemAdministrators());
            //Iterables.removeAll(sysAdmins,admins);
            //admins.addAll(sysAdmins);
            
            if(!sysAdmins.contains(jiraAuthenticationContext.getLoggedInUser()))
            {    
                sysAdmins.add(jiraAuthenticationContext.getLoggedInUser());
            }
            
            Collection<NotificationRecipient> recipients = Collections2.transform(sysAdmins,new Function<User, NotificationRecipient>() {
                @Override
                public NotificationRecipient apply(User from)
                {
                    return new NotificationRecipient(from);
                }
            });
    
            String fromAddress = MailFactory.getServerManager().getDefaultSMTPMailServer().getDefaultFrom();
            reportEmailer.addMailToQueue(recipients, i18n.getText("wfshare.report.email.workflow.import.report"),report,fromAddress, i18n.getText("wfshare.report.email.jira.workflow.importer"));
        }
        catch (Throwable t)
        {
            t.printStackTrace();
        }
        
    }

    private ImportableJiraWorkflow getJiraWorkflowFromPlan(SharedWorkflowImportPlan plan) throws Exception
    {
        ImportableJiraWorkflow jiraWorkflow;
        
        try
        {
            String xml = FileUtils.readFileToString(plan.getWorkflowXml());
            WorkflowDescriptor descriptor = WorkflowUtil.convertXMLtoWorkflowDescriptor(xml);
            jiraWorkflow = new ImportableJiraWorkflow(plan.getWorkflowName(), descriptor, workflowManager);

        }
        catch (Exception e)
        {
            throw new Exception(i18n.getText("wfshare.exception.unable.to.create.workflow.descriptor"), e);
        }
        
        return jiraWorkflow;
    }

    private Set<Status> importStatuses(SharedWorkflowImportPlan plan, ImportableJiraWorkflow jiraWorkflow) throws Exception
    {
        Set<Status> createdStatuses = new HashSet<Status>();
        if (null != plan.getStatusMappings() && !plan.getStatusMappings().isEmpty())
        {
            try
            {
                createdStatuses = updateStatuses(jiraWorkflow, plan.getStatusMappings());
            }
            catch (Throwable t)
            {
                throw new Exception(i18n.getText("wfshare.exception.unable.to.migrate.statuses"), t);
            }
        }

        return createdStatuses;
    }
    
    private Set<String> importPlugins(SharedWorkflowImportPlan plan, ImportableJiraWorkflow jiraWorkflow) throws Exception
    {
        Set<String> installedPluginKeys = new HashSet<String>();
        
        if (null != plan.getPluginInstallInfo() && !plan.getPluginInstallInfo().isEmpty())
        {
            try
            {
                installedPluginKeys = installPluginsOrRemoveFromWorkflow(plan.getPluginInstallInfo(), plan.getLibFolder(), jiraWorkflow);
            }
            catch (Throwable t)
            {
                throw new Exception(i18n.getText("wfshare.exception.unable.to.install.plugins"), t);
            }
        }
        
        return installedPluginKeys;
    }

    private CustomFieldCreationResult importCustomFields(SharedWorkflowImportPlan plan, ImportableJiraWorkflow jiraWorkflow) throws Exception
    {
        List<CustomFieldInfo> fieldInfoList;
        try
        {
            fieldInfoList = plan.getCustomFieldInfo();
        }
        catch (Throwable t)
        {
            throw new Exception(i18n.getText("wfshare.exception.unable.to.read.custom.field.data"), t);
        }

        CustomFieldCreationResult result = new CustomFieldCreationResult();
        
        if (fieldInfoList != null && !fieldInfoList.isEmpty())
        {
            try
            {
                result = createCustomFields(fieldInfoList, jiraWorkflow.getName());
            }
            catch (Throwable t)
            {
                throw new Exception(i18n.getText("wfshare.exception.unable.to.migrate.custom.fields"), t);
            }
        }
        
        return result;
    }

    private void updateWorkflowCustomFieldIds(ImportableJiraWorkflow jiraWorkflow, Map<String, String> oldToNewIdMapping)
    {
        WorkflowExtensionsHelper.ExtensionCustomFieldUpdateResult result = workflowExtensionsHelper.updateWorkflowCustomFieldIds(jiraWorkflow, oldToNewIdMapping);
        reporter.addCustomFieldMappings(result);
    }
    
    private ScreenCreationResult importScreens(SharedWorkflowImportPlan plan, ImportableJiraWorkflow jiraWorkflow, Map<String, String> fieldMappings) throws Exception
    {
        List<ScreenInfo> screenInfoList;
        try
        {
            screenInfoList = plan.getScreenInfo();
        }
        catch (Throwable t)
        {
            throw new Exception(i18n.getText("wfshare.exception.unable.to.read.screen.data"), t);
        }

        ScreenCreationResult result = new ScreenCreationResult();
        
        if (screenInfoList != null && !screenInfoList.isEmpty())
        {
            try
            {
                result = createScreens(screenInfoList, fieldMappings, jiraWorkflow.getName());
            }
            catch (Throwable t)
            {
                throw new Exception(i18n.getText("wfshare.exception.unable.to.migrate.screens"), t);
            }
        }
        
        return result;
    }

    private void updateWorkflowScreenIds(ImportableJiraWorkflow jiraWorkflow, Map<Long, Long> oldToNewIdMapping)
    {
        workflowScreensHelper.updateWorkflowScreenIds(jiraWorkflow,oldToNewIdMapping);
        jiraWorkflow.resetFieldScreens();
    }

    private void createWorkflow(ImportableJiraWorkflow jiraWorkflow) throws Exception
    {
        try
        {
            workflowManager.createWorkflow(jiraAuthenticationContext.getLoggedInUser(), jiraWorkflow);
            reporter.addCreatedWorkflow(jiraWorkflow.getName());
        }
        catch (Throwable t)
        {
            throw new Exception(i18n.getText("wfshare.exception.unable.to.create.workflow"), t);
        }
    }

    private void importLayoutAndAnnotations(SharedWorkflowImportPlan plan)
    {
        try
        {
            if (plan.getLayoutJson().exists())
            {
                String layout = FileUtils.readFileToString(plan.getLayoutJson());
                String layoutPropKey = WorkflowDesignerConstants.LAYOUT_PREFIX.concat(plan.getWorkflowName());

                workflowDesignerPropertySet.setProperty(layoutPropKey, layout);
            }

            if (plan.getAnnotationsJson().exists())
            {
                String annotationJson = FileUtils.readFileToString(plan.getAnnotationsJson());
                String annotationPropKey = WorkflowDesignerConstants.ANNOTATION_PREFIX.concat(plan.getWorkflowName());
                workflowDesignerPropertySet.setProperty(annotationPropKey, annotationJson);
            }

        }
        catch (Throwable t)
        {
            //just ignore, no biggie if layout and annotations are missing
        }
    }

    private void rollback(Set<Status> createdStatuses, Set<String> installedPluginKeys, Set<CustomField> createdCustomFields, Set<FieldScreen> createdScreens)
    {
        removeStatuses(createdStatuses);
        removePlugins(installedPluginKeys);
        removeCustomFields(createdCustomFields);
        removeScreens(createdScreens);
    }

    private void removePlugins(Set<String> installedPluginKeys)
    {
        for (String key : installedPluginKeys)
        {
            Plugin plugin = pluginAccessor.getPlugin(key);
            if (null != plugin)
            {
                pluginController.uninstall(plugin);
            }
        }
    }

    private Set<String> installPluginsOrRemoveFromWorkflow(List<PluginInstallInfo> pluginInstallInfo, File libFolder, ImportableJiraWorkflow jiraWorkflow) throws Exception
    {
        removeExtensionsFromWorkflow(jiraWorkflow, ImmutableList.copyOf(Collections2.filter(pluginInstallInfo, new Predicate<PluginInstallInfo>()
        {
            @Override
            public boolean apply(PluginInstallInfo pluginInstallInfo)
            {
                return pluginInstallInfo.getRemoveExtensions();
            }
        })));

        reportAlreadyInstalled(ImmutableList.copyOf(Collections2.filter(pluginInstallInfo, new Predicate<PluginInstallInfo>()
        {
            @Override
            public boolean apply(PluginInstallInfo pluginInstallInfo)
            {
                return (!pluginInstallInfo.getInstallable() && pluginInstallInfo.getAlreadyInstalled());
            }
        })));
        
        return
                installPlugins(ImmutableList.copyOf(Collections2.filter(pluginInstallInfo, new Predicate<PluginInstallInfo>()
                {
                    @Override
                    public boolean apply(PluginInstallInfo pluginInstallInfo)
                    {
                        return (pluginInstallInfo.getInstallable());
                    }
                })), libFolder);
    }

    private void reportAlreadyInstalled(ImmutableList<PluginInstallInfo> pluginInstallInfo)
    {
        for (PluginInstallInfo installInfo : pluginInstallInfo)
        {
            reporter.addAlreadyInstalledPlugin(installInfo);
        }
    }

    private void removeExtensionsFromWorkflow(ImportableJiraWorkflow jiraWorkflow, List<PluginInstallInfo> pluginInstallInfo)
    {
        Set<String> allClasses = new HashSet<String>();

        for (PluginInstallInfo installInfo : pluginInstallInfo)
        {
            reporter.addUninstalledPlugin(installInfo);
            
            List<String> classes = installInfo.getPluginInfo().getClassNames();
            if (null != classes && !classes.isEmpty())
            {
                allClasses.addAll(classes);
            }
        }

        WorkflowExtensionsHelper.ExtensionRemovalResult result = workflowExtensionsHelper.removeExtensionsWithClassnames(jiraWorkflow, allClasses);
        reporter.addRemovedExtensions(result);
    }

    private void removeStatuses(Set<Status> statuses)
    {
        for (Status status : statuses)
        {
            try
            {
                workflowStatusHelper.removeStatus(status.getId());
            }
            catch (Throwable t)
            {
                //ignore
            }
        }
    }

    private Set<String> installPlugins(List<PluginInstallInfo> pluginInstallInfo, File libFolder) throws Exception
    {
        Set<PluginArtifact> artifacts = new HashSet<PluginArtifact>();
        Set<String> installedPluginKeys = Collections.<String>emptySet();

        for (PluginInstallInfo installInfo : pluginInstallInfo)
        {
            if (installInfo.getInstallable() && !installInfo.getAlreadyInstalled())
            {
                switch (installInfo.getSource())
                {
                    case PAC:
                    {
                        artifacts.add(createPluginArtifactFromPac(installInfo, libFolder));
                        break;
                    }
                    case LIB:
                    {
                        artifacts.add(createPluginArtifactFromLib(installInfo, libFolder));
                        break;
                    }
                }
                
                reporter.addInstalledPlugin(installInfo);
            }
            else if(installInfo.getAlreadyInstalled())
            {
                reporter.addAlreadyInstalledPlugin(installInfo);
            }
        }

        if (!artifacts.isEmpty())
        {
            installedPluginKeys = pluginController.installPlugins(artifacts.toArray(new PluginArtifact[0]));
        }

        return installedPluginKeys;
    }

    private PluginArtifact createPluginArtifactFromLib(PluginInstallInfo installInfo, File libFolder) throws FileNotFoundException
    {
        File jar = new File(libFolder, installInfo.getPluginInfo().getArtifactName());
        if (!jar.exists())
        {
            throw new FileNotFoundException(i18n.getText("wfshare.exception.cannot.find.plugin.file", jar.getName()));
        }

        return new JarPluginArtifact(jar);
    }

    private PluginArtifact createPluginArtifactFromPac(PluginInstallInfo installInfo, File libFolder) throws Exception
    {
        String newFilename = createPluginFilename(installInfo.getPluginInfo().getKey(), installInfo.getInstallVersion());
        File jar = new File(libFolder, newFilename);
        pacFileDownloader.downloadFile(installInfo.getPacDownloadUrl().toString(),jar);

        return new JarPluginArtifact(jar);
    }

    private String createPluginFilename(String key, String version)
    {
        String name = key + "-" + version + ".jar";//NON-NLS
        return name.replaceAll(CLEAN_FILENAME_PATTERN, "-");
    }

    private Set<Status> updateStatuses(ImportableJiraWorkflow jiraWorkflow, List<StatusMapping> statusMappings) throws Exception
    {
        WorkflowDescriptor descriptor = jiraWorkflow.getDescriptor();
        Set<Status> createdStatuses = new HashSet<Status>();
        Map<String, StepDescriptor> statusStepMap = Maps.uniqueIndex(descriptor.getSteps(), new Function<StepDescriptor, String>()
        {

            @Override
            public String apply(StepDescriptor stepDescriptor)
            {
                return (String) stepDescriptor.getMetaAttributes().get(JIRA_STATUS_ID);
            }
        });

        for (StatusMapping mapping : statusMappings)
        {
            boolean created = false;
            if (mapping.getNewId().equals("-1"))
            {
                try
                {
                    Status status = workflowStatusHelper.createStatus(mapping.getNewName(), "", NEW_STATUS_DEFAULT_ICON);
                    mapping.setNewId(status.getId());
                    createdStatuses.add(status);
                    created = true;
                }
                catch (Throwable t)
                {
                    if (!createdStatuses.isEmpty())
                    {
                        removeStatuses(createdStatuses);
                    }

                    throw new Exception(t);
                }
            }

            StepDescriptor step = statusStepMap.get(mapping.getOriginalId());
            if (null != step)
            {
                step.getMetaAttributes().put(JIRA_STATUS_ID, mapping.getNewId());
            }
            
            if(created)
            {
                reporter.addCreatedStatus(step.getName(),mapping);
            }
            else
            {
                reporter.addMappedStatus(step.getName(),mapping);
            }
        }

        return createdStatuses;
    }

    private CustomFieldCreationResult createCustomFields(List<CustomFieldInfo> fieldInfoList, String workflowName) throws Exception
    {
        CustomFieldCreationResult result = new CustomFieldCreationResult();
        
        for (CustomFieldInfo fieldInfo : fieldInfoList)
        {
            try
            {
                String fieldPluginKey = fieldInfo.getPluginKey();
                if (StringUtils.isBlank(fieldPluginKey) || pluginAccessor.isPluginEnabled(fieldPluginKey))
                {
                    fieldInfo.setDescription(i18n.getText("wfshare.import.label.created.by.workflow.import", fieldInfo.getDescription(), workflowName));
                    
                    CustomField cf = customFieldCreator.createCustomField(fieldInfo);
                    result.addCustomField(cf);
                    result.addMapping(fieldInfo.getOriginalId(),cf.getId());
                    reporter.addCreatedCustomField(cf);
                }
            }
            catch (Throwable t)
            {
                if (!result.getCreatedFields().isEmpty())
                {
                    removeCustomFields(result.getCreatedFields());
                }

                throw new Exception(t);
            }
        }

        return result;
    }

    private ScreenCreationResult createScreens(List<ScreenInfo> screenInfoList, Map<String, String> createdFieldsMapping, String workflowName) throws Exception
    {
        ScreenCreationResult result = new ScreenCreationResult();
        
        for(ScreenInfo screenInfo : screenInfoList)
        {
            try
            {
                screenInfo.setDescription(i18n.getText("wfshare.import.label.created.by.workflow.import", screenInfo.getDescription(), workflowName)); 
                FieldScreen screen = screenCreator.createScreen(screenInfo);
                
                result.addScreen(screen);
                result.addMapping(screenInfo.getOriginalId(),screen.getId());
                
                screenCreator.addScreenTabs(screen,screenInfo,createdFieldsMapping);
                
                reporter.addCreatedScreen(screen);
                reporter.addScreenMapping(screen.getName(),screenInfo.getOriginalId(),screen.getId());
            }
            catch (Throwable t)
            {
                if(!result.getCreatedScreens().isEmpty())
                {
                    removeScreens(result.getCreatedScreens());
                }
                throw new Exception(t);
                
            }
        }
        
        return result;
    }

    private void removeCustomFields(Set<CustomField> createdFields)
    {
        for (CustomField field : createdFields)
        {
            try
            {
                customFieldCreator.removeCustomFieldAndSchemes(field);
            }
            catch (RemoveException e)
            {
                //ignore
            }
        }
    }

    private void removeScreens(Set<FieldScreen> createdScreens)
    {
        for (FieldScreen screen : createdScreens)
        {
            screenCreator.removeScreen(screen.getId());
        }
    }

    private File getTempFolder(String name) throws IOException
    {
        String folderName = name.replaceAll(CLEAN_FILENAME_PATTERN, "-") + "-" + System.nanoTime();

        return new File(getBaseCacheFolder(), folderName);
    }

    private class ScreenCreationResult {
        
        private Set<FieldScreen> createdScreens;
        private Map<Long,Long> oldToNewIdMapping;

        private ScreenCreationResult()
        {
            this.createdScreens = new HashSet<FieldScreen>();
            this.oldToNewIdMapping = new HashMap<Long, Long>();
        }

        public void addScreen(FieldScreen screen)
        {
            this.createdScreens.add(screen);
        }

        public void addMapping(Long oldId, Long newId)
        {
            this.oldToNewIdMapping.put(oldId,newId);
        }

        public Set<FieldScreen> getCreatedScreens()
        {
            return createdScreens;
        }

        public Map<Long, Long> getOldToNewIdMapping()
        {
            return oldToNewIdMapping;
        }
    }

    private class CustomFieldCreationResult 
    {
        private Set<CustomField> createdFields;
        private Map<String,String> oldToNewIdMapping;

        private CustomFieldCreationResult()
        {
            this.createdFields = new HashSet<CustomField>();
            this.oldToNewIdMapping = new HashMap<String, String>();
        }
        
        public void addCustomField(CustomField field)
        {
            this.createdFields.add(field);
        }
        
        public void addMapping(String oldId, String newId)
        {
            this.oldToNewIdMapping.put(oldId,newId);
        }

        public Set<CustomField> getCreatedFields()
        {
            return createdFields;
        }

        public Map<String, String> getOldToNewIdMapping()
        {
            return oldToNewIdMapping;
        }
    }
}
