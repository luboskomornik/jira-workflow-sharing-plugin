package com.atlassian.jira.plugin.workflow.sharing.util;

import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * @since version
 */
public class ZipUtils
{

    public static void unzip(final File zipFile, final String destDir) throws IOException
    {
        unzip(zipFile, new File(destDir), 0);
    }

    public static void unzip(final File zipFile, final File destDir) throws IOException
    {
        unzip(zipFile, destDir, 0);
    }
    
    /**
     * Unzips a file
     *
     * @param zipFile
     *            the Zip file
     * @param destDir
     *            the destination folder
     * @param leadingPathSegmentsToTrim
     *            number of root folders to skip. Example: If all files are in generated-resources/home/*,
     *            then you may want to skip 2 folders.
     * @throws IOException
     */
    public static void unzip(final File zipFile, final File destDir, int leadingPathSegmentsToTrim) throws IOException
    {
        final ZipFile zip = new ZipFile(zipFile);
        try
        {
            final Enumeration<? extends ZipEntry> entries = zip.entries();
            while (entries.hasMoreElements())
            {
                final ZipEntry zipEntry = entries.nextElement();
                String zipPath = trimPathSegments(zipEntry.getName(), leadingPathSegmentsToTrim);
                final File file = new File(destDir, zipPath);
                if (zipEntry.isDirectory())
                {
                    file.mkdirs();
                    continue;
                }
                // make sure our parent exists in case zipentries are out of order
                if (!file.getParentFile().exists())
                {
                    file.getParentFile().mkdirs();
                }

                InputStream is = null;
                OutputStream fos = null;
                try
                {
                    is = zip.getInputStream(zipEntry);
                    fos = new FileOutputStream(file);
                    IOUtils.copy(is, fos);
                }
                finally
                {
                    IOUtils.closeQuietly(is);
                    IOUtils.closeQuietly(fos);
                }
                file.setLastModified(zipEntry.getTime());
            }
        }
        finally
        {
            try
            {
                zip.close();
            }
            catch (IOException e)
            {
                // ignore
            }
        }
    }

    private static String trimPathSegments(String zipPath, final int trimLeadingPathSegments)
    {
        int startIndex = 0;
        for (int i = 0; i < trimLeadingPathSegments; i++)
        {
            int nextSlash = zipPath.indexOf("/", startIndex);
            if (nextSlash == -1)
            {
                break;
            }
            else
            {
                startIndex = nextSlash + 1;
            }
        }

        return zipPath.substring(startIndex);
    }
}
