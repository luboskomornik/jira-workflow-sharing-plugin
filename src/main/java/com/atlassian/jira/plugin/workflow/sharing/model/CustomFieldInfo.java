package com.atlassian.jira.plugin.workflow.sharing.model;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since version
 */
public class CustomFieldInfo implements Serializable
{
    private static final long serialVersionUID = 5388956019740247364L;
    private String originalId;
    private String name;
    private String description;
    private String typeModuleKey;
    private String searcherModuleKey;
    private String defaultValueXml;
    private List<OptionInfo> options;
    private String pluginKey;

    @JsonCreator
    public CustomFieldInfo(
            @JsonProperty("originalId") String originalId
            ,@JsonProperty("name") String name
            ,@JsonProperty("description") String description
            ,@JsonProperty("typeModuleKey") String typeModuleKey
            ,@JsonProperty("searcherModuleKey") String searcherModuleKey
            ,@JsonProperty("defaultValueXml") String defaultValueXml
            ,@JsonProperty("options") List<OptionInfo> options
            ,@JsonProperty("pluginKey") String pluginKey)
    {
        this.originalId = originalId;
        this.name = name;
        this.description = description;
        this.typeModuleKey = typeModuleKey;
        this.searcherModuleKey = searcherModuleKey;
        this.defaultValueXml = defaultValueXml;
        this.options = options;
        this.pluginKey = pluginKey;
    }

    public String getOriginalId()
    {
        return originalId;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getTypeModuleKey()
    {
        return typeModuleKey;
    }

    public String getSearcherModuleKey()
    {
        return searcherModuleKey;
    }

    public String getDefaultValueXml()
    {
        return defaultValueXml;
    }

    public List<OptionInfo> getOptions()
    {
        return options;
    }

    public String getPluginKey()
    {
        return pluginKey;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(originalId).append(name).append(typeModuleKey).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this) { return true; }

        if(!(obj instanceof CustomFieldInfo)) { return false; }

        CustomFieldInfo info = (CustomFieldInfo) obj;

        return new EqualsBuilder().append(this.getOriginalId(),info.getOriginalId()).append(this.getName(), info.getName()).append(this.getTypeModuleKey(),info.getTypeModuleKey()).isEquals();
    }

    @Override
    public String toString()
    {
        return "CustomFieldInfo name:" + name + " type module key:" + typeModuleKey; //NON-NLS
    }
}
