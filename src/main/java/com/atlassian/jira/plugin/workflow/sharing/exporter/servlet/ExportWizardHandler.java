package com.atlassian.jira.plugin.workflow.sharing.exporter.servlet;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.atlassian.jira.plugin.workflow.sharing.WorkflowExtensionsHelper;
import com.atlassian.jira.plugin.workflow.sharing.exporter.component.JiraWorkflowSharingExporter;
import com.atlassian.jira.plugin.workflow.sharing.servlet.AbstractServletHandler;
import com.atlassian.jira.util.lang.Pair;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import org.apache.commons.lang.StringUtils;

/**
 * @since version
 */
public class ExportWizardHandler extends AbstractServletHandler
{
    private enum SessionVar
    {
        WFSHARE_EXPORT_WFNAME, WFSHARE_EXPORT_WFMODE, WFSHARE_EXPORT_NOTES, WFSHARE_ORIGINAL_URL
    }

    public static final String ALLOW_TRACKING_FIELD_NAME = "wfShareAllowTracking";
    public static final String WF_NAME_FIELD_NAME = "workflowName";
    public static final String WF_MODE_FIELD_NAME = "workflowMode";
    public static final String NOTES_FIELD_NAME = "notes";
    
    private final TemplateRenderer renderer;
    private final JiraWorkflowSharingExporter jiraWorkflowSharingExporter;
    private final WorkflowExtensionsHelper workflowExtensionsHelper;
    private final VelocityRequestContextFactory requestContextFactory;
    private final I18nResolver i18n;

    protected ExportWizardHandler(LoginUriProvider loginUriProvider, WebSudoManager webSudoManager, UserManager userManager, TemplateRenderer renderer, JiraWorkflowSharingExporter jiraWorkflowSharingExporter, WorkflowExtensionsHelper workflowExtensionsHelper, VelocityRequestContextFactory requestContextFactory, I18nResolver i18n)
    {
        super(loginUriProvider, webSudoManager, userManager);
        this.renderer = renderer; 
        this.jiraWorkflowSharingExporter = jiraWorkflowSharingExporter; 
        this.workflowExtensionsHelper = workflowExtensionsHelper; 
        this.requestContextFactory = requestContextFactory;
        this.i18n = i18n;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        if (enforceAdminLogin(request, response))
        {
            return;
        }

        response.setContentType("text/html;charset=utf-8");//NON-NLS

        //make sure login redirect info is cleared
        super.clearSessionAttributes(request.getSession());

        final Map<String, Object> context = new HashMap<String, Object>();

        context.put("requestContext", requestContextFactory.getJiraVelocityRequestContext());//NON-NLS

        ExportServletMapping requestMapping = ExportServletMapping.fromPath(getMappingPath(request));

        //if we're not coming from our own path, start a new session/import
        String referer = request.getHeader("referer");//NON-NLS
        if (!StringUtils.contains(referer, "/plugins/servlet/wfshare-export"))//NON-NLS
        {

            clearSessionAttributes(request.getSession());
            request.getSession().setAttribute(SessionVar.WFSHARE_ORIGINAL_URL.name(), referer);
            requestMapping = ExportServletMapping.EXPORT_VERIFY_PLUGINS;
        }
        
        try
        {
            String originalUrl = getSessionAttributeOrNull(request, SessionVar.WFSHARE_ORIGINAL_URL.name());
            if (null != originalUrl)
            {
                context.put("cancelUrl", originalUrl);//NON-NLS
            }
            else
            {
                context.put("cancelUrl", "");//NON-NLS
            }
            
            switch (requestMapping)
            {
                case EXPORT_VERIFY_PLUGINS:
                    showVerifyPlugins(request, response, context);
                    break;
                case EXPORT_ADD_NOTES:
                    showAddNotes(request, response, context);
                    break;
                case EXPORT_WORKFLOW:
                    handleExport(request, response, context);
                    break;
                case NOT_MAPPED:
                    showVerifyPlugins(request, response, context);
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            String error = (e.getMessage() != null)? e.getMessage() : e.getClass().getName();
            String message = i18n.getText("wfshare.exception.generic.error", error);

            String errorTemplate = ExportServletMapping.ERROR.getResultTemplate();
            context.put("errorMessage", message);//NON-NLS

            renderer.render(errorTemplate, context, response.getWriter());
        }
    }

    private void showVerifyPlugins(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws Exception
    {
        String wfName = getSessionAttributeOrNull(request,SessionVar.WFSHARE_EXPORT_WFNAME.name());
        String wfMode = getSessionAttributeOrNull(request,SessionVar.WFSHARE_EXPORT_WFMODE.name());

        Pair<String,String> nameAndMode = extractWorkflowNameAndMode(request);
        if(null != nameAndMode)
        {
            wfName = nameAndMode.first();
            wfMode = nameAndMode.second();

            request.getSession().setAttribute(SessionVar.WFSHARE_EXPORT_WFNAME.name(), wfName);
            request.getSession().setAttribute(SessionVar.WFSHARE_EXPORT_WFMODE.name(), wfMode);
        }
        
        if(StringUtils.isBlank(wfName))
        {
            throw new Exception(i18n.getText("wfshare.exception.no.workflow.name.param"));
        }
        
        context.put("workflowName",wfName);//NON-NLS

        JiraWorkflow workflow = workflowExtensionsHelper.getWorkflowForNameAndMode(wfName,wfMode);
        
        if(null == workflow)
        {
            throw new Exception(i18n.getText("wfshare.exception.workflow.not.found",wfName));
        }

        Map<Plugin, Set<String>> requiredPlugins = workflowExtensionsHelper.getRequiredPlugins(workflow);

        if(requiredPlugins.isEmpty())
        {
            showAddNotes(request, response, context);
            return;
        }
        
        for(Plugin plugin : requiredPlugins.keySet())
        {
            if(!(plugin instanceof OsgiPlugin))
            {
                throw new Exception(i18n.getText("wfshare.exception.cannot.include.v1.plugin",plugin.getName()));
            }
        }

        setNavigationPaths(context, ExportServletMapping.EXPORT_VERIFY_PLUGINS);
        context.put("requiredPlugins",requiredPlugins);//NON-NLS
        renderer.render(ExportServletMapping.EXPORT_VERIFY_PLUGINS.getResultTemplate(), context, response.getWriter());
    }

    private void showAddNotes(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws Exception
    {
        
        setNavigationPaths(context, ExportServletMapping.EXPORT_ADD_NOTES);
        renderer.render(ExportServletMapping.EXPORT_ADD_NOTES.getResultTemplate(), context, response.getWriter());
    }
    
    private void handleExport(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context) throws Exception
    {
        String notes = request.getParameter(NOTES_FIELD_NAME);
        boolean allowTracking = Boolean.parseBoolean(request.getParameter(ALLOW_TRACKING_FIELD_NAME));
        
        String wfName = getSessionAttribute(request, SessionVar.WFSHARE_EXPORT_WFNAME.name(), i18n.getText("wfshare.exception.wf.name.not.found.in.session"));
        String wfMode = getSessionAttribute(request, SessionVar.WFSHARE_EXPORT_WFMODE.name(), i18n.getText("wfshare.exception.wf.mode.not.found.in.session"));

        File zipFile = null;
        if ("draft".equalsIgnoreCase(wfMode))//NON-NLS
        {
            zipFile = jiraWorkflowSharingExporter.exportDraftWorflow(wfName,true, notes,allowTracking);
        }
        else
        {
            zipFile = jiraWorkflowSharingExporter.exportActiveWorkflow(wfName, true, notes,allowTracking);
        }

        if(null == zipFile)
        {
            throw new Exception(i18n.getText("wfshare.exception.null.workflow.zip",wfName));
        }
        
        clearSessionAttributes(request.getSession());
        context.put("exportedZip",zipFile.getName());//NON-NLS
        renderer.render(ExportServletMapping.EXPORT_WORKFLOW.getResultTemplate(), context, response.getWriter());
    }
    
    private Pair<String,String> extractWorkflowNameAndMode(HttpServletRequest request) throws Exception
    {
        String wfName = request.getParameter(WF_NAME_FIELD_NAME);
        String wfMode = request.getParameter(WF_MODE_FIELD_NAME);

        if(StringUtils.isBlank(wfName))
        {
            return null;
        }

        if(StringUtils.isBlank(wfMode))
        {
            wfMode = "active";//NON-NLS
        }
        
        return Pair.strictPairOf(wfName, wfMode);
    }

    private void setNavigationPaths(Map<String, Object> context, ExportServletMapping servletMapping)
    {
        String nextPath = "";
        String backPath = "";
        if (null != servletMapping.next())
        {
            nextPath = "/plugins/servlet/wfshare-export/" + servletMapping.next().getPath();//NON-NLS
        }

        if (null != servletMapping.previous())
        {
            backPath = "/plugins/servlet/wfshare-export/" + servletMapping.previous().getPath();//NON-NLS
        }

        context.put("nextUrl", requestContextFactory.getJiraVelocityRequestContext().getBaseUrl() + nextPath);//NON-NLS
        context.put("backUrl", requestContextFactory.getJiraVelocityRequestContext().getBaseUrl() + backPath);//NON-NLS
    }

    @Override
    protected void clearSessionAttributes(HttpSession session)
    {
        super.clearSessionAttributes(session);
        for (SessionVar sessionVar : SessionVar.values())
        {
            session.removeAttribute(sessionVar.name());
        }
    }
}
