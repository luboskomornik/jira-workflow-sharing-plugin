package com.atlassian.jira.plugin.workflow.sharing;

import java.io.File;
import java.net.URI;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;

/**
 * @since version
 */
public class PluginArtifactFinder
{

    public File findArtifact(Plugin plugin) throws Exception
    {
        String location = null;
        
        if(plugin instanceof OsgiPlugin)
        {
            OsgiPlugin osgiPlugin = (OsgiPlugin) plugin;
            location = osgiPlugin.getBundle().getLocation();
        }
        
        return new File(new URI(location));
    }

}
