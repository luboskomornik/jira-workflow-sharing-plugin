package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.plugin.workflow.sharing.WorkflowExtensionsHelper;
import com.atlassian.jira.plugin.workflow.sharing.WorkflowScreensHelper;
import com.atlassian.jira.plugin.workflow.sharing.WorkflowSharingGoogleTracker;
import com.atlassian.jira.plugin.workflow.sharing.importer.JiraWorkflowSharingImporter;
import com.atlassian.jira.plugin.workflow.sharing.importer.JiraWorkflowSharingImporterImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.sal.api.message.I18nResolver;

import com.sysbliss.jira.plugins.workflow.util.WorkflowDesignerPropertySet;

/**
 * @since version
 */
public class WorkflowImporterFactoryImpl implements WorkflowImporterFactory
{
    private final JiraHome jiraHome;
    private final WorkflowStatusHelper statusHelper;
    private final PluginController pluginController;
    private final WorkflowManager workflowManager;
    private final WorkflowExtensionsHelper workflowExtensionsHelper;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final PluginAccessor pluginAccessor;
    private final WorkflowDesignerPropertySet workflowDesignerPropertySet;
    private final CustomFieldCreator customFieldCreator;
    private final ScreenCreator screenCreator;
    private final WorkflowScreensHelper workflowScreensHelper;
    private final ReportEmailer reportEmailer;
    private final UserUtil userUtil;
    private final WorkflowSharingGoogleTracker googleTracker;
    private final PacFileDownloader pacFileDownloader;
    private final I18nResolver i18n;

    public WorkflowImporterFactoryImpl(JiraHome jiraHome, WorkflowStatusHelper statusHelper, PluginController pluginController, WorkflowManager workflowManager, WorkflowExtensionsHelper workflowExtensionsHelper, JiraAuthenticationContext jiraAuthenticationContext, PluginAccessor pluginAccessor, WorkflowDesignerPropertySet workflowDesignerPropertySet, CustomFieldCreator customFieldCreator, ScreenCreator screenCreator, WorkflowScreensHelper workflowScreensHelper, ReportEmailer reportEmailer, UserUtil userUtil, WorkflowSharingGoogleTracker googleTracker, PacFileDownloader pacFileDownloader, I18nResolver i18n)
    {
        this.jiraHome = jiraHome;
        this.statusHelper = statusHelper;
        this.pluginController = pluginController;
        this.workflowManager = workflowManager;
        this.workflowExtensionsHelper = workflowExtensionsHelper;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.pluginAccessor = pluginAccessor;
        this.workflowDesignerPropertySet = workflowDesignerPropertySet;
        this.customFieldCreator = customFieldCreator;
        this.screenCreator = screenCreator;
        this.workflowScreensHelper = workflowScreensHelper;
        this.reportEmailer = reportEmailer; 
        this.userUtil = userUtil;
        this.googleTracker = googleTracker;
        this.pacFileDownloader = pacFileDownloader;
        this.i18n = i18n;
    }

    @Override
    public JiraWorkflowSharingImporter newImporter()
    {
        return new JiraWorkflowSharingImporterImpl(jiraHome, statusHelper,pluginController,workflowManager,workflowExtensionsHelper,jiraAuthenticationContext,pluginAccessor,workflowDesignerPropertySet,customFieldCreator,screenCreator,workflowScreensHelper, reportEmailer, userUtil, googleTracker, pacFileDownloader, i18n);
    }
}
