package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import com.atlassian.jira.plugin.workflow.sharing.importer.JiraWorkflowSharingImporter;

/**
 * @since version
 */
public interface WorkflowImporterFactory
{
    JiraWorkflowSharingImporter newImporter();
}
