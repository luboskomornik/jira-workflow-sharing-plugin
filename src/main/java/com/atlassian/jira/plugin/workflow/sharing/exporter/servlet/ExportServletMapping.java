package com.atlassian.jira.plugin.workflow.sharing.exporter.servlet;

/**
 * @since version
 */
public enum ExportServletMapping
{
    EXPORT_VERIFY_PLUGINS("wfshare-export", "exporter/export-verify-plugins.vm")//NON-NLS
    ,EXPORT_ADD_NOTES("add-notes","exporter/export-add-notes.vm")//NON-NLS
    ,EXPORT_WORKFLOW("export-workflow","exporter/export-success.vm")//NON-NLS
    ,ERROR("","export-error.vm")//NON-NLS
    ,NOT_MAPPED("","");

    public static final String TEMPLATE_PREFIX = "/templates/workflow-sharing/";//NON-NLS

    private String path;
    private String resultTemplate;

    private ExportServletMapping(String path, String template)
    {
        this.path = path;
        this.resultTemplate = TEMPLATE_PREFIX + template;
    }

    public String getPath()
    {
        return path;
    }

    public String getResultTemplate()
    {
        return resultTemplate;
    }

    public ExportServletMapping next()
    {
        if(this.ordinal() < (ExportServletMapping.values().length - 1))
        {
            return ExportServletMapping.values()[this.ordinal() + 1];
        }
        else
        {
            return null;
        }
    }

    public ExportServletMapping previous()
    {
        if(this.ordinal() > 0)
        {
            return ExportServletMapping.values()[this.ordinal() - 1];
        }
        else
        {
            return null;
        }
    }

    public static ExportServletMapping fromPath(String path)
    {
        for(ExportServletMapping mapping : ExportServletMapping.values())
        {
            if(mapping.getPath().equals(path))
            {
                return mapping;
            }
        }

        return NOT_MAPPED;
    }
}
