package com.atlassian.jira.plugin.workflow.sharing;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.jira.plugin.workflow.sharing.exception.VersionParseException;
import com.atlassian.sal.api.message.I18nResolver;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import org.apache.commons.lang.StringUtils;

/**
 * some code borrowed from maven-release-manager DefaultVersionInfo
 * 
 * @since version
 */
public class ArtifactVersion implements Comparable<ArtifactVersion>
{
    private static final String SNAPSHOT = "SNAPSHOT";
    private static final int DIGITS_INDEX = 1;
    private static final int QUALIFIER_SEPARATOR_INDEX = 2;
    private static final int QUALIFIER_INDEX = 3;
    private static final int QUALIFIER_REV_SEPARATOR_INDEX = 4;
    private static final int QUALIFIER_REVISION_INDEX = 5;
    private static final int BUILD_SEPARATOR_INDEX = 6;
    private static final int BUILD_SPECIFIER_INDEX = 7;
    
    public static final Pattern STANDARD_PATTERN = Pattern.compile(
            "^((?:\\d+\\.)*\\d+)" +      // digit(s) and '.' repeated - followed by digit (version digits 1.22.0, etc)
                    "([-_])?" +                 // optional - or _  (annotation separator)
                    "([a-zA-Z]*)" +             // alpha characters (looking for annotation - alpha, beta, RC, etc.)
                    "([-_])?" +                 // optional - or _  (annotation revision separator)
                    "(\\d*)" +                  // digits  (any digits after rc or beta is an annotation revision)
                    "(?:([-_])?(.*?))?$" );      // - or _ followed everything else (build specifier)

    private final String versionLabel;
    private List<Integer> digits;
    private String qualifier;
    private Integer qualifierRevision;
    private String build;
    private final I18nResolver i18n;
    
    public ArtifactVersion(String versionLabel, I18nResolver i18n) throws VersionParseException
    {
        this.i18n = i18n;
        this.versionLabel = versionLabel;
        Matcher m = STANDARD_PATTERN.matcher(versionLabel);
        if ( m.matches() )
        {
            digits = parseDigits(m.group(DIGITS_INDEX));

            if (!SNAPSHOT.equals(m.group(QUALIFIER_INDEX)))
            {
                qualifier = m.group(QUALIFIER_INDEX);

                if(StringUtils.isNotBlank(m.group(QUALIFIER_REV_SEPARATOR_INDEX))
                        && StringUtils.isBlank(m.group(QUALIFIER_REVISION_INDEX)))
                {
                    build = m.group(BUILD_SPECIFIER_INDEX);
                }
                else
                {
                    qualifierRevision = integerOrNull(m.group(QUALIFIER_REVISION_INDEX));
                    build = m.group(BUILD_SPECIFIER_INDEX);
                }
            }
            else
            {
                build = SNAPSHOT;
            }
        }
        else
        {
            throw new VersionParseException(this.i18n.getText("wfshare.exception.error.parsing.version", versionLabel));
        }
    }

    @Override
    public int compareTo(ArtifactVersion that)
    {
        int result = getMajorVersion() - that.getMajorVersion();
        if ( result == 0 )
        {
            result = getMinorVersion() - that.getMinorVersion();
        }
        if ( result == 0 )
        {
            result = getMicroVersion() - that.getMicroVersion();
        }
        if ( result == 0 )
        {
            if(StringUtils.isNotBlank(qualifier))
            {
                String thatQualifier = that.getQualifier();

                if (StringUtils.isNotBlank(thatQualifier))
                {
                    if ((qualifier.length() > thatQualifier.length())
                            && qualifier.startsWith(thatQualifier))
                    {
                        // here, the longer one that otherwise match is considered older
                        result = -1;
                    }
                    else if ((qualifier.length() < thatQualifier.length())
                            && thatQualifier.startsWith(qualifier))
                    {
                        // here, the shorter one that otherwise match is considered newer
                        result = 1;
                    }
                    else
                    {
                        result = qualifier.compareTo(thatQualifier);
                    }
                    
                    if(result == 0)
                    {
                        Integer thatQualifierRev = that.getQualifierRevision();
                        //qualifiers are the same, check the qualifier rev
                        if(qualifierRevision != null)
                        {
                            if(thatQualifier != null)
                            {
                                result = qualifierRevision.compareTo(thatQualifierRev);
                            }
                            else
                            {
                                //we have a revision but that doesn't, we're newer
                                result = 1;
                            }
                        }
                        else if(thatQualifierRev != null)
                        {
                            //that has a revision and we don't, that is newer
                            return -1;
                        }
                    }
                }
                else
                {
                    // otherVersion has no qualifier but we do - that's newer
                    result = -1;
                }
            }
            else if(StringUtils.isNotBlank(that.getQualifier()))
            {
                // otherVersion has a qualifier but we don't, we're newer
                result = 1;
            }

            String thatBuild = that.getBuild();
            if(StringUtils.isNotBlank(build) && !SNAPSHOT.equals(build))
            {
                if(StringUtils.isNotBlank(thatBuild))
                {
                    if(SNAPSHOT.equals(thatBuild))
                    {
                        result = -1;
                    }
                    else
                    {
                        result = build.compareTo(thatBuild);
                    }
                }
                else
                {
                    //we have a build, that doesn't, that is newer?
                    result = -1;
                }
            }
            else if(SNAPSHOT.equals(build))
            {
                //our snapshot is always newer
                result = 1;
            }
            else if(StringUtils.isNotBlank(thatBuild))
            {
                result = 1;
            }
        }

        return result;
    }

    public int getMajorVersion()
    {
        return (!digits.isEmpty()) ? digits.get(0).intValue() : 0;
    }

    public int getMinorVersion()
    {
        return (digits.size() > 1) ? digits.get(1).intValue() : 0;
    }

    public int getMicroVersion()
    {
        return (digits.size() > 2) ? digits.get(2).intValue() : 0;
    }

    public Integer getQualifierRevision()
    {
        return qualifierRevision;
    }

    public String getQualifier()
    {
        return qualifier;
    }

    public String getBuild()
    {
        return build;
    }

    public String getVersionString()
    {
        return versionLabel;
    }

    private List<Integer> parseDigits(String digitString)
    {
        return Lists.transform(Arrays.asList(StringUtils.split(digitString, '.')), new Function<String,Integer>()
        {
            @Override
            public Integer apply(String s)
            {
                return Integer.valueOf(s);
            }
        });
    }

    private Integer integerOrNull(String s)
    {
        if(StringUtils.isNotBlank(s) && StringUtils.isNumeric(s))
        {
            return Integer.valueOf(s);
        }

        return null;
    }
}
