package com.atlassian.jira.plugin.workflow.sharing.pac;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;

/**
 * @since version
 */
public class JWSPacClientFactoryImpl implements JWSPacClientFactory
{
    private final PacServiceFactory pacServiceFactory;
    private final ApplicationProperties applicationProperties;
    private final I18nResolver i18n;

    public JWSPacClientFactoryImpl(PacServiceFactory pacServiceFactory, ApplicationProperties applicationProperties, I18nResolver i18n)
    {
        this.pacServiceFactory = pacServiceFactory;
        this.applicationProperties = applicationProperties;
        this.i18n = i18n;
    }

    @Override
    public JWSPacClient getPacClient()
    {
        return new LegacyPacClientImpl(pacServiceFactory.getProductService(), pacServiceFactory.getPluginService(),pacServiceFactory.getPluginVersionService(), applicationProperties, i18n);
    }
}
