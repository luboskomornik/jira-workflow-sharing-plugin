package com.atlassian.jira.plugin.workflow.sharing;

/**
 * @since version
 */
public enum WorkflowSharingFiles {
    WORKFLOW("workflow.xml")//NON-NLS
    ,LAYOUT("layout.json")//NON-NLS
    ,ANNOTATION("annotation.json")//NON-NLS
    ,PLUGINS("plugins.json")//NON-NLS
    ,LIB("plugins/")//NON-NLS
    ,CUSTOM_FIELDS("customfields.json")//NON-NLS
    ,SCREENS("screens.json")//NON-NLS
    ,NOTES("notes.md");//NON-NLS

    private String path;

    WorkflowSharingFiles(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
