package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.sharing.model.CustomFieldInfo;

import org.ofbiz.core.entity.GenericEntityException;

/**
 * @since version
 */
public interface CustomFieldCreator
{
    CustomField createCustomField(CustomFieldInfo fieldInfo) throws GenericEntityException;
    
    void removeCustomFieldAndSchemes(CustomField field) throws RemoveException;
}
