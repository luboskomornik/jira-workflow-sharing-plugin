package com.atlassian.jira.plugin.workflow.sharing.util;


import com.atlassian.jira.plugin.workflow.sharing.ArtifactVersion;
import com.atlassian.jira.plugin.workflow.sharing.exception.VersionParseException;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.transformer.AbstractStringTransformedDownloadableResource;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformer;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;

import org.dom4j.Element;

/**
 * @since version
 */
public class JSWebTransformer implements WebResourceTransformer
{
    private final ApplicationProperties applicationProperties;
    private final I18nResolver i18n;

    public JSWebTransformer(ApplicationProperties applicationProperties, I18nResolver i18n)
    {
        this.applicationProperties = applicationProperties;
        this.i18n = i18n;
    }

    @Override
    public DownloadableResource transform(Element configElement, ResourceLocation location, String filePath, DownloadableResource nextResource)
    {
        return new JsTransformedResource(nextResource,applicationProperties, i18n);
    }

    private static class JsTransformedResource extends AbstractStringTransformedDownloadableResource
    {
        private static final String VAR_CONTEXTPATH = "$contextPath";
        private static final String VAR_FULLVERSION = "$jiraVersion";
        private static final String VAR_MAJORVERSION = "$jiraMajorVersion";
        private static final String VAR_MINORVERSION = "$jiraMinorVersion";
        private static final String VAR_MICROVERSION = "$jiraMicroVersion";

        private final ApplicationProperties applicationProperties;
        private final I18nResolver i18n;

        public JsTransformedResource(DownloadableResource originalResource, ApplicationProperties applicationProperties, I18nResolver i18n)
        {
            super(originalResource);
            this.applicationProperties = applicationProperties;
            this.i18n = i18n;
        }

        @Override
        protected String transform(String originalContent)
        {
            String fullVersion = applicationProperties.getVersion();
            String major = "";
            String minor = "";
            String micro = "";
            try
            {
                ArtifactVersion jiraVersion = new ArtifactVersion(applicationProperties.getVersion(), i18n);
                major = Integer.toString(jiraVersion.getMajorVersion());
                minor = Integer.toString(jiraVersion.getMinorVersion());
                micro = Integer.toString(jiraVersion.getMicroVersion());
                
            }
            catch (VersionParseException e)
            {
                //do nothing
            }
            return originalContent
                    .replace(VAR_CONTEXTPATH, applicationProperties.getBaseUrl())
                    .replace(VAR_FULLVERSION, fullVersion)
                    .replace(VAR_MAJORVERSION,major)
                    .replace(VAR_MINORVERSION,minor)
                    .replace(VAR_MICROVERSION,micro);
        }
    }
}
