package com.atlassian.jira.plugin.workflow.sharing.exporter.servlet;

import com.atlassian.jira.plugin.workflow.sharing.exporter.component.JiraWorkflowSharingExporter;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URI;

/**
 * @since version
 */
public class SharedWorkflowDownloadServlet extends HttpServlet {
    private final JiraWorkflowSharingExporter jiraWorkflowSharingExporter;
    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;

    public SharedWorkflowDownloadServlet(JiraWorkflowSharingExporter jiraWorkflowSharingExporter, UserManager userManager, LoginUriProvider loginUriProvider) {
        this.jiraWorkflowSharingExporter = jiraWorkflowSharingExporter;
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = userManager.getRemoteUsername(req);
        if (username == null || (!userManager.isAdmin(username) && !userManager.isSystemAdmin(username))) {
            redirectToLogin(req, resp);
            return;
        }

        InputStream is = null;
        OutputStream sos = null;

        try {
            String filename = req.getParameter("file");//NON-NLS

            File zipFile = jiraWorkflowSharingExporter.getExportedWorkflowFile(filename);
            is = new FileInputStream(zipFile);

            resp.setContentType("application/zip");//NON-NLS
            resp.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");//NON-NLS

            sos = resp.getOutputStream();
            IOUtils.copy(is, sos);

            sos.flush();
        } finally {
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(sos);
        }
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
