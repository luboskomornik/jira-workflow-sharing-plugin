package com.atlassian.jira.plugin.workflow.sharing.exception;

/**
 * @since version
 */
public class WorkflowSharingImportException extends Exception
{
    public WorkflowSharingImportException()
    {
    }

    public WorkflowSharingImportException(String message)
    {
        super(message);
    }

    public WorkflowSharingImportException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public WorkflowSharingImportException(Throwable cause)
    {
        super(cause);
    }
}
