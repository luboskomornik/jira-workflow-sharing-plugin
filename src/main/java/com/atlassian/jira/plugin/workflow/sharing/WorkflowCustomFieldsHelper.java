package com.atlassian.jira.plugin.workflow.sharing;

import java.io.IOException;
import java.util.regex.Pattern;

import com.atlassian.jira.workflow.JiraWorkflow;

/**
 * @since version
 */
public interface WorkflowCustomFieldsHelper
{
    public static final String CUSTOM_FIELD_PREFIX = "customfield_";
    public static final Pattern CUSTOM_FIELD_PATTERN = Pattern.compile("(customfield\\_[0-9]*)");
    public static final Pattern CUSTOM_FIELD_XML_PATTERN = Pattern.compile(">\\s*(customfield\\_[0-9]*)\\s*<");

    String getCustomFieldTypeClassname(String customFieldId);

    String getCustomFieldsJson(JiraWorkflow workflow, String... additionalFields) throws IOException;
}
