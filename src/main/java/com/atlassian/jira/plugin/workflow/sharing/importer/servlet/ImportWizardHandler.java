package com.atlassian.jira.plugin.workflow.sharing.importer.servlet;

import java.io.*;
import java.util.*;
import java.util.zip.ZipInputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.atlassian.gzipfilter.org.apache.commons.lang.WordUtils;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugin.workflow.sharing.WorkflowSharingFiles;
import com.atlassian.jira.plugin.workflow.sharing.importer.*;
import com.atlassian.jira.plugin.workflow.sharing.importer.component.PacFileDownloader;
import com.atlassian.jira.plugin.workflow.sharing.importer.component.PluginInstallInfoFactory;
import com.atlassian.jira.plugin.workflow.sharing.importer.component.WorkflowImporterFactory;
import com.atlassian.jira.plugin.workflow.sharing.importer.component.WorkflowStatusHelper;
import com.atlassian.jira.plugin.workflow.sharing.servlet.AbstractServletHandler;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

/**
 * @since version
 */
public final class ImportWizardHandler extends AbstractServletHandler
{
    private enum SessionVar
    {
        WFSHARE_IMPORT_PLAN, WFSHARE_WORKFLOW_NAME, WFSHARE_STATUS_HOLDERS, WFSHARE_ORIGINAL_URL, WFSHARE_REPORT_CANCEL_URL, WFSHARE_STATUS_MAPPING
    }

    public static final String IMPORT_FILE_FIELD_NAME = "wfShareImportFile";
    public static final String WF_NAME_FIELD_NAME = "wfShareWorkflowName";
    public static final String BUNDLE_DOWNLOAD_URL_FIELD_NAME = "wfShareBundleDownloadUrl";
    public static final String BUNDLE_FILENAME_FIELD_NAME = "wfShareBundleFilename";
    public static final String ALLOW_TRACKING_FIELD_NAME = "wfShareAllowTracking";
    public static final String PARAM_STATUS_FOR_PREFIX = "status-for-";
    public static final String INSTALL_PLUGIN_PARAM = "installPlugin";
    public static final String REPORT_PARAM = "report";

    private final TemplateRenderer renderer;
    private final WorkflowImporterFactory importerFactory;
    private final WorkflowManager workflowManager;
    private final I18nResolver i18n;
    private final WorkflowStatusHelper workflowStatusHelper;
    private final PluginInstallInfoFactory pluginInstallInfoFactory;
    private final VelocityRequestContextFactory requestContextFactory;
    private final PacFileDownloader pacFileDownloader;

    protected ImportWizardHandler(LoginUriProvider loginUriProvider, WebSudoManager webSudoManager, UserManager userManager, TemplateRenderer renderer, WorkflowImporterFactory importerFactory, WorkflowManager workflowManager, I18nResolver i18n, WorkflowStatusHelper workflowStatusHelper, PluginInstallInfoFactory pluginInstallInfoFactory, VelocityRequestContextFactory requestContextFactory, PacFileDownloader pacFileDownloader)
    {
        super(loginUriProvider, webSudoManager, userManager);
        this.renderer = renderer;
        this.importerFactory = importerFactory;
        this.workflowManager = workflowManager;
        this.i18n = i18n;
        this.workflowStatusHelper = workflowStatusHelper;
        this.pluginInstallInfoFactory = pluginInstallInfoFactory;
        this.requestContextFactory = requestContextFactory;
        this.pacFileDownloader = pacFileDownloader;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        if (enforceAdminLogin(request, response))
        {
            return;
        }

        response.setContentType("text/html;charset=utf-8");//NON-NLS

        //make sure login redirect info is cleared
        super.clearSessionAttributes(request.getSession());

        final Map<String, Object> context = new HashMap<String, Object>();

        context.put("requestContext", requestContextFactory.getJiraVelocityRequestContext());//NON-NLS

        ImportServletMapping requestMapping = ImportServletMapping.fromPath(getMappingPath(request));

        //if we're not coming from our own path, start a new session/import
        String referer = request.getHeader("referer");//NON-NLS
        if (!StringUtils.contains(referer, "/plugins/servlet/wfshare-import"))//NON-NLS
        {

            clearSessionAttributes(request.getSession());
            request.getSession().setAttribute(SessionVar.WFSHARE_ORIGINAL_URL.name(), referer);
            requestMapping = ImportServletMapping.IMPORT_CHOOSE_ZIP;
        }

        try
        {
            String originalUrl = getSessionAttributeOrNull(request, SessionVar.WFSHARE_ORIGINAL_URL.name());
            if (null != originalUrl)
            {
                context.put("cancelUrl", originalUrl);//NON-NLS
            }
            else
            {
                context.put("cancelUrl", "");//NON-NLS
            }

            switch (requestMapping)
            {
                case IMPORT_CHOOSE_ZIP:
                    showChooseZip(request, response, context, requestMapping);
                    break;
                case IMPORT_SET_NAME:
                    if(!ServletFileUpload.isMultipartContent(request) && null != request.getParameter(BUNDLE_DOWNLOAD_URL_FIELD_NAME))
                    {
                        doDownloadMarketplaceZip(request,response,context,requestMapping);
                    }
                    else
                    {
                        doUploadZip(request, response, context, requestMapping);
                    }
                    break;
                case IMPORT_MAP_STATUSES:
                    doSetName(request, response, context, requestMapping);
                    break;
                case IMPORT_VERIFY_PLUGINS:
                    doMapStauses(request, response, context, requestMapping);
                    break;
                case IMPORT_VIEW_SUMMARY:
                    doVerifyPlugins(request, response, context, requestMapping);
                    break;
                case IMPORT_WORKFLOW:
                    doImport(request, response, context, requestMapping);
                    break;
                case IMPORT_VIEW_REPORT:
                    handleViewReport(request, response, context, requestMapping);
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            String error = (e.getMessage() != null) ? e.getMessage() : e.getClass().getName();
            String message = i18n.getText("wfshare.exception.generic.error", error);

            String errorTemplate = ImportServletMapping.ERROR.getResultTemplate();
            context.put("errorMessage", message);//NON-NLS

            renderer.render(errorTemplate, context, response.getWriter());
        }

    }

    private void showChooseZip(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ImportServletMapping servletMapping) throws Exception
    {
        String originalUrl = getSessionAttributeOrNull(request, SessionVar.WFSHARE_ORIGINAL_URL.name());
        clearSessionAttributes(request.getSession());
        
        if (null != originalUrl)
        {
            request.getSession().setAttribute(SessionVar.WFSHARE_ORIGINAL_URL.name(),originalUrl);
        }
        
        setNavigationPaths(context, servletMapping);
        context.put("isSysAdmin",isSystemAdmin());//NON-NLS
        renderer.render(servletMapping.getResultTemplate(), context, response.getWriter());
    }

    private void doDownloadMarketplaceZip(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ImportServletMapping servletMapping) throws Exception
    {
        JiraWorkflowSharingImporter importer = importerFactory.newImporter();
        
        //check to see if we're handling a "back" click
        SharedWorkflowImportPlan plan = getSessionAttributeOrNull(request, SessionVar.WFSHARE_IMPORT_PLAN.name());
        if(null == plan)
        {
            String downloadUrl = request.getParameter(BUNDLE_DOWNLOAD_URL_FIELD_NAME);
            String dirtyZipFilename = request.getParameter(BUNDLE_FILENAME_FIELD_NAME);
            String zipFilename = dirtyZipFilename.replaceAll("[:\\/*?|<> _]","-");
            

            File zipFile = new File(importer.getBaseCacheFolder(), zipFilename);
            pacFileDownloader.downloadFile(downloadUrl,zipFile);
            
            plan = extractPlanFromFile(zipFile,importer);
        }

        request.getSession().setAttribute(SessionVar.WFSHARE_IMPORT_PLAN.name(), plan);
        showChooseName(request, response, context, plan);
    }

    private void doUploadZip(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ImportServletMapping servletMapping) throws Exception
    {
        JiraWorkflowSharingImporter importer = importerFactory.newImporter();

        //check to see if we're handling a "back" click
        SharedWorkflowImportPlan plan = getSessionAttributeOrNull(request, SessionVar.WFSHARE_IMPORT_PLAN.name());

        if (null == plan)
        {
            if (ServletFileUpload.isMultipartContent(request))
            {
                ServletFileUpload upload = new ServletFileUpload();
                FileItemIterator iter = upload.getItemIterator(request);

                while (iter.hasNext())
                {
                    FileItemStream item = iter.next();
                    String fieldName = item.getFieldName();
                    String fileName = item.getName();
                    String contentType = item.getContentType();

                    if (!IMPORT_FILE_FIELD_NAME.equals(fieldName))
                    {
                        continue;
                    }

                    if (!item.isFormField())
                    {
                        FileOutputStream fos = null;
                        InputStream is = null;
                        File zipFile = new File(importer.getBaseCacheFolder(), fileName);
                        try
                        {
                            fos = new FileOutputStream(zipFile);
                            is = item.openStream();
                            IOUtils.copy(is, fos);
                            
                            plan = extractPlanFromFile(zipFile,importer);
                        }
                        finally
                        {
                            IOUtils.closeQuietly(fos);
                            IOUtils.closeQuietly(is);
                        }
                    }
                    else
                    {
                        throw new FileUploadException(i18n.getText("wfshare.exception.uploading.by.field.not.supported"));
                    }
                }
            }
            else
            {
                throw new FileUploadException(i18n.getText("wfshare.exception.not.a.multipart.form.request"));
            }
        }

        request.getSession().setAttribute(SessionVar.WFSHARE_IMPORT_PLAN.name(), plan);
        showChooseName(request, response, context, plan);
    }

    private SharedWorkflowImportPlan extractPlanFromFile(File zipFile,JiraWorkflowSharingImporter importer) throws Exception
    {
        FileInputStream fiz = null;
        ZipInputStream zis = null;
        try
        {
            fiz = new FileInputStream(zipFile);
            zis = new ZipInputStream(fiz);
            if(null == zis.getNextEntry())
            {
                FileUtils.deleteQuietly(zipFile);
                throw new Exception(i18n.getText("wfshare.exception.file.must.be.a.zip.file"));
            }
        }
        finally
        {
            IOUtils.closeQuietly(zis);
            IOUtils.closeQuietly(fiz);
        }

        SharedWorkflowImportPlan plan;
        try
        {
            File unzipFolder = importer.unzipToTempFolder(zipFile);
    
            File workflowXml = new File(unzipFolder, WorkflowSharingFiles.WORKFLOW.getPath());
            File layoutJson = new File(unzipFolder, WorkflowSharingFiles.LAYOUT.getPath());
            File annotationJson = new File(unzipFolder, WorkflowSharingFiles.ANNOTATION.getPath());
            File pluginsJson = new File(unzipFolder, WorkflowSharingFiles.PLUGINS.getPath());
            File customfieldsJson = new File(unzipFolder, WorkflowSharingFiles.CUSTOM_FIELDS.getPath());
            File screensJson = new File(unzipFolder, WorkflowSharingFiles.SCREENS.getPath());
            File notes = new File(unzipFolder, WorkflowSharingFiles.NOTES.getPath());
            File libFolder = new File(unzipFolder, WorkflowSharingFiles.LIB.getPath());
    
            if (!workflowXml.exists())
            {
                throw new Exception(i18n.getText("wfshare.exception.workflow.xml.not.found.in.zip"));
            }
    
            plan = new SharedWorkflowImportPlan(unzipFolder, zipFile.getName());
            plan.setWorkflowXml(workflowXml);
            plan.setLayoutJson(layoutJson);
            plan.setAnnotationsJson(annotationJson);
            plan.setPluginsJson(pluginsJson);
            plan.setCustomfieldsJson(customfieldsJson);
            plan.setScreensJson(screensJson);
            plan.setNotes(notes);
            plan.setLibFolder(libFolder);
        }
        finally 
        {
            FileUtils.deleteQuietly(zipFile);
        }

        return plan;
    }

    private void showChooseName(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, SharedWorkflowImportPlan plan) throws Exception
    {
        //check for plugins and show error if not sysadmin
        if (plan.getPluginsJson().exists())
        {
            List<PluginInstallInfo> pluginInstallInfo = pluginInstallInfoFactory.getPluginInstallationInfo(plan.getPluginsJson());
            if(null != pluginInstallInfo && !pluginInstallInfo.isEmpty())
            {
                Collection<PluginInstallInfo> installablePlugins = Collections2.filter(pluginInstallInfo, new Predicate<PluginInstallInfo>()
                {
                    @Override
                    public boolean apply(PluginInstallInfo pluginInstallInfo)
                    {
                        return (pluginInstallInfo.getInstallable() && !pluginInstallInfo.getAlreadyInstalled());
                    }
                });
                
                if (!installablePlugins.isEmpty() && !isSystemAdmin())
                {
                    clearSessionAttributes(request.getSession());
                    renderer.render(ImportServletMapping.REQUIRES_SYSADMIN.getResultTemplate(), context, response.getWriter());
                    return;
                }
            }
        }
        
        String suggestedName = getSessionAttributeOrNull(request, SessionVar.WFSHARE_WORKFLOW_NAME.name());

        if (null == suggestedName)
        {
            suggestedName = createWorkflowNameFromFilename(plan.getZipFilename());
        }

        plan.setWorkflowName(suggestedName);

        String notesHtml = plan.getNotesAsHtml();
        if (StringUtils.isNotBlank(notesHtml))
        {
            context.put("notesHtml", notesHtml);//NON-NLS
        }

        context.put("suggestedWorkflowName", suggestedName);//NON-NLS
        context.put("allWorkflowNames", getAllWorkflowNames());//NON-NLS

        setNavigationPaths(context, ImportServletMapping.IMPORT_SET_NAME);
        renderer.render(ImportServletMapping.IMPORT_SET_NAME.getResultTemplate(), context, response.getWriter());
    }

    private void doSetName(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ImportServletMapping servletMapping) throws Exception
    {
        SharedWorkflowImportPlan plan = getSessionAttribute(request, SessionVar.WFSHARE_IMPORT_PLAN.name(), i18n.getText("wfshare.exception.import.plan.not.found.in.session"));

        String sessionName = getSessionAttributeOrNull(request, SessionVar.WFSHARE_WORKFLOW_NAME.name());
        String chosenName = request.getParameter(WF_NAME_FIELD_NAME);
        
        if (null == chosenName && null != sessionName)
        {
            chosenName = sessionName;
        }

        if (StringUtils.isNotBlank(chosenName))
        {
            plan.setWorkflowName(chosenName);
        }

        request.getSession().setAttribute(SessionVar.WFSHARE_WORKFLOW_NAME.name(), chosenName);

        showMapStauses(request, response, context, plan);
    }

    private void showMapStauses(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, SharedWorkflowImportPlan plan) throws Exception
    {
        Map<String, StatusMapping> statusHolders = getSessionAttributeOrNull(request, SessionVar.WFSHARE_STATUS_HOLDERS.name());
        if (null == statusHolders)
        {
            statusHolders = workflowStatusHelper.getStatusHolders(plan.getWorkflowXml());
            request.getSession().setAttribute(SessionVar.WFSHARE_STATUS_HOLDERS.name(), statusHolders);
        }

        context.put("jiraStatuses", getJiraStatuses());//NON-NLS
        context.put("statusHolders", statusHolders);//NON-NLS

        setNavigationPaths(context, ImportServletMapping.IMPORT_MAP_STATUSES);

        renderer.render(ImportServletMapping.IMPORT_MAP_STATUSES.getResultTemplate(), context, response.getWriter());
    }

    private void doMapStauses(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ImportServletMapping servletMapping) throws Exception
    {
        SharedWorkflowImportPlan plan = getSessionAttribute(request, SessionVar.WFSHARE_IMPORT_PLAN.name(), i18n.getText("wfshare.exception.import.plan.not.found.in.session"));
        Map<String, StatusMapping> statusHolders = getSessionAttribute(request, SessionVar.WFSHARE_STATUS_HOLDERS.name(), i18n.getText("wfshare.exception.status.holders.not.found.in.session"));

        Map<String,String[]> statusMap = getStatusParams(request);

        List<StatusMapping> statusMappings = getSessionAttributeOrNull(request,SessionVar.WFSHARE_STATUS_MAPPING.name());
        if(null != statusMap && !statusMap.isEmpty())
        {
            statusMappings = createStatusMappings(statusHolders, statusMap);
            request.getSession().setAttribute(SessionVar.WFSHARE_STATUS_MAPPING.name(),statusMappings);
        }

        plan.setStatusMappings(statusMappings);
        showVerifyPlugins(request, response, context, plan);
    }

    private void showVerifyPlugins(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, SharedWorkflowImportPlan plan) throws Exception
    {
        File pluginsJson = plan.getPluginsJson();
        if (!pluginsJson.exists())
        {
            showSummary(request, response, context, plan);
            return;
        }

        List<PluginInstallInfo> pluginInstallInfo = pluginInstallInfoFactory.getPluginInstallationInfo(pluginsJson);

        plan.setPluginInstallInfo(pluginInstallInfo);
        context.put("pluginInstallInfo", pluginInstallInfo);//NON-NLS

        setNavigationPaths(context, ImportServletMapping.IMPORT_VERIFY_PLUGINS);
        renderer.render(ImportServletMapping.IMPORT_VERIFY_PLUGINS.getResultTemplate(), context, response.getWriter());
    }

    private void doVerifyPlugins(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ImportServletMapping servletMapping) throws Exception
    {
        SharedWorkflowImportPlan plan = getSessionAttribute(request, SessionVar.WFSHARE_IMPORT_PLAN.name(), i18n.getText("wfshare.exception.import.plan.not.found.in.session"));
        List<PluginInstallInfo> pluginInstallInfo = plan.getPluginInstallInfo();

        String[] pluginKeysParam = request.getParameterValues(INSTALL_PLUGIN_PARAM);

        final List<String> installablePluginKeys;

        if (null != pluginKeysParam)
        {
            installablePluginKeys = Arrays.asList(pluginKeysParam);
        }
        else
        {
            installablePluginKeys = Collections.<String>emptyList();
        }

        if (!installablePluginKeys.isEmpty() && !isSystemAdmin())
        {
            Collection<PluginInstallInfo> notAlreadyInstalled = Collections2.filter(pluginInstallInfo, new Predicate<PluginInstallInfo>()
            {
                @Override
                public boolean apply(PluginInstallInfo pluginInstallInfo)
                {
                    return (pluginInstallInfo.getInstallable() && !pluginInstallInfo.getAlreadyInstalled());
                }
            });
            
            if(!notAlreadyInstalled.isEmpty())
            {
                clearSessionAttributes(request.getSession());
                renderer.render(ImportServletMapping.REQUIRES_SYSADMIN.getResultTemplate(), context, response.getWriter());
                return;
            }
        }

        if ((installablePluginKeys.isEmpty() || installablePluginKeys.contains(null)) && null != pluginInstallInfo)
        {
            markUninstallableAndRemoveExtensions(pluginInstallInfo);
        }
        else if (null != pluginInstallInfo)
        {
            Collection<PluginInstallInfo> uninstallable = Collections2.filter(pluginInstallInfo, new Predicate<PluginInstallInfo>()
            {
                @Override
                public boolean apply(PluginInstallInfo pluginInstallInfo)
                {
                    return !installablePluginKeys.contains((pluginInstallInfo.getPluginInfo().getKey()));
                }
            });

            markUninstallableAndRemoveExtensions(uninstallable);
        }

        showSummary(request, response, context, plan);
    }

    private void showSummary(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, SharedWorkflowImportPlan plan) throws IOException
    {
        context.put("importPlan", plan);//NON-NLS
        setNavigationPaths(context, ImportServletMapping.IMPORT_VIEW_SUMMARY);

        renderer.render(ImportServletMapping.IMPORT_VIEW_SUMMARY.getResultTemplate(), context, response.getWriter());
    }

    private void doImport(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ImportServletMapping servletMapping) throws Exception
    {
        boolean allowTracking = Boolean.parseBoolean(request.getParameter(ALLOW_TRACKING_FIELD_NAME));

        JiraWorkflowSharingImporter importer = importerFactory.newImporter();

        SharedWorkflowImportPlan plan = getSessionAttribute(request, SessionVar.WFSHARE_IMPORT_PLAN.name(), i18n.getText("wfshare.exception.import.plan.not.found.in.session"));
        WorkflowImportResult result = importer.importWorkflow(plan, allowTracking);
        WorkflowImportReport report = result.getReport();
        try
        {
            String reportFilename = writeReport(report, importer);
            context.put("reportName", reportFilename);//NON-NLS
        }
        catch (IOException e)
        {
            //ignore
        }

        String originalUrl = getSessionAttributeOrNull(request, SessionVar.WFSHARE_ORIGINAL_URL.name());
        clearSessionAttributes(request.getSession());
        
        request.getSession().setAttribute(SessionVar.WFSHARE_REPORT_CANCEL_URL.name(),originalUrl);
        
        showSuccess(request, response, context, plan);
    }

    private void showSuccess(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, SharedWorkflowImportPlan plan) throws IOException
    {
        setNavigationPaths(context, ImportServletMapping.IMPORT_WORKFLOW);
        renderer.render(ImportServletMapping.IMPORT_WORKFLOW.getResultTemplate(), context, response.getWriter());
    }

    private void handleViewReport(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, ImportServletMapping servletMapping) throws Exception
    {
        JiraWorkflowSharingImporter importer = importerFactory.newImporter();

        String reportName = request.getParameter(REPORT_PARAM);

        if (StringUtils.isNotBlank(reportName))
        {
            try
            {
                String reportHtml = FileUtils.readFileToString(new File(importer.getBaseCacheFolder(), reportName));
                context.put("reportHtml", reportHtml);//NON-NLS
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        String originalUrl = getSessionAttributeOrNull(request, SessionVar.WFSHARE_REPORT_CANCEL_URL.name());
        if (null != originalUrl)
        {
            context.put("cancelUrl", originalUrl);//NON-NLS
        }
        else
        {
            context.put("cancelUrl", "");//NON-NLS
        }
        
        clearSessionAttributes(request.getSession());
        setNavigationPaths(context, servletMapping);
        renderer.render(servletMapping.getResultTemplate(), context, response.getWriter());

    }

    private List<StatusMapping> createStatusMappings(Map<String, StatusMapping> statusHolders, Map<String, String[]> statusParams) throws Exception
    {
        List<StatusMapping> mappings = new ArrayList<StatusMapping>();

        if (!statusParams.isEmpty())
        {
            for (Map.Entry<String, String[]> entry : statusParams.entrySet())
            {
                String oldId = StringUtils.substringAfterLast(entry.getKey(), "-");

                if (!statusHolders.containsKey(oldId))
                {
                    throw new Exception(i18n.getText("wfshare.exception.unknown.status.id", oldId));
                }

                //if we're creating a new status, use the new name, else use the original name for summary display
                String newName = (entry.getValue()[0].equals("-1")) ? statusHolders.get(oldId).getOriginalName() : workflowStatusHelper.getNameForStatusId(entry.getValue()[0]);

                StatusMapping newMapping = new StatusMapping(oldId, statusHolders.get(oldId).getOriginalName(), entry.getValue()[0], newName);
                mappings.add(newMapping);
                statusHolders.put(oldId, newMapping);
            }
        }

        return mappings;
    }

    private Map<String,String[]> getStatusParams(HttpServletRequest request)
    {
        Map<String, String[]> idMap = ImmutableMap.copyOf(Maps
                .filterKeys(request.getParameterMap(), new Predicate<String>()
                {
                    @Override
                    public boolean apply(String key)
                    {
                        return key.startsWith(PARAM_STATUS_FOR_PREFIX);
                    }
                }));
        
        return idMap;
    }
    
    private void markUninstallableAndRemoveExtensions(Collection<PluginInstallInfo> installInfo)
    {
        for (PluginInstallInfo info : installInfo)
        {
            info.notInstallable();
            
            if(!info.getAlreadyInstalled())
            {
                info.removeExtensions();
            }
        }
    }

    private String createWorkflowNameFromFilename(String zipFile)
    {
        String workflowName = WordUtils.capitalizeFully(FilenameUtils.removeExtension(zipFile).replaceAll("[_:\\\\/*?|<>-]", " "));
        String suggestedName = workflowName;

        if (workflowManager.workflowExists(workflowName))
        {
            int x = 2;
            while (workflowManager.workflowExists(suggestedName))
            {
                suggestedName = i18n.getText("wfshare.import.prefix", String.valueOf(x), workflowName);
                x++;
            }
        }

        return suggestedName;
    }

    private List<String> getAllWorkflowNames()
    {
        return ImmutableList.copyOf(Collections2.transform(workflowManager
                .getWorkflows(), new Function<JiraWorkflow, String>()
        {
            @Override
            public String apply(JiraWorkflow wf)
            {
                return wf.getName();
            }
        }));
    }

    public List<Status> getJiraStatuses()
    {
        return workflowStatusHelper.getJiraStatuses();
    }

    private String writeReport(WorkflowImportReport report, JiraWorkflowSharingImporter importer) throws IOException
    {
        FileUtils.writeStringToFile(new File(importer.getBaseCacheFolder(), report.getReportName()), report.asHtml());
        FileUtils.writeStringToFile(new File(importer.getBaseCacheFolder(), report.getReportName() + ".md"), report.asMarkdown());
        return report.getReportName();
    }

    private void setNavigationPaths(Map<String, Object> context, ImportServletMapping servletMapping)
    {
        String nextPath = "";
        String backPath = "";
        if (null != servletMapping.next())
        {
            nextPath = "/plugins/servlet/wfshare-import/" + servletMapping.next().getPath();//NON-NLS
        }

        if (null != servletMapping.previous())
        {
            backPath = "/plugins/servlet/wfshare-import/" + servletMapping.previous().getPath();//NON-NLS
        }

        context.put("nextUrl", requestContextFactory.getJiraVelocityRequestContext().getBaseUrl() + nextPath);//NON-NLS
        context.put("backUrl", requestContextFactory.getJiraVelocityRequestContext().getBaseUrl() + backPath);//NON-NLS
    }

    @Override
    protected void clearSessionAttributes(HttpSession session)
    {
        super.clearSessionAttributes(session);
        for (SessionVar sessionVar : SessionVar.values())
        {
            session.removeAttribute(sessionVar.name());
        }
    }

}
