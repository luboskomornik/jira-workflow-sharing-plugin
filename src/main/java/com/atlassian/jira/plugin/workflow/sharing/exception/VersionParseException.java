package com.atlassian.jira.plugin.workflow.sharing.exception;

/**
 * @since version
 */
public class VersionParseException extends Exception
{
    public VersionParseException()
    {
    }

    public VersionParseException(String message)
    {
        super(message);
    }

    public VersionParseException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public VersionParseException(Throwable cause)
    {
        super(cause);
    }
}
