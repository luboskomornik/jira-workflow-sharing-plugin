package com.atlassian.jira.plugin.workflow.sharing.model;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @since version
 */
public class ScreenInfo implements Serializable
{
    private static final long serialVersionUID = 5428739985963548119L;
    private Long originalId;
    private String name;
    private String description;
    private List<ScreenTabInfo> tabs;

    @JsonCreator
    public ScreenInfo(@JsonProperty("originalId") Long originalId
            ,@JsonProperty("name") String name
            ,@JsonProperty("description") String description
            ,@JsonProperty("tabs") List<ScreenTabInfo> tabs)
    {
        this.originalId = originalId;
        this.name = name;
        this.description = description;
        this.tabs = tabs;
    }

    public Long getOriginalId()
    {
        return originalId;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public List<ScreenTabInfo> getTabs()
    {
        return tabs;
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(originalId).append(name).toHashCode();
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == this) { return true; }

        if(!(obj instanceof ScreenInfo)) { return false; }

        ScreenInfo info = (ScreenInfo) obj;

        return new EqualsBuilder().append(this.getOriginalId(), info.getOriginalId()).append(this.getName(), info.getName()).isEquals();
    }

    @Override
    public String toString()
    {
        return "ScreenInfo name:" + name;//NON-NLS
    }
}
