package com.atlassian.jira.plugin.workflow.sharing;

import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;

/**
 * @since version
 */
public class ParentWorkflowChecker
{
    private final WorkflowManager workflowManager;

    public ParentWorkflowChecker(WorkflowManager workflowManager)
    {
        this.workflowManager = workflowManager;
    }

    public boolean isParentWorkflowActive(JiraWorkflow workflow)
    {
        //not an draft workflow? Well you don't have a parent so your parent is active for the purposes
        // of this method.
        if(!workflow.isDraftWorkflow())
        {
            return true;
        }
        JiraWorkflow parentWorkflow = workflowManager.getWorkflow(workflow.getName());
        return parentWorkflow != null && parentWorkflow.isActive();
    }
}
