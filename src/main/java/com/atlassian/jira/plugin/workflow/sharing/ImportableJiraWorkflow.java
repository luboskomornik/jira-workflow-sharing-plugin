package com.atlassian.jira.plugin.workflow.sharing;

import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;

import com.opensymphony.workflow.loader.WorkflowDescriptor;

/**
 * @since version
 */
public class ImportableJiraWorkflow extends ConfigurableJiraWorkflow
{

    public ImportableJiraWorkflow(String name, WorkflowDescriptor workflowDescriptor, WorkflowManager workflowManager)
    {
        super(name, workflowDescriptor, workflowManager);
    }

    public ImportableJiraWorkflow(String name, WorkflowManager workflowManager)
    {
        super(name, workflowManager);
    }

    @Override
    public void reset()
    {
        //do nothing, e.g. don't create field screens until we need them!
    }
    
    public void resetFieldScreens()
    {
        super.reset();
    }
}
