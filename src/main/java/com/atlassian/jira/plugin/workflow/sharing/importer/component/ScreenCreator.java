package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import java.util.Map;

import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.plugin.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.ScreenInfo;

/**
 * @since version
 */
public interface ScreenCreator
{

    void removeScreen(Long screenId);

    FieldScreen createScreen(ScreenInfo screenInfo);
    void addScreenTabs(FieldScreen screen, ScreenInfo screenInfo, Map<String, String> createdFieldsMapping);
}
