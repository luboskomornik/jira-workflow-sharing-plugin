package com.atlassian.jira.plugin.workflow.sharing.importer;

import java.io.File;
import java.io.IOException;

import com.atlassian.jira.plugin.workflow.sharing.exception.WorkflowSharingImportException;

/**
 * @since version
 */
public interface JiraWorkflowSharingImporter
{
    File unzipToTempFolder(File workflowZip) throws IOException;
    File getBaseCacheFolder() throws IOException;

    WorkflowImportResult importWorkflow(SharedWorkflowImportPlan plan,boolean allowGoogleTracking) throws WorkflowSharingImportException;
}
