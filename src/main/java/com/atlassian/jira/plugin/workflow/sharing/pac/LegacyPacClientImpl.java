package com.atlassian.jira.plugin.workflow.sharing.pac;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import com.atlassian.jira.plugin.workflow.sharing.ArtifactVersion;
import com.atlassian.jira.plugin.workflow.sharing.exception.VersionParseException;
import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.plugins.domain.model.product.Product;
import com.atlassian.plugins.service.plugin.PluginService;
import com.atlassian.plugins.service.plugin.PluginVersionService;
import com.atlassian.plugins.service.product.ProductService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.MapMaker;
import com.google.common.collect.Ordering;

/**
 * @since version
 */
public class LegacyPacClientImpl implements JWSPacClient
{
    private static final String JIRA_KEY = "jira";
    private final ProductService productService;
    private final PluginService pluginService;
    private final PluginVersionService pluginVersionService;
    private final ApplicationProperties applicationProperties;
    private final I18nResolver i18n;
    
    private Product latestPacAppVersion;
    private final ConcurrentMap<String, List<PluginVersion>> pacPlugins;

    public LegacyPacClientImpl(ProductService productService, PluginService pluginService, PluginVersionService pluginVersionService, ApplicationProperties applicationProperties, I18nResolver i18n)
    {
        this.productService = productService;
        this.pluginService = pluginService;
        this.pluginVersionService = pluginVersionService;
        this.applicationProperties = applicationProperties;
        this.i18n = i18n;
        this.latestPacAppVersion = null;
        this.pacPlugins = new MapMaker().expiration(10, TimeUnit.MINUTES).makeMap();
    }

    @Override
    public List<PluginVersion> findCompatiblePluginVersionsByPluginKey(String pluginKey)
    {
        List<PluginVersion> versions = Collections.<PluginVersion>emptyList();
        if (!pacPlugins.containsKey(pluginKey))
        {
            try
            {
                Product latest = getOurJiraVersionOrLatest();
                if (null != latest)
                {
                    versions = pluginVersionService.findCompatiblePluginVersionsByPluginKey(
                            JIRA_KEY
                            , getOurJiraVersionOrLatest().getBuildNumber()
                            , pluginKey
                            , null
                            , null /* offset */
                            , ImmutableList.of("plugin")//NON-NLS
                    );

                    pacPlugins.putIfAbsent(pluginKey, ImmutableList.copyOf(versions));
                }
            }
            catch (Throwable t)
            {
                //ignore
            }
        }
        else
        {
            versions = pacPlugins.get(pluginKey);
        }

        return versions;
    }

    private List<PluginVersion> findPluginVersionsByPluginKey(String pluginKey)
    {
        List<PluginVersion> versions = Collections.<PluginVersion>emptyList();
        if (!pacPlugins.containsKey(pluginKey))
        {
            try
            {
                Product latest = getOurJiraVersionOrLatest();
                if (null != latest)
                {
                    versions = pluginVersionService.findCompatiblePluginVersionsByPluginKey(
                            JIRA_KEY
                            , getOurJiraVersionOrLatest().getBuildNumber()
                            , pluginKey
                            , null
                            , null /* offset */
                            , ImmutableList.of("plugin")//NON-NLS
                    );

                    pacPlugins.putIfAbsent(pluginKey, ImmutableList.copyOf(versions));
                }
            }
            catch (Throwable t)
            {
                //ignore
            }
        }
        else
        {
            versions = pacPlugins.get(pluginKey);
        }

        return versions;
    }


    @Override
    public boolean pacHasPlugin(String pluginKey)
    {
        return ((CustomPluginVersionService)pluginVersionService).hasPlugin(pluginKey);
    }

    @Override
    public boolean isVersionCompatible(String key, String version)
    {
        boolean isCompatible = false;
        List<PluginVersion> versions = findCompatiblePluginVersionsByPluginKey(key);

        try
        {
            ArtifactVersion localVersion = new ArtifactVersion(version, i18n);
            if (null != versions && !versions.isEmpty())
            {
                for (PluginVersion pluginVersion : versions)
                {
                    ArtifactVersion pacVersion = new ArtifactVersion(pluginVersion.getVersion(), i18n);

                    if (localVersion.compareTo(pacVersion) == 0)
                    { 
                        isCompatible = true;
                        break;
                    }
                }
            }
        }
        catch (VersionParseException e)
        {
            //just ignore since we'll return false
        }

        return isCompatible;
    }

    @Override
    public PluginVersion getLatestCompatibleVersionByKey(String key)
    {
        PluginVersion latest = null;
        List<PluginVersion> versions = findCompatiblePluginVersionsByPluginKey(key);
        if (null != versions && !versions.isEmpty())
        {
            latest =
                    Ordering.from(new Comparator<PluginVersion>()
                    {
                        @Override
                        public int compare(PluginVersion o1, PluginVersion o2)
                        {
                            return o1.getBuildNumber().compareTo(o2.getBuildNumber());
                        }
                    }).reverse().sortedCopy(versions).get(0);
        }

        return latest;
    }

    private Product getOurJiraVersionOrLatest()
    {
        Product product = null;
        if (null == latestPacAppVersion)
        {
            try
            {
                Product latestPac = getLatestProductVersion();
                Product ourProduct = getProductVersionFromPac(applicationProperties.getVersion());
                product = ourProduct;

                ArtifactVersion ourVersion = new ArtifactVersion(ourProduct.getVersionNumber(), i18n);
                ArtifactVersion pacVersion = new ArtifactVersion(latestPac.getVersionNumber(), i18n);

                if (pacVersion.compareTo(ourVersion) < 0)
                {
                    product = latestPac;
                }
            }
            catch (Exception e)
            {
                //ignore
            }

            latestPacAppVersion = product;
        }

        return latestPacAppVersion;
    }

    private Product getProductVersionFromPac(String version)
    {
        Product product = null;
        try
        {
            List<Product> products = productService.getProductVersionsAfterVersion(JIRA_KEY, 0L);
            for (Product pacProduct : products)
            {
                if (version.equalsIgnoreCase(pacProduct.getVersionNumber()))
                {
                    product = pacProduct;
                    break;
                }
            }
        }
        catch (Throwable t)
        {
            //ignore
        }

        return product;
    }

    private Product getLatestProductVersion()
    {
        Product product = null;
        try
        {
            product = productService.getLatestProductVersion(JIRA_KEY);
        }
        catch (Throwable t)
        {
            //ignore
        }

        return product;
    }
}
