package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugin.workflow.sharing.importer.StatusMapping;

import org.ofbiz.core.entity.GenericEntityException;

/**
 * @since version
 */
public interface WorkflowStatusHelper
{
    Map<String,StatusMapping> getStatusHolders(File xmlFile) throws Exception;

    List<Status> getJiraStatuses();

    void removeStatus(String id) throws GenericEntityException;

    Status createStatus(String newName, String desc, String newStatusDefaultIcon) throws GenericEntityException;
    
    String getNameForStatusId(String id);
}
