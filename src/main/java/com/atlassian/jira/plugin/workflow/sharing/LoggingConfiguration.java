package com.atlassian.jira.plugin.workflow.sharing;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;


/**
 * @since version
 */
public class LoggingConfiguration
{
    private static final Logger log = Logger.getLogger(LoggingConfiguration.class);

    public LoggingConfiguration()
    {
        Level level = Level.toLevel("INFO");//NON-NLS
        Logger logger = Logger.getLogger("com.atlassian.jira.plugin.workflow.sharing");//NON-NLS
        logger.setLevel(level);
    }
}
