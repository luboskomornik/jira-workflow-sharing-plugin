package com.atlassian.jira.plugin.workflow.sharing.exporter.servlet;

import com.atlassian.jira.plugin.workflow.sharing.servlet.BaseHandlerServlet;
import com.atlassian.jira.plugin.workflow.sharing.servlet.ServletHandler;

/**
 * @since version
 */
public class WorkflowSharingExportServlet extends BaseHandlerServlet
{

    public WorkflowSharingExportServlet(ExportWizardHandler handler)
    {
        super(handler);
    }
}
