package com.atlassian.jira.plugin.workflow.sharing.importer;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.jira.plugin.workflow.sharing.importer.PluginInstallInfo;
import com.atlassian.jira.plugin.workflow.sharing.importer.StatusMapping;
import com.atlassian.jira.plugin.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.WorkflowExtensionsPluginInfo;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.pegdown.Extensions;
import org.pegdown.PegDownProcessor;

/**
 * @since version
 */
public final class SharedWorkflowImportPlan implements Serializable
{
    private static final long serialVersionUID = -6834060780888044072L;
    private final File workingFolder;
    private final String zipFilename;
    private File workflowXml;
    private File layoutJson;
    private File annotationsJson;
    private File pluginsJson;
    private File customfieldsJson;
    private File screensJson;
    private File notes;
    private File libFolder;
    private List<StatusMapping> statusMappings;
    private List<PluginInstallInfo> pluginInstallInfo;
    private String workflowName;
    private PegDownProcessor pegDown;

    public SharedWorkflowImportPlan(File baseCacheFolder, String zipFilename)
    {
        this.workingFolder = baseCacheFolder;
        this.zipFilename = zipFilename;
        this.pegDown = new PegDownProcessor(Extensions.ALL);
    }

    public File getWorkingFolder()
    {
        return workingFolder;
    }

    public String getZipFilename()
    {
        return zipFilename;
    }

    public File getWorkflowXml()
    {
        return workflowXml;
    }

    public void setWorkflowXml(File workflowXml)
    {
        this.workflowXml = workflowXml;
    }

    public File getLayoutJson()
    {
        return layoutJson;
    }

    public void setLayoutJson(File layoutJson)
    {
        this.layoutJson = layoutJson;
    }

    public File getAnnotationsJson()
    {
        return annotationsJson;
    }

    public void setAnnotationsJson(File annotationsJson)
    {
        this.annotationsJson = annotationsJson;
    }

    public File getPluginsJson()
    {
        return pluginsJson;
    }

    public void setPluginsJson(File pluginsJson)
    {
        this.pluginsJson = pluginsJson;
    }

    public File getLibFolder()
    {
        return libFolder;
    }

    public void setLibFolder(File libFolder)
    {
        this.libFolder = libFolder;
    }

    public List<StatusMapping> getStatusMappings()
    {
        return statusMappings;
    }

    public void setStatusMappings(List<StatusMapping> statusMappings)
    {
        this.statusMappings = statusMappings;
    }

    public List<PluginInstallInfo> getPluginInstallInfo()
    {
        return pluginInstallInfo;
    }

    public void setPluginInstallInfo(List<PluginInstallInfo> pluginInstallInfo)
    {
        this.pluginInstallInfo = pluginInstallInfo;
    }

    public String getWorkflowName()
    {
        return workflowName;
    }

    public void setWorkflowName(String workflowName)
    {
        this.workflowName = workflowName;
    }

    public File getCustomfieldsJson()
    {
        return customfieldsJson;
    }

    public void setCustomfieldsJson(File customfieldsJson)
    {
        this.customfieldsJson = customfieldsJson;
    }

    public File getScreensJson()
    {
        return screensJson;
    }

    public void setScreensJson(File screensJson)
    {
        this.screensJson = screensJson;
    }

    public List<CustomFieldInfo> getCustomFieldInfo() throws IOException
    {
        List<CustomFieldInfo> info = new ArrayList<CustomFieldInfo>();
        if(null != customfieldsJson && customfieldsJson.exists())
        {
            ObjectMapper mapper = new ObjectMapper();
            info = mapper.readValue(customfieldsJson, new TypeReference<List<CustomFieldInfo>>() {});
        }
        
        final Set<String> uniqueIds = new HashSet<String>();
        
        List<CustomFieldInfo> uniqueInfo = new ArrayList<CustomFieldInfo>();
        Iterables.addAll(uniqueInfo,Iterables.filter(info,new Predicate<CustomFieldInfo>() {
            @Override
            public boolean apply(CustomFieldInfo input)
            {
                if(!uniqueIds.contains(input.getOriginalId()))
                {
                    uniqueIds.add(input.getOriginalId());
                    return true;
                }
                return false;
            }
        }));
        
        return uniqueInfo;
    }
    
    public List<ScreenInfo> getScreenInfo() throws IOException
    {
        List<ScreenInfo> info = new ArrayList<ScreenInfo>();
        if(null != screensJson && screensJson.exists())
        {
            ObjectMapper mapper = new ObjectMapper();
            info = mapper.readValue(screensJson, new TypeReference<List<ScreenInfo>>() {});
        }

        final Set<String> uniqueIds = new HashSet<String>();

        List<ScreenInfo> uniqueInfo = new ArrayList<ScreenInfo>();
        Iterables.addAll(uniqueInfo,Iterables.filter(info,new Predicate<ScreenInfo>() {
            @Override
            public boolean apply(ScreenInfo input)
            {
                if(!uniqueIds.contains(input.getOriginalId().toString()))
                {
                    uniqueIds.add(input.getOriginalId().toString());
                    return true;
                }
                return false;
            }
        }));

        return uniqueInfo;
    }

    public File getNotes()
    {
        return notes;
    }

    public void setNotes(File notes)
    {
        this.notes = notes;
    }
    
    public String getNotesAsMarkdown()
    {
        String md = "";
        if(null != notes && notes.exists())
        {
            try
            {
                md = FileUtils.readFileToString(notes);
            }
            catch (IOException e)
            {
                md = "";
            }
        }
        String safeMD = Jsoup.clean(md, Whitelist.relaxed());
        
        return safeMD;
    }

    public String getNotesAsHtml()
    {
        String html = "";
        if(null != notes && notes.exists())
        {
            try
            {
                String md = FileUtils.readFileToString(notes);
                html = pegDown.markdownToHtml(md);
            }
            catch (IOException e)
            {
                html = "";
            }
        }

        String safeHtml = Jsoup.clean(html, Whitelist.relaxed());
        
        return safeHtml;
    }
}
