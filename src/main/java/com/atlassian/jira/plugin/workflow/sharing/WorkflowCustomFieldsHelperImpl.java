package com.atlassian.jira.plugin.workflow.sharing;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.plugin.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.OptionInfo;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.atlassian.plugin.ModuleDescriptor;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.ofbiz.core.entity.GenericValue;

/**
 * @since version
 */
public class WorkflowCustomFieldsHelperImpl implements WorkflowCustomFieldsHelper
{
    private final CustomFieldManager customFieldManager;
    private final OfBizDelegator ofBizDelegator;
    private final ModuleDescriptorLocator moduleDescriptorLocator;

    public WorkflowCustomFieldsHelperImpl(CustomFieldManager customFieldManager, OfBizDelegator ofBizDelegator, ModuleDescriptorLocator moduleDescriptorLocator)
    {
        this.customFieldManager = customFieldManager;
        this.ofBizDelegator = ofBizDelegator;
        this.moduleDescriptorLocator = moduleDescriptorLocator;
    }

    @Override
    public String getCustomFieldsJson(JiraWorkflow workflow, String ... additionalFields) throws IOException
    {
        String workflowXml = WorkflowUtil.convertDescriptorToXML(workflow.getDescriptor());
        Matcher matcher = CUSTOM_FIELD_XML_PATTERN.matcher(workflowXml);
        List<CustomFieldInfo> cfInfoList = new ArrayList<CustomFieldInfo>();
        while (matcher.find())
        {
            final String customFieldId = matcher.group(1);
            if(StringUtils.isNotBlank(customFieldId))
            {
                CustomField field = customFieldManager.getCustomFieldObject(customFieldId);
                if(null != field && Collections2.filter(cfInfoList,new Predicate<CustomFieldInfo>() {
                    @Override
                    public boolean apply(CustomFieldInfo input)
                    {
                        return input.getOriginalId().equals(customFieldId);
                    }
                }).isEmpty())
                {
                    CustomFieldInfo cfInfo = createCustomFieldInfo(field);
                    cfInfoList.add(cfInfo);
                }
            }
        }
        
        for(final String fieldId : additionalFields)
        {
            CustomField additionalField = customFieldManager.getCustomFieldObject(fieldId);
            if(null != additionalField && Collections2.filter(cfInfoList,new Predicate<CustomFieldInfo>() {
                @Override
                public boolean apply(CustomFieldInfo input)
                {
                    return input.getOriginalId().equals(fieldId);
                }
            }).isEmpty())
            {
                CustomFieldInfo cfInfo = createCustomFieldInfo(additionalField);
                cfInfoList.add(cfInfo);
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        final StringWriter sw = new StringWriter();

        mapper.writeValue(sw,cfInfoList);

        return sw.toString();
    }

    private CustomFieldInfo createCustomFieldInfo(CustomField field)
    {
        FieldConfig fieldConfig = field.getConfigurationSchemes().get(0).getOneAndOnlyConfig();

        String fieldId = field.getId();
        String fieldPluginKey = getPluginKeyProvidingField(fieldId);
        String fieldName = field.getName();
        String fieldDesc = field.getDescription();

        CustomFieldType cfType = field.getCustomFieldType();
        
        //using reflection to support both jira 4.4 & 5.0
        //in 4.4 CustomFieldTypeModuleDescriptor is a class but in 5.0 it's an interface

        String ctkey = "";
        try
        {
            Method getDescriptor = cfType.getClass().getMethod("getDescriptor",new Class[0]);
            Object descriptor = getDescriptor.invoke(cfType,null);
            
            Method getCompleteKey = descriptor.getClass().getMethod("getCompleteKey",new Class[0]);
            ctkey = (String)getCompleteKey.invoke(descriptor,null);
        }
        catch (NoSuchMethodException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (InvocationTargetException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        
        //String ctkey = cfType.getDescriptor().getCompleteKey();

        CustomFieldSearcher cfSearcher = field.getCustomFieldSearcher();
        String searcherKey = "";
        if(null != cfSearcher)
        {
            //again with jira 4.4/5.0
            try
            {
                Method getSearchDescriptor = cfSearcher.getClass().getMethod("getDescriptor",new Class[0]);
                Object searchDescriptor = getSearchDescriptor.invoke(cfSearcher,null);

                Method getSearchCompleteKey = searchDescriptor.getClass().getMethod("getCompleteKey",new Class[0]);
                searcherKey = (String)getSearchCompleteKey.invoke(searchDescriptor,null);
            }
            catch (NoSuchMethodException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            catch (InvocationTargetException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            catch (IllegalAccessException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            //searcherKey = cfSearcher.getDescriptor().getCompleteKey();
        }

        Options options = field.getOptions(null,fieldConfig , null);
        List<OptionInfo> optionInfo = getOptionInfo(options);

        String defaultDataType = CustomFieldType.DEFAULT_VALUE_TYPE;
        String defaultKey = fieldConfig.getId().toString();

        Map fields = MapBuilder.build(GenericConfigManager.ENTITY_DATA_TYPE, defaultDataType, GenericConfigManager.ENTITY_DATA_KEY, defaultKey);
        List gvs = ofBizDelegator.findByAnd(GenericConfigManager.ENTITY_TABLE_NAME,fields);

        String defaultXml = "";
        if(gvs != null && !gvs.isEmpty())
        {
            GenericValue gv = (GenericValue) gvs.iterator().next();
            defaultXml = gv.getString(GenericConfigManager.ENTITY_XML_VALUE);
        }

        CustomFieldInfo cfInfo = new CustomFieldInfo(
                fieldId
                ,fieldName
                ,fieldDesc
                ,ctkey
                ,searcherKey
                ,defaultXml
                ,optionInfo
                ,fieldPluginKey
        );
        
        return cfInfo;
    }

    private List<OptionInfo> getOptionInfo(List<Option> options)
    {
        List<OptionInfo> rootOptions = new ArrayList<OptionInfo>();

        if(null != options && !options.isEmpty())
        {
            for(Option option : options)
            {
                List<OptionInfo> childOptions = getOptionInfo(option.getChildOptions());
                OptionInfo optionInfo = new OptionInfo(option.getOptionId().toString(),option.getValue(),option.getSequence().toString(),option.getDisabled(),childOptions);
                rootOptions.add(optionInfo);
            }
        }

        return rootOptions;
    }
    
    private String getPluginKeyProvidingField(String fieldId)
    {
        String pluginKey = "";
        String fieldTypeClass = getCustomFieldTypeClassname(fieldId);
        Collection<ModuleDescriptor> moduleDescriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleClassname(fieldTypeClass);
        if(null != moduleDescriptors && !moduleDescriptors.isEmpty())
        {
            pluginKey = moduleDescriptors.iterator().next().getPluginKey();
        }

        return pluginKey;
    }

    @Override
    public String getCustomFieldTypeClassname(String customFieldId)
    {
        String classname = null;

        CustomField cf = customFieldManager.getCustomFieldObject(customFieldId);
        if(null != cf)
        {
            CustomFieldType cfType = cf.getCustomFieldType();
            classname = cfType.getClass().getName();
        }

        return classname;
    }

}
