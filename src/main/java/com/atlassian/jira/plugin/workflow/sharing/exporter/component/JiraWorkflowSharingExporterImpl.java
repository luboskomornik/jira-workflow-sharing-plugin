package com.atlassian.jira.plugin.workflow.sharing.exporter.component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.workflow.WorkflowService;
import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.jira.plugin.workflow.sharing.*;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.atlassian.plugin.Plugin;
import com.atlassian.sal.api.message.I18nResolver;

import com.sysbliss.jira.plugins.workflow.WorkflowDesignerConstants;
import com.sysbliss.jira.plugins.workflow.util.WorkflowDesignerPropertySet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JiraWorkflowSharingExporterImpl implements JiraWorkflowSharingExporter
{
    private static final Logger log = LoggerFactory.getLogger("atlassian-plugin");//NON-NLS
    public static final String BASE_EXPORT_FOLDER = "workflowexports";
    public static final String CLEAN_FILENAME_PATTERN = "[:\\\\/*?|<> _]";

    private final WorkflowService workflowService;
    private final WorkflowDesignerPropertySet workflowDesignerPropertySet;
    private final JiraHome jiraHome;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final WorkflowExtensionsHelper workflowExtensionsHelper;
    private final WorkflowScreensHelper workflowScreensHelper;
    private final WorkflowCustomFieldsHelper workflowCustomFieldsHelper;
    private final WorkflowSharingGoogleTracker googleTracker;
    private final I18nResolver i18n;

    public JiraWorkflowSharingExporterImpl(WorkflowDesignerPropertySet workflowDesignerPropertySet, WorkflowService workflowService, JiraHome jiraHome, WorkflowExtensionsHelper workflowExtensionsHelper, JiraAuthenticationContext jiraAuthenticationContext, WorkflowScreensHelper workflowScreensHelper, WorkflowCustomFieldsHelper workflowCustomFieldsHelper, WorkflowSharingGoogleTracker googleTracker, I18nResolver i18n)
    {
        this.workflowDesignerPropertySet = workflowDesignerPropertySet;
        this.workflowService = workflowService;
        this.jiraHome = jiraHome;
        this.workflowExtensionsHelper = workflowExtensionsHelper;

        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.workflowScreensHelper = workflowScreensHelper;
        this.workflowCustomFieldsHelper = workflowCustomFieldsHelper;
        this.googleTracker = googleTracker;
        this.i18n = i18n;
    }

    @Override
    public File exportActiveWorkflow(String name, boolean includePluginJars, String notes,boolean allowGoogleTracking) throws Exception
    {
        File zipFile = null;
        JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(jiraAuthenticationContext.getLoggedInUser());
        JiraWorkflow workflow = workflowService.getWorkflow(jiraServiceContext, name);

        if (null != workflow)
        {
            String layoutKey = WorkflowDesignerConstants.LAYOUT_PREFIX.concat(workflow.getName());
            String annotationKey = WorkflowDesignerConstants.ANNOTATION_PREFIX.concat(workflow.getName());
            zipFile = exportWorkflow(workflow, layoutKey, annotationKey, includePluginJars,notes,allowGoogleTracking);
        }

        return zipFile;
    }

    @Override
    public File exportDraftWorflow(String name, boolean includePluginJars, String notes,boolean allowGoogleTracking) throws Exception
    {
        File zipFile = null;
        JiraServiceContext jiraServiceContext = new JiraServiceContextImpl(jiraAuthenticationContext.getLoggedInUser());
        JiraWorkflow workflow = workflowService.getDraftWorkflow(jiraServiceContext, name);

        if (null != workflow)
        {
            String layoutKey = WorkflowDesignerConstants.LAYOUT_DRAFT_PREFIX.concat(workflow.getName());
            String annotationKey = WorkflowDesignerConstants.ANNOTATION_DRAFT_PREFIX.concat(workflow.getName());
            zipFile = exportWorkflow(workflow, layoutKey, annotationKey, includePluginJars,notes,allowGoogleTracking);
        }
        return zipFile;
    }

    public File exportWorkflow(JiraWorkflow workflow, String layoutKey, String annotationKey, boolean includePluginJars, String notes,boolean allowGoogleTracking) throws Exception
    {
        String workflowXml = WorkflowUtil.convertDescriptorToXML(workflow.getDescriptor());
        String layoutJson = workflowDesignerPropertySet.getProperty(layoutKey);
        String annotationJson = workflowDesignerPropertySet.getProperty(annotationKey);

        Map<Plugin, Set<String>> requiredPlugins = workflowExtensionsHelper.getRequiredPlugins(workflow);
        String pluginsJson = workflowExtensionsHelper.requiredPluginsToJson(requiredPlugins);
        String customFieldsJson = workflowCustomFieldsHelper.getCustomFieldsJson(workflow,workflowScreensHelper.getCustomFieldIdsForWorkflowScreens(workflow));
        String screensJson = workflowScreensHelper.getScreensJson(workflow);

        boolean hasPlugins = false;

        ZipOutputStream zipOutputStream = null;
        File zipFile = null;
        try
        {
            zipFile = getZipFile(workflow);
            zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));

            if (StringUtils.isNotBlank(workflowXml))
            {
                addStringToZip(WorkflowSharingFiles.WORKFLOW.getPath(), workflowXml, zipOutputStream);
            }
            else
            {
                throw new IOException(i18n.getText("wfshare.exception.workflow.xml.is.blank"));
            }

            if (StringUtils.isNotBlank(layoutJson))
            {
                addStringToZip(WorkflowSharingFiles.LAYOUT.getPath(), layoutJson, zipOutputStream);
            }

            if (StringUtils.isNotBlank(annotationJson))
            {
                addStringToZip(WorkflowSharingFiles.ANNOTATION.getPath(), annotationJson, zipOutputStream);
            }

            if (StringUtils.isNotBlank(pluginsJson) && !pluginsJson.equals("[]"))
            {
                hasPlugins = true;
                addStringToZip(WorkflowSharingFiles.PLUGINS.getPath(), pluginsJson, zipOutputStream);
            }

            if (hasPlugins && includePluginJars)
            {
                List<File> pluginArtifactFiles = workflowExtensionsHelper.getPluginArtifactFiles(requiredPlugins.keySet());
                addFilesToZip(WorkflowSharingFiles.LIB.getPath(), pluginArtifactFiles, zipOutputStream);
            }

            if(StringUtils.isNotBlank(customFieldsJson)&& !customFieldsJson.equals("[]"))
            {
                addStringToZip(WorkflowSharingFiles.CUSTOM_FIELDS.getPath(), customFieldsJson, zipOutputStream);
            }

            if(StringUtils.isNotBlank(screensJson)&& !screensJson.equals("[]"))
            {
                addStringToZip(WorkflowSharingFiles.SCREENS.getPath(), screensJson, zipOutputStream);
            }
            
            if (StringUtils.isNotBlank(notes))
            {
                addStringToZip(WorkflowSharingFiles.NOTES.getPath(), notes, zipOutputStream);
            }
        }
        finally
        {
            IOUtils.closeQuietly(zipOutputStream);
        }

        if(allowGoogleTracking)
        {
            googleTracker.trackExport(workflow.getName());    
        }
        
        return zipFile;
    }

    private void addFilesToZip(String folderName, List<File> pluginArtifactFiles, ZipOutputStream zipOutputStream) throws IOException
    {
        String entryPrefix = ensurePrefixWithSlash(folderName);
        zipOutputStream.putNextEntry(new ZipEntry(entryPrefix));
        zipOutputStream.closeEntry();

        FileInputStream jarIS = null;
        for (File pluginJar : pluginArtifactFiles)
        {
            try
            {
                zipOutputStream.putNextEntry(new ZipEntry(folderName + pluginJar.getName()));
                jarIS = FileUtils.openInputStream(pluginJar);
                IOUtils.copy(jarIS, zipOutputStream);
                zipOutputStream.closeEntry();
            }
            finally
            {
                IOUtils.closeQuietly(jarIS);
            }
        }
    }

    private static String ensurePrefixWithSlash(String prefix)
    {
        String entryPrefix = prefix;

        if (StringUtils.isNotBlank(entryPrefix) && !entryPrefix.equals("/"))
        {
            // strip leading '/'
            if (entryPrefix.charAt(0) == '/')
            {
                entryPrefix = entryPrefix.substring(1);
            }
            // ensure trailing '/'
            if (entryPrefix.charAt(entryPrefix.length() - 1) != '/')
            {
                entryPrefix = entryPrefix + "/";
            }
        }

        return entryPrefix;
    }

    protected void addStringToZip(String filename, String content, ZipOutputStream zipOutputStream) throws IOException
    {
        zipOutputStream.putNextEntry(new ZipEntry(filename));
        IOUtils.write(content, zipOutputStream);
        zipOutputStream.closeEntry();
    }

    protected File getZipFile(JiraWorkflow workflow) throws IOException {
        String cleanWorkflowName = workflow.getName().replaceAll(CLEAN_FILENAME_PATTERN, "-");
        String fileName;

        if (workflow.isDraftWorkflow())
        {
            fileName = cleanWorkflowName.concat("-draft.jwb");//NON-NLS
        }
        else
        {
            fileName = cleanWorkflowName.concat(".jwb");//NON-NLS
        }

        File baseFolder = new File(jiraHome.getExportDirectory(), BASE_EXPORT_FOLDER);

        FileUtils.forceMkdir(baseFolder);

        return new File(baseFolder, fileName);
    }

    public File getExportedWorkflowFile(String filename)
    {
        File baseFolder = new File(jiraHome.getExportDirectory(), BASE_EXPORT_FOLDER);
        return new File(baseFolder, filename);
    }
}