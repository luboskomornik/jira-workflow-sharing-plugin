package com.atlassian.jira.plugin.workflow.sharing.pac;

import com.atlassian.plugins.client.service.AbstractServiceClient;
import com.atlassian.plugins.client.service.ClientContextFactory;
import com.atlassian.plugins.client.service.plugin.PluginServiceClientImpl;
import com.atlassian.plugins.client.service.plugin.PluginVersionServiceClientImpl;
import com.atlassian.plugins.client.service.product.ProductServiceClientImpl;
import com.atlassian.plugins.service.plugin.PluginService;
import com.atlassian.plugins.service.plugin.PluginVersionService;
import com.atlassian.plugins.service.product.ProductService;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since version
 */
public class PacServiceFactoryImpl implements PacServiceFactory
{
    private static final String UPM_PAC_BASE_URL = System.getProperty("pac.baseurl", "https://plugins.atlassian.com/server");//NON-NLS
    private static final boolean LOG_REQUESTS = Boolean.valueOf(System.getProperty("pac.log.requests"));//NON-NLS
    private final ClientContextFactory clientContextFactory;

    public PacServiceFactoryImpl(ClientContextFactory clientContextFactory)
    {
        this.clientContextFactory = checkNotNull(clientContextFactory, "clientContextFactory");//NON-NLS
    }

    @Override
    public PluginService getPluginService()
    {
        return configure(new PluginServiceClientImpl());
    }

    @Override
    public ProductService getProductService()
    {
        return configure(new ProductServiceClientImpl());
    }

    @Override
    public PluginVersionService getPluginVersionService()
    {
        return configure(new CustomPluginVersionService());
    }

    private <T extends AbstractServiceClient> T configure(T client)
    {
        
        client.setBaseUrl(getPacBaseUrl());
        client.setContextFactory(clientContextFactory);
        client.setLogWebResourceRequests(LOG_REQUESTS);
        return client;
    }
    
    private String getPacBaseUrl()
    {
        return UPM_PAC_BASE_URL;
    }
}
