package com.atlassian.jira.plugin.workflow.sharing.importer.servlet;

import com.atlassian.jira.plugin.workflow.sharing.servlet.BaseHandlerServlet;

/**
 * @since version
 */

public class WorkflowSharingImportServlet extends BaseHandlerServlet
{
    public WorkflowSharingImportServlet(ImportWizardHandler handler)
    {
        super(handler);
    }
}
