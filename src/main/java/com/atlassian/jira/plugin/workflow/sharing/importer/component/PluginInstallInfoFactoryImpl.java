package com.atlassian.jira.plugin.workflow.sharing.importer.component;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.atlassian.jira.plugin.workflow.sharing.ArtifactVersion;
import com.atlassian.jira.plugin.workflow.sharing.exception.VersionParseException;
import com.atlassian.jira.plugin.workflow.sharing.importer.PluginInstallInfo;
import com.atlassian.jira.plugin.workflow.sharing.model.WorkflowExtensionsPluginInfo;
import com.atlassian.jira.plugin.workflow.sharing.pac.JWSPacClient;
import com.atlassian.jira.plugin.workflow.sharing.pac.JWSPacClientFactory;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.sal.api.message.I18nResolver;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import static java.lang.Boolean.getBoolean;

/**
 * @since version
 */
public class PluginInstallInfoFactoryImpl implements PluginInstallInfoFactory
{
    private static final String UPM_PAC_DISABLE = "upm.pac.disable";
    public static final String UPM_ON_DEMAND = "atlassian.upm.on.demand";

    private final PluginAccessor pluginAccessor;
    private final I18nResolver i18n;
    private final JWSPacClientFactory clientFactory;

    public PluginInstallInfoFactoryImpl(PluginAccessor pluginAccessor, I18nResolver i18n, JWSPacClientFactory clientFactory)
    {
        this.pluginAccessor = pluginAccessor;
        this.i18n = i18n;
        this.clientFactory = clientFactory;
    }

    @Override
    public List<PluginInstallInfo> getPluginInstallationInfo(File pluginsJson) throws IOException
    {
        List<PluginInstallInfo> installInfo = Collections.<PluginInstallInfo>emptyList();

        ObjectMapper mapper = new ObjectMapper();
        List<WorkflowExtensionsPluginInfo> infoList = mapper.readValue(pluginsJson, new TypeReference<List<WorkflowExtensionsPluginInfo>>() {});

        if (isOnDemand())
        {
            installInfo = generateOnDemandInstallInfo(infoList);
        }
        else
        {
            installInfo = generateInstallInfo(infoList);
        }

        return installInfo;
    }

    private List<PluginInstallInfo> generateOnDemandInstallInfo(List<WorkflowExtensionsPluginInfo> infoList)
    {
        List<PluginInstallInfo> installInfo = new ArrayList<PluginInstallInfo>();
        String noInstallMessage = i18n.getText("wfshare.install-info-msg.onDemand");
        String alreadyInstalledMessage = i18n.getText("wfshare.already-installed-info-msg.onDemand");

        for (WorkflowExtensionsPluginInfo pluginInfo : infoList)
        {
            Plugin jiraPlugin = pluginAccessor.getPlugin(pluginInfo.getKey());

            if (null == jiraPlugin)
            {
                installInfo.add(
                        PluginInstallInfo.builder(pluginInfo)
                                         .fromLib()
                                         .notInstallable()
                                         .removeExtensions()
                                         .message(noInstallMessage)
                                         .build());
            }
            else
            {
                installInfo.add(
                        PluginInstallInfo.builder(pluginInfo)
                                         .fromLib()
                                         .notInstallable()
                                         .alreadyInstalled(jiraPlugin.getPluginInformation().getVersion())
                                         .message(alreadyInstalledMessage)
                                         .build()
                );
            }

        }

        return installInfo;
    }

    private List<PluginInstallInfo> generateInstallInfo(List<WorkflowExtensionsPluginInfo> infoList)
    {
        if (isPacDisabled())
        {
            return generateWithoutPac(infoList);
        }

        return generateWithPac(infoList);
    }

    private List<PluginInstallInfo> generateWithoutPac(List<WorkflowExtensionsPluginInfo> infoList)
    {
        List<PluginInstallInfo> installInfo = new ArrayList<PluginInstallInfo>();

        for (WorkflowExtensionsPluginInfo pluginInfo : infoList)
        {
            Plugin jiraPlugin = pluginAccessor.getPlugin(pluginInfo.getKey());

            if (null == jiraPlugin)
            {
                installInfo.add(createInstallInfoForNewPluginWithoutPac(pluginInfo));
            }
            else
            {
                installInfo.add(createInstallInfoForExistingPluginWithoutPac(pluginInfo, jiraPlugin));
            }
        }

        return installInfo;
    }

    private List<PluginInstallInfo> generateWithPac(List<WorkflowExtensionsPluginInfo> infoList)
    {
        List<PluginInstallInfo> installInfo = new ArrayList<PluginInstallInfo>();

        JWSPacClient pacClient = clientFactory.getPacClient();

        for (WorkflowExtensionsPluginInfo pluginInfo : infoList)
        {
            Plugin jiraPlugin = pluginAccessor.getPlugin(pluginInfo.getKey());

            if (null == jiraPlugin)
            {
                installInfo.add(createInstallInfoForNewPluginWithPac(pluginInfo, pacClient));
            }
            else
            {
                installInfo.add(createInstallInfoForExistingPluginWithPac(pluginInfo, jiraPlugin, pacClient));
            }
        }

        return installInfo;
    }

    private PluginInstallInfo createInstallInfoForExistingPluginWithoutPac(WorkflowExtensionsPluginInfo pluginInfo, Plugin jiraPlugin)
    {
        PluginInstallInfo installInfo = createInstallInfoIfNewerVersionFound(pluginInfo, jiraPlugin);

        //from here on, the lib version is newer so we'll upgrade if we can
        if (null == installInfo)
        {
            installInfo =
                    PluginInstallInfo.builder(pluginInfo)
                                     .fromLib()
                                     .message(i18n.getText("wfshare.install-info-msg.upgrade", jiraPlugin.getPluginInformation().getVersion()))
                                     .build();
        }

        return installInfo;
    }

    private PluginInstallInfo createInstallInfoForNewPluginWithoutPac(WorkflowExtensionsPluginInfo pluginInfo)
    {
        return PluginInstallInfo.builder(pluginInfo)
                                .fromLib()
                                .message(i18n.getText("wfshare.install-info-msg.pacDisabled"))
                                .build();
    }

    private PluginInstallInfo createInstallInfoForNewPluginWithPac(WorkflowExtensionsPluginInfo pluginInfo, JWSPacClient pacClient)
    {
        PluginInstallInfo installInfo = createInstallInfoIfPrivateOrPacCompatible(pluginInfo, pacClient);

        if (null == installInfo)
        {
            PluginVersion version = pacClient.getLatestCompatibleVersionByKey(pluginInfo.getKey());
            if (null == version)
            {
                //no compatible version found, don't install
                installInfo =
                        PluginInstallInfo.builder(pluginInfo)
                                         .fromLib()
                                         .notInstallable()
                                         .removeExtensions()
                                         .message(i18n.getText("wfshare.install-info-msg.removeNotCompatible"))
                                         .build();
            }
            else
            {
                //install new pac version
                try
                {
                    installInfo =
                            PluginInstallInfo.builder(pluginInfo)
                                             .fromPac(version.getVersion())
                                             .pacDownloadUrl(new URI(version.getBinaryUrl()).toURL())
                                             .message(i18n.getText("wfshare.install-info-msg.foundCompatibleInPac", version.getVersion()))
                                             .build();
                }
                catch (Exception e)
                {
                    //ignore
                }
            }
        }

        return installInfo;
    }

    private PluginInstallInfo createInstallInfoForExistingPluginWithPac(WorkflowExtensionsPluginInfo pluginInfo, Plugin jiraPlugin, JWSPacClient pacClient)
    {
        PluginInstallInfo installInfo = createInstallInfoIfNewerVersionFound(pluginInfo, jiraPlugin);

        //from here on, the lib version is newer so we'll upgrade if we can
        if (null == installInfo)
        {
            installInfo = createInstallInfoIfPrivateOrPacCompatible(pluginInfo, pacClient);

            if (null == installInfo)
            {
                //lib version is not compatible, give a warning and keep existing plugin
                installInfo =
                        PluginInstallInfo.builder(pluginInfo)
                                         .fromLib()
                                         .notInstallable()
                                         .alreadyInstalled(jiraPlugin.getPluginInformation().getVersion())
                                         .message(i18n.getText("wfshare.install-info-msg.notCompatibleKeepExisting", jiraPlugin.getPluginInformation().getVersion()))
                                         .build();
            }
            else if (installInfo.getInstallVersion().equalsIgnoreCase(jiraPlugin.getPluginInformation().getVersion()))
            {
                installInfo.alreadyInstalled(installInfo.getInstallVersion());
            }
        }

        return installInfo;
    }

    private PluginInstallInfo createInstallInfoIfPrivateOrPacCompatible(WorkflowExtensionsPluginInfo pluginInfo, JWSPacClient pacClient)
    {
        PluginInstallInfo installInfo = null;
        if (!pacClient.pacHasPlugin(pluginInfo.getKey()))
        {
            //add info as an unknown "private" plugin
            installInfo =
                    PluginInstallInfo.builder(pluginInfo)
                                     .fromLib()
                                     .message(i18n.getText("wfshare.install-info-msg.notFoundInPac"))
                                     .build();
        }
        else if (pacClient.isVersionCompatible(pluginInfo.getKey(), pluginInfo.getVersion()))
        {
            installInfo =
                    PluginInstallInfo.builder(pluginInfo)
                                     .fromLib()
                                     .build();
        }

        return installInfo;
    }

    private PluginInstallInfo createInstallInfoIfNewerVersionFound(WorkflowExtensionsPluginInfo pluginInfo, Plugin jiraPlugin)
    {
        PluginInstallInfo installInfo = null;
        try
        {
            ArtifactVersion libVersion = new ArtifactVersion(pluginInfo.getVersion(), i18n);
            ArtifactVersion jiraVersion = new ArtifactVersion(jiraPlugin.getPluginInformation().getVersion(), i18n);
            if (libVersion.compareTo(jiraVersion) < 0)
            {
                //don't install, there's already a newer version installed
                installInfo =
                        PluginInstallInfo.builder(pluginInfo)
                                         .fromLib()
                                         .notInstallable()
                                         .alreadyInstalled(jiraPlugin.getPluginInformation().getVersion())
                                         .message(i18n.getText("wfshare.install-info-msg.newVersionAlreadyInstalled", jiraPlugin.getPluginInformation().getVersion()))
                                         .build();
            }
        }
        catch (VersionParseException e)
        {
            e.printStackTrace();
            installInfo =
                    PluginInstallInfo.builder(pluginInfo)
                                     .fromLib()
                                     .notInstallable()
                                     .removeExtensions()
                                     .message(i18n.getText("wfshare.install-info-msg.versionParseError"))
                                     .build();
        }

        return installInfo;
    }

    private boolean isPacDisabled()
    {
        return getBoolean(UPM_PAC_DISABLE);
    }

    private boolean isOnDemand()
    {
        //TODO: make sure this works!
        return getBoolean(UPM_ON_DEMAND);
    }

}
