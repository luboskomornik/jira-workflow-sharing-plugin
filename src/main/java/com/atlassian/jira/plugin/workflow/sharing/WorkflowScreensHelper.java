package com.atlassian.jira.plugin.workflow.sharing;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;

import com.opensymphony.workflow.loader.ActionDescriptor;

import org.codehaus.jackson.JsonGenerationException;

/**
 * @since version
 */
public interface WorkflowScreensHelper
{
    public static final String FIELDSCREEN_VIEW = "fieldscreen";
    public static final String FIELDSCREEN_ID_NAME = "jira.fieldscreen.id";

    String getScreensJson(JiraWorkflow workflow) throws IOException;

    Set<String> getClassnamesForScreen(String screenId);

    Set<String> getClassnamesForScreenFromAction(ActionDescriptor action);

    String[] getCustomFieldIdsForWorkflowScreens(JiraWorkflow workflow);

    void updateWorkflowScreenIds(ConfigurableJiraWorkflow jiraWorkflow, Map<Long, Long> oldToNewIdMapping);
    
}
