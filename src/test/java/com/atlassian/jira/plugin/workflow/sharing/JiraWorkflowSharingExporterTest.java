package com.atlassian.jira.plugin.workflow.sharing;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.jira.plugin.workflow.sharing.model.WorkflowExtensionsPluginInfo;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @since 3.5
 */
public class JiraWorkflowSharingExporterTest
{

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void testSomething() throws IOException
    {

        List<WorkflowExtensionsPluginInfo> plugins = new ArrayList<WorkflowExtensionsPluginInfo>(2);

        WorkflowExtensionsPluginInfo plug1 = new WorkflowExtensionsPluginInfo("Plugin One", "com.atlassian.plugin.plugin-one","desc","1.0","atlassian","http://www.atlassian.com","my-plugin-one.jar");
        WorkflowExtensionsPluginInfo plug2 = new WorkflowExtensionsPluginInfo("Plugin Two", "com.atlassian.plugin.plugin-two","desc","1.0","atlassian","http://www.atlassian.com","my-plugin-two.jar");

        plugins.add(plug1);
        plugins.add(plug2);

        plug1.getClassNames().add("com.example.SomeClass");
        plug1.getClassNames().add("com.example.SomeOtherClass");
        plug2.getClassNames().add("com.example.YetAnotherClass");

        ObjectMapper mapper = new ObjectMapper();
        final StringWriter sw = new StringWriter();

        mapper.writeValue(sw,plugins);

        String json = sw.toString();

        assert true;

    }

}
