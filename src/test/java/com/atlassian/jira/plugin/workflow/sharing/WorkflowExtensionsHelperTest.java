package com.atlassian.jira.plugin.workflow.sharing;

import java.io.IOException;
import java.util.List;

import com.atlassian.jira.bc.workflow.WorkflowService;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;

import org.junit.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since 3.5
 */
public class WorkflowExtensionsHelperTest
{
    private static List<PluginFactory> DEFAULT_PLUGIN_FACTORIES;

    ModuleDescriptorLocator moduleDescriptorLocator;
    JiraAuthenticationContext jiraAuthenticationContext;
    WorkflowService workflowService;
    WorkflowScreensHelper workflowScreensHelper;
    WorkflowCustomFieldsHelper workflowCustomFieldsHelper;
    
    @Before
    public void setup() throws IOException
    {
        moduleDescriptorLocator = mock(ModuleDescriptorLocator.class);
        jiraAuthenticationContext = mock(JiraAuthenticationContext.class);
        workflowService = mock(WorkflowService.class);
        workflowScreensHelper = mock(WorkflowScreensHelper.class);
        workflowCustomFieldsHelper = mock(WorkflowCustomFieldsHelper.class);
    }

    @After
    public void tearDown()
    {

    }

    @Ignore
    @Test
    public void jsonPluginInfoIsValid() throws Exception
    {

        PluginInformation pinfo = mock(PluginInformation.class);
        when(pinfo.getDescription()).thenReturn("desc");
        when(pinfo.getVendorName()).thenReturn("Atlassian");
        when(pinfo.getVendorUrl()).thenReturn("http://www.atlassian.com");
        when(pinfo.getVersion()).thenReturn("1.0");


        int expectedNumPlugins = 2;
        String pluginOneName = "Plugin One";
        String pluginOneKey = "com.atlassian.plugins.plugin-one";
        int expectedNumClassesOne = 2;

        String pluginTwoName = "Plugin Two";
        String pluginTwoKey = "com.atlassian.plugins.plugin-two";
        int expectedNumClassesTwo = 1;

        OsgiPlugin plugin1 = mock(OsgiPlugin.class);
        OsgiPlugin plugin2 = mock(OsgiPlugin.class);
        PluginArtifact artifact1 = mock(PluginArtifact.class);
        PluginArtifact artifact2 = mock(PluginArtifact.class);
/*
        when(plugin1.getName()).thenReturn(pluginOneName);
        when(plugin1.getKey()).thenReturn(pluginOneKey);
        when(plugin1.getPluginInformation()).thenReturn(pinfo);
        when(plugin1.getPluginArtifact()).thenReturn(artifact1);
        when(artifact1.getName()).thenReturn("plugin-one.jar");

        when(plugin2.getName()).thenReturn(pluginTwoName);
        when(plugin2.getKey()).thenReturn(pluginTwoKey);
        when(plugin2.getPluginInformation()).thenReturn(pinfo);
        when(plugin2.getPluginArtifact()).thenReturn(artifact2);
        when(artifact2.getName()).thenReturn("plugin-two.jar");

        String class1 = "com.example.plugin.ExtensionOne";
        String class2 = "com.example.plugin.ExtensionTwo";
        String class3 = "com.example.plugin.ExtensionThree";

        Map<Plugin, Set<String>> pluginListMap = new LinkedHashMap<Plugin, Set<String>>(2);
        
        pluginListMap.put(plugin1, Sets.newHashSet(class1, class2));
        pluginListMap.put(plugin2, Sets.newHashSet(class3));


        WorkflowExtensionsHelper helper = new WorkflowExtensionsHelperImpl(moduleDescriptorLocator, jiraAuthenticationContext, workflowService, workflowScreensHelper, workflowCustomFieldsHelper, pluginArtifactFinder);
        String json = helper.requiredPluginsToJson(pluginListMap);

        assert (StringUtils.isNotBlank(json)) : "JSON String is Blank";

        ObjectMapper mapper = new ObjectMapper();

        List<WorkflowExtensionsPluginInfo> infoList = mapper.readValue(json, new TypeReference<List<WorkflowExtensionsPluginInfo>>(){});
        int actualNumPlugins = infoList.size();

        assert (null != infoList) : "infoList is null";
        assert (expectedNumPlugins == actualNumPlugins) : "Wrong number of plugins. Expected " + expectedNumPlugins + " but found " + actualNumPlugins;
        assert (infoList.get(0).getName().equals(pluginOneName)) : "plugin at index 0 has wrong name. Expected " + pluginOneName + " but was " + infoList.get(0).getName();
        assert (infoList.get(0).getKey().equals(pluginOneKey)) : "plugin at index 0 has wrong key. Expected " + pluginOneKey + " but was " + infoList.get(0).getKey();
        assert (expectedNumClassesOne == infoList.get(0).getClassNames().size()) : "plugin at index 0 has number of class names. Expected " + expectedNumClassesOne + " but was " + infoList.get(0).getClassNames().size();


        assert (infoList.get(1).getName()
                .equals(pluginTwoName)) : "plugin at index 1 has wrong name. Expected " + pluginTwoName + " but was " + infoList
                .get(1).getName();
        assert (infoList.get(1).getKey()
                .equals(pluginTwoKey)) : "plugin at index 1 has wrong key. Expected " + pluginTwoKey + " but was " + infoList
                .get(1).getKey();
        assert (expectedNumClassesTwo == infoList.get(1).getClassNames()
                .size()) : "plugin at index 1 has number of class names. Expected " + expectedNumClassesTwo + " but was " + infoList
                .get(1).getClassNames().size();
*/
    }

    @Ignore
    @Test
    public void emptyMapReturnsEmptyJsonArray() throws Exception
    {
        /*
        Map<Plugin, Set<String>> pluginListMap = new HashMap<Plugin, Set<String>>();
        WorkflowExtensionsHelper helper = new WorkflowExtensionsHelperImpl(moduleDescriptorLocator, jiraAuthenticationContext, workflowService, workflowScreensHelper, workflowCustomFieldsHelper, pluginArtifactFinder);
        String json = helper.requiredPluginsToJson(pluginListMap);
        assert (json.equals("[]")) : "Expected empty JSON Array but got " + json;

        ObjectMapper mapper = new ObjectMapper();
        List<WorkflowExtensionsPluginInfo> infoList = mapper
                .readValue(json, new TypeReference<List<WorkflowExtensionsPluginInfo>>()
                {});

        assert (null != infoList) : "infoList is null";
        assert (0 == infoList.size()) : "Expected empty infoList but size is " + infoList.size();
*/
    }


}
