package com.atlassian.jira.plugin.workflow.sharing;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.message.MessageCollection;

import org.junit.Before;
import org.junit.Test;


/**
 * @since version
 */
public class VersionCompare
{
    private I18nResolver i18n;
    
    @Before
    public void setup()
    {
        this.i18n = new I18nResolver(){

            @Override
            public String getRawText(String s)
            {
                return s;
            }

            @Override
            public String getText(String s, Serializable... serializables)
            {
                return s;
            }

            @Override
            public String getText(String s)
            {
                return s;
            }

            @Override
            public String getText(Message message)
            {
                return message.toString();
            }

            @Override
            public Message createMessage(String s, Serializable... serializables)
            {
                return null;
            }

            @Override
            public MessageCollection createMessageCollection()
            {
                return null;
            }

            @Override
            public Map<String, String> getAllTranslationsForPrefix(String s)
            {
                return null;
            }

            @Override
            public Map<String, String> getAllTranslationsForPrefix(String s, Locale locale)
            {
                return null;
            }
        };
    }
    
    @Test
    public void majorOnly() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1", i18n);
        ArtifactVersion v2 = new ArtifactVersion("2", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void minorOnly() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1.0", i18n);
        ArtifactVersion v2 = new ArtifactVersion("1.1", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void microOnly() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1.0.0", i18n);
        ArtifactVersion v2 = new ArtifactVersion("1.0.1", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void majorMinor() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1", i18n);
        ArtifactVersion v2 = new ArtifactVersion("1.1", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void minorMicro() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1.01", i18n);
        ArtifactVersion v2 = new ArtifactVersion("1.01.1", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void qualifiedMajorOnly() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1rc1", i18n);
        ArtifactVersion v2 = new ArtifactVersion("2rc1", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void qualifiedSameMajorOnly() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1rc1", i18n);
        ArtifactVersion v2 = new ArtifactVersion("1rc2", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void qualifiedAndUnqualifiedMajorOnly() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1rc1", i18n);
        ArtifactVersion v2 = new ArtifactVersion("2", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }


    @Test
    public void qualifiedMinorOnly() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1.1rc1", i18n);
        ArtifactVersion v2 = new ArtifactVersion("1.2rc1", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void qualifiedSameMinorOnly() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1.1rc1", i18n);
        ArtifactVersion v2 = new ArtifactVersion("1.1rc2", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void qualifiedAndUnqualifiedMinorOnly() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1.1rc1", i18n);
        ArtifactVersion v2 = new ArtifactVersion("1.1", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void qualifiedOnly() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1.1alpha", i18n);
        ArtifactVersion v2 = new ArtifactVersion("1.1beta", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

    @Test
    public void qualifiedRev() throws Exception
    {
        ArtifactVersion v1 = new ArtifactVersion("1.1alpha1", i18n);
        ArtifactVersion v2 = new ArtifactVersion("1.1alpha2", i18n);

        int comp = v1.compareTo(v2);

        assert(comp < 0);
    }

}
